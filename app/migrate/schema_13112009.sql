-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- M�quina: localhost
-- Data de Cria��o: 19-Nov-2009 �s 21:54
-- Vers�o do servidor: 5.1.37
-- vers�o do PHP: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de Dados: `radio_america`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `listeners`
--

CREATE TABLE IF NOT EXISTS `listeners` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `endereco` varchar(200) NOT NULL,
  `bairro` varchar(60) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `cep` char(8) NOT NULL,
  `telefone` varchar(25) NOT NULL,
  `data_nascimento` date NOT NULL,
  `email` varchar(150) NOT NULL,
  `socio` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `listeners`
--

INSERT INTO `listeners` (`id`, `nome`, `endereco`, `bairro`, `estado`, `cidade`, `cep`, `telefone`, `data_nascimento`, `email`, `socio`, `created`, `modified`) VALUES
(1, 'Michel de Almeida Carvalho', 'Rua Professora Francisca �ngela A. Prado, 192', 'Parque do Sol', 'SP', 'Guaratinguet�', '12518080', '(12)31254654', '1985-06-10', 'michel@theoneit.com', 1, '2009-11-12 02:13:57', '2009-11-17 02:48:04'),
(2, 'Rosiene Soares Carvalho', 'Rua Nelson Luiz de Franca, 21', 'COHAB (Nova Guar�)', 'SP', 'Guaratinguet�', '12517380', '(12)31257340', '1999-11-29', 'carvalho.michel@gmail.com', 1, '2009-11-17 02:34:58', '2009-11-17 02:34:58'),
(3, 'Larissa Soares Carvalho', 'Rua Professora Francisca �ngela A. Prado, 192', 'Parque do Sol', 'SP', 'Guaratinguet�', '12518080', '(11)77463681', '1970-11-18', 'xexeul@hotmail.com', 1, '2009-11-17 02:38:17', '2009-11-17 02:43:19'),
(5, 'Leticia Soares Carvalho', ' Re Re Re, 182', 'Ri Ri RI', 'SP', 'Guaratinguet�', '12500000', '(12)97405389', '2009-11-17', 'bug@theoneit.com', 1, '2009-11-17 02:45:51', '2009-11-17 02:45:51'),
(6, 'Joao Batista Carvalho', 'Rua Nelson Luiz de Franca, 22', 'COHAB (Nova Guar�)', 'SP', 'Guaratinguet�', '12517380', '(12)31254654', '2009-11-17', 'status_contabilidade@hotmail.com', 0, '2009-11-17 02:46:43', '2009-11-17 02:46:43'),
(7, 'Super teste de ouvinte', 'Rua 6, 192', 'Pq do Sol', 'Sp', 'Guara', '', '(12)31224064', '2009-11-18', 'carvalho_michel@yahoo.com', 0, '2009-11-18 23:51:53', '2009-11-18 23:51:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `participations`
--

CREATE TABLE IF NOT EXISTS `participations` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `program_id` int(4) NOT NULL,
  `listener_id` int(15) NOT NULL,
  `pedido_musical` text NOT NULL,
  `mensagem` longtext NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `participations`
--

INSERT INTO `participations` (`id`, `program_id`, `listener_id`, `pedido_musical`, `mensagem`, `created`, `modified`) VALUES
(1, 2, 6, ' gs gdf gdfs', 'g dfg dfg dfg', '2009-11-18 02:54:21', '2009-11-18 02:54:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `participation_promotions`
--

CREATE TABLE IF NOT EXISTS `participation_promotions` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(8) NOT NULL,
  `listener_id` int(15) NOT NULL,
  `resposta` longtext NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `participation_promotions`
--

INSERT INTO `participation_promotions` (`id`, `promotion_id`, `listener_id`, `resposta`, `created`, `modified`) VALUES
(1, 3, 6, ' dgdsfgdf sgds gdf gsgdfgd', '2009-11-18 02:54:22', '2009-11-18 02:54:22'),
(2, 4, 6, ' ta fg dsg df gdsdf', '2009-11-18 02:54:22', '2009-11-18 02:54:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `programs`
--

CREATE TABLE IF NOT EXISTS `programs` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `speaker_id` int(4) DEFAULT NULL,
  `horario_inicio` time DEFAULT NULL,
  `horario_fim` time NOT NULL,
  `dia_da_semana` varchar(35) NOT NULL,
  `radio_id` int(4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `programs`
--

INSERT INTO `programs` (`id`, `nome`, `speaker_id`, `horario_inicio`, `horario_fim`, `dia_da_semana`, `radio_id`, `created`, `modified`) VALUES
(1, 'Manha com Deus', 2, '02:24:00', '02:58:00', '4', 1, '2009-11-12 02:04:59', '2009-11-17 03:38:18'),
(2, 'Teste Programa 2', 3, '03:16:00', '03:16:00', '4', 1, '2009-11-16 23:16:22', '2009-11-17 03:35:12'),
(3, 'Super Programa 3', 2, '01:16:00', '02:16:00', '4', 1, '2009-11-16 23:16:46', '2009-11-18 03:43:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `promotions`
--

CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `data_inicio` datetime DEFAULT NULL,
  `data_fim` datetime DEFAULT NULL,
  `premio` varchar(150) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `promotions`
--

INSERT INTO `promotions` (`id`, `descricao`, `data_inicio`, `data_fim`, `premio`, `created`, `modified`) VALUES
(3, 'gdf gdfsg dfsg dfsg gdf gdfgdgdfs gdf gd gdfg gdfgdfgsdfg', '2009-11-23 04:34:00', '2009-11-27 04:34:00', ' gdfsg dfsgdfsgdsfgdfs gdfgdfgdfsg', '2009-11-13 04:34:37', '2009-11-16 22:54:34'),
(4, 'ASPd faoksdkf dspkfp KDFok dPfds', '2009-11-10 01:38:00', '2009-11-27 13:36:00', 'KDOA SKDKS AKDLAS DKASLK;AS KDSAK ;LAS', '2009-11-16 23:53:16', '2009-11-16 23:53:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `promotion_programs`
--

CREATE TABLE IF NOT EXISTS `promotion_programs` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(5) NOT NULL,
  `program_id` int(4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `promotion_programs`
--

INSERT INTO `promotion_programs` (`id`, `promotion_id`, `program_id`, `created`, `modified`) VALUES
(2, 3, 1, '2009-11-16 23:45:42', '2009-11-16 23:45:42'),
(4, 3, 2, '2009-11-16 23:45:43', '2009-11-16 23:45:43'),
(5, 4, 2, '2009-11-17 00:01:32', '2009-11-17 00:01:32'),
(6, 4, 1, '2009-11-17 00:01:32', '2009-11-17 00:01:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `radios`
--

CREATE TABLE IF NOT EXISTS `radios` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `radios`
--

INSERT INTO `radios` (`id`, `nome`, `created`, `modified`) VALUES
(1, 'R�dio Am�rica', '2009-11-12 02:01:58', '2009-11-13 03:03:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `speakers`
--

CREATE TABLE IF NOT EXISTS `speakers` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `speakers`
--

INSERT INTO `speakers` (`id`, `nome`, `created`, `modified`) VALUES
(1, 'Fl�via', '2009-11-12 02:02:59', '2009-11-13 02:08:07'),
(2, 'Oswaldo', '2009-11-13 02:08:29', '2009-11-13 02:08:29'),
(3, 'Flavia iRI Teste', '2009-11-13 03:47:43', '2009-11-17 01:09:02'),
(4, 'Flavia Ces', '2009-11-13 03:47:51', '2009-11-13 03:49:37');
