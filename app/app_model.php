<?php

class AppModel extends Model {
	public $actsAs = array( 'AuditLog.Auditable' );

	public function current_user() {
		return Configure::read( 'User' );
	}
}
?>