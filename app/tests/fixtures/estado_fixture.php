<?php 
/* SVN FILE: $Id$ */
/* Estado Fixture generated on: 2009-11-11 23:42:19 : 1257990139*/

class EstadoFixture extends CakeTestFixture {
	var $name = 'Estado';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'descricao' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 50),
		'uf' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 2),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'descricao'  => 'Lorem ipsum dolor sit amet',
		'uf'  => ''
	));
}
?>