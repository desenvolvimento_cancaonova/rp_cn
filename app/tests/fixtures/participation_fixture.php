<?php 
/* SVN FILE: $Id$ */
/* Participation Fixture generated on: 2009-11-11 23:42:36 : 1257990156*/

class ParticipationFixture extends CakeTestFixture {
	var $name = 'Participation';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 20, 'key' => 'primary'),
		'program_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 4),
		'listener_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 15),
		'pedido_musical' => array('type'=>'text', 'null' => false, 'default' => NULL),
		'mensagem' => array('type'=>'text', 'null' => false, 'default' => NULL),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'program_id'  => 1,
		'listener_id'  => 1,
		'pedido_musical'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'mensagem'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'created'  => '2009-11-11 23:42:36',
		'modified'  => '2009-11-11 23:42:36'
	));
}
?>