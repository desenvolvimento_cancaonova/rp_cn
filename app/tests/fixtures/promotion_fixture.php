<?php 
/* SVN FILE: $Id$ */
/* Promotion Fixture generated on: 2009-11-11 23:42:41 : 1257990161*/

class PromotionFixture extends CakeTestFixture {
	var $name = 'Promotion';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 8, 'key' => 'primary'),
		'descricao' => array('type'=>'text', 'null' => false, 'default' => NULL),
		'data_inicio' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'data_fim' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'premio' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 150),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'descricao'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'data_inicio'  => '2009-11-11 23:42:41',
		'data_fim'  => '2009-11-11 23:42:41',
		'premio'  => 'Lorem ipsum dolor sit amet',
		'created'  => '2009-11-11 23:42:41',
		'modified'  => '2009-11-11 23:42:41'
	));
}
?>