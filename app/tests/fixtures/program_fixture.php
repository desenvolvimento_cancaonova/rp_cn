<?php 
/* SVN FILE: $Id$ */
/* Program Fixture generated on: 2009-11-11 23:42:39 : 1257990159*/

class ProgramFixture extends CakeTestFixture {
	var $name = 'Program';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 6, 'key' => 'primary'),
		'nome' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 120),
		'speaker_id' => array('type'=>'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'horario_inicio' => array('type'=>'time', 'null' => true, 'default' => NULL),
		'horario_fim' => array('type'=>'time', 'null' => false, 'default' => NULL),
		'dia_da_semana' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 35),
		'radio_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 4),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'nome'  => 'Lorem ipsum dolor sit amet',
		'speaker_id'  => 1,
		'horario_inicio'  => '23:42:39',
		'horario_fim'  => '23:42:39',
		'dia_da_semana'  => 'Lorem ipsum dolor sit amet',
		'radio_id'  => 1,
		'created'  => '2009-11-11 23:42:39',
		'modified'  => '2009-11-11 23:42:39'
	));
}
?>