<?php 
/* SVN FILE: $Id$ */
/* Cidade Fixture generated on: 2009-11-11 23:42:16 : 1257990136*/

class CidadeFixture extends CakeTestFixture {
	var $name = 'Cidade';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'estado_id' => array('type'=>'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'descricao' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 100),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'id_estado' => array('column' => 'estado_id', 'unique' => 0))
	);
	var $records = array(array(
		'id'  => 1,
		'estado_id'  => 1,
		'descricao'  => 'Lorem ipsum dolor sit amet'
	));
}
?>