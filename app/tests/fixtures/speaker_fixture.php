<?php 
/* SVN FILE: $Id$ */
/* Speaker Fixture generated on: 2009-11-11 23:42:47 : 1257990167*/

class SpeakerFixture extends CakeTestFixture {
	var $name = 'Speaker';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 5, 'key' => 'primary'),
		'nome' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 120),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'nome'  => 'Lorem ipsum dolor sit amet',
		'created'  => '2009-11-11 23:42:47',
		'modified'  => '2009-11-11 23:42:47'
	));
}
?>