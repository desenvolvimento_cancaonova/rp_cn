<?php 
/* SVN FILE: $Id$ */
/* ParticipationPromotion Fixture generated on: 2009-11-11 23:42:30 : 1257990150*/

class ParticipationPromotionFixture extends CakeTestFixture {
	var $name = 'ParticipationPromotion';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 20, 'key' => 'primary'),
		'promotion_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 8),
		'listener_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 15),
		'resposta' => array('type'=>'text', 'null' => false, 'default' => NULL),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'promotion_id'  => 1,
		'listener_id'  => 1,
		'resposta'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'created'  => '2009-11-11 23:42:30',
		'modified'  => '2009-11-11 23:42:30'
	));
}
?>