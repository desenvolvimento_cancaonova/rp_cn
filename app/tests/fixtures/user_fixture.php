<?php 
/* SVN FILE: $Id$ */
/* User Fixture generated on: 2009-11-26 23:44:08 : 1259286248*/

class UserFixture extends CakeTestFixture {
	var $name = 'User';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 5, 'key' => 'primary'),
		'username' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 35),
		'password' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 64),
		'created' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type'=>'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id'  => 1,
		'username'  => 'Lorem ipsum dolor sit amet',
		'password'  => 'Lorem ipsum dolor sit amet',
		'created'  => '2009-11-26 23:44:08',
		'modified'  => '2009-11-26 23:44:08'
	));
}
?>