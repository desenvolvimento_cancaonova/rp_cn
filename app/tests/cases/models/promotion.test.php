<?php 
/* SVN FILE: $Id$ */
/* Promotion Test cases generated on: 2009-11-11 23:42:41 : 1257990161*/
App::import('Model', 'Promotion');

class PromotionTestCase extends CakeTestCase {
	var $Promotion = null;
	var $fixtures = array('app.promotion');

	function startTest() {
		$this->Promotion =& ClassRegistry::init('Promotion');
	}

	function testPromotionInstance() {
		$this->assertTrue(is_a($this->Promotion, 'Promotion'));
	}

	function testPromotionFind() {
		$this->Promotion->recursive = -1;
		$results = $this->Promotion->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Promotion' => array(
			'id'  => 1,
			'descricao'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'data_inicio'  => '2009-11-11 23:42:41',
			'data_fim'  => '2009-11-11 23:42:41',
			'premio'  => 'Lorem ipsum dolor sit amet',
			'created'  => '2009-11-11 23:42:41',
			'modified'  => '2009-11-11 23:42:41'
		));
		$this->assertEqual($results, $expected);
	}
}
?>