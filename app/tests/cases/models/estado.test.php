<?php 
/* SVN FILE: $Id$ */
/* Estado Test cases generated on: 2009-11-11 23:42:19 : 1257990139*/
App::import('Model', 'Estado');

class EstadoTestCase extends CakeTestCase {
	var $Estado = null;
	var $fixtures = array('app.estado');

	function startTest() {
		$this->Estado =& ClassRegistry::init('Estado');
	}

	function testEstadoInstance() {
		$this->assertTrue(is_a($this->Estado, 'Estado'));
	}

	function testEstadoFind() {
		$this->Estado->recursive = -1;
		$results = $this->Estado->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Estado' => array(
			'id'  => 1,
			'descricao'  => 'Lorem ipsum dolor sit amet',
			'uf'  => ''
		));
		$this->assertEqual($results, $expected);
	}
}
?>