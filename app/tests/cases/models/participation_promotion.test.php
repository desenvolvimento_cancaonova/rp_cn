<?php 
/* SVN FILE: $Id$ */
/* ParticipationPromotion Test cases generated on: 2009-11-11 23:42:30 : 1257990150*/
App::import('Model', 'ParticipationPromotion');

class ParticipationPromotionTestCase extends CakeTestCase {
	var $ParticipationPromotion = null;
	var $fixtures = array('app.participation_promotion');

	function startTest() {
		$this->ParticipationPromotion =& ClassRegistry::init('ParticipationPromotion');
	}

	function testParticipationPromotionInstance() {
		$this->assertTrue(is_a($this->ParticipationPromotion, 'ParticipationPromotion'));
	}

	function testParticipationPromotionFind() {
		$this->ParticipationPromotion->recursive = -1;
		$results = $this->ParticipationPromotion->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('ParticipationPromotion' => array(
			'id'  => 1,
			'promotion_id'  => 1,
			'listener_id'  => 1,
			'resposta'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created'  => '2009-11-11 23:42:30',
			'modified'  => '2009-11-11 23:42:30'
		));
		$this->assertEqual($results, $expected);
	}
}
?>