<?php 
/* SVN FILE: $Id$ */
/* User Test cases generated on: 2009-11-26 23:44:08 : 1259286248*/
App::import('Model', 'User');

class UserTestCase extends CakeTestCase {
	var $User = null;
	var $fixtures = array('app.user');

	function startTest() {
		$this->User =& ClassRegistry::init('User');
	}

	function testUserInstance() {
		$this->assertTrue(is_a($this->User, 'User'));
	}

	function testUserFind() {
		$this->User->recursive = -1;
		$results = $this->User->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('User' => array(
			'id'  => 1,
			'username'  => 'Lorem ipsum dolor sit amet',
			'password'  => 'Lorem ipsum dolor sit amet',
			'created'  => '2009-11-26 23:44:08',
			'modified'  => '2009-11-26 23:44:08'
		));
		$this->assertEqual($results, $expected);
	}
}
?>