<?php 
/* SVN FILE: $Id$ */
/* Radio Test cases generated on: 2009-11-11 23:42:42 : 1257990162*/
App::import('Model', 'Radio');

class RadioTestCase extends CakeTestCase {
	var $Radio = null;
	var $fixtures = array('app.radio');

	function startTest() {
		$this->Radio =& ClassRegistry::init('Radio');
	}

	function testRadioInstance() {
		$this->assertTrue(is_a($this->Radio, 'Radio'));
	}

	function testRadioFind() {
		$this->Radio->recursive = -1;
		$results = $this->Radio->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Radio' => array(
			'id'  => 1,
			'nome'  => 'Lorem ipsum dolor sit amet',
			'created'  => '2009-11-11 23:42:42',
			'modified'  => '2009-11-11 23:42:42'
		));
		$this->assertEqual($results, $expected);
	}
}
?>