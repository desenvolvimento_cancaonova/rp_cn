<?php 
/* SVN FILE: $Id$ */
/* Participation Test cases generated on: 2009-11-11 23:42:36 : 1257990156*/
App::import('Model', 'Participation');

class ParticipationTestCase extends CakeTestCase {
	var $Participation = null;
	var $fixtures = array('app.participation');

	function startTest() {
		$this->Participation =& ClassRegistry::init('Participation');
	}

	function testParticipationInstance() {
		$this->assertTrue(is_a($this->Participation, 'Participation'));
	}

	function testParticipationFind() {
		$this->Participation->recursive = -1;
		$results = $this->Participation->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Participation' => array(
			'id'  => 1,
			'program_id'  => 1,
			'listener_id'  => 1,
			'pedido_musical'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'mensagem'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created'  => '2009-11-11 23:42:36',
			'modified'  => '2009-11-11 23:42:36'
		));
		$this->assertEqual($results, $expected);
	}
}
?>