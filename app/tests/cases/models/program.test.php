<?php 
/* SVN FILE: $Id$ */
/* Program Test cases generated on: 2009-11-11 23:42:39 : 1257990159*/
App::import('Model', 'Program');

class ProgramTestCase extends CakeTestCase {
	var $Program = null;
	var $fixtures = array('app.program');

	function startTest() {
		$this->Program =& ClassRegistry::init('Program');
	}

	function testProgramInstance() {
		$this->assertTrue(is_a($this->Program, 'Program'));
	}

	function testProgramFind() {
		$this->Program->recursive = -1;
		$results = $this->Program->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Program' => array(
			'id'  => 1,
			'nome'  => 'Lorem ipsum dolor sit amet',
			'speaker_id'  => 1,
			'horario_inicio'  => '23:42:39',
			'horario_fim'  => '23:42:39',
			'dia_da_semana'  => 'Lorem ipsum dolor sit amet',
			'radio_id'  => 1,
			'created'  => '2009-11-11 23:42:39',
			'modified'  => '2009-11-11 23:42:39'
		));
		$this->assertEqual($results, $expected);
	}
}
?>