<?php 
/* SVN FILE: $Id$ */
/* Speaker Test cases generated on: 2009-11-11 23:42:47 : 1257990167*/
App::import('Model', 'Speaker');

class SpeakerTestCase extends CakeTestCase {
	var $Speaker = null;
	var $fixtures = array('app.speaker');

	function startTest() {
		$this->Speaker =& ClassRegistry::init('Speaker');
	}

	function testSpeakerInstance() {
		$this->assertTrue(is_a($this->Speaker, 'Speaker'));
	}

	function testSpeakerFind() {
		$this->Speaker->recursive = -1;
		$results = $this->Speaker->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Speaker' => array(
			'id'  => 1,
			'nome'  => 'Lorem ipsum dolor sit amet',
			'created'  => '2009-11-11 23:42:47',
			'modified'  => '2009-11-11 23:42:47'
		));
		$this->assertEqual($results, $expected);
	}
}
?>