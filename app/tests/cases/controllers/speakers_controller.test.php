<?php 
/* SVN FILE: $Id$ */
/* SpeakersController Test cases generated on: 2009-11-11 23:42:47 : 1257990167*/
App::import('Controller', 'Speakers');

class TestSpeakers extends SpeakersController {
	var $autoRender = false;
}

class SpeakersControllerTest extends CakeTestCase {
	var $Speakers = null;

	function startTest() {
		$this->Speakers = new TestSpeakers();
		$this->Speakers->constructClasses();
	}

	function testSpeakersControllerInstance() {
		$this->assertTrue(is_a($this->Speakers, 'SpeakersController'));
	}

	function endTest() {
		unset($this->Speakers);
	}
}
?>