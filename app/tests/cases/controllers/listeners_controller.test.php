<?php 
/* SVN FILE: $Id$ */
/* ListenersController Test cases generated on: 2009-11-11 23:42:26 : 1257990146*/
App::import('Controller', 'Listeners');

class TestListeners extends ListenersController {
	var $autoRender = false;
}

class ListenersControllerTest extends CakeTestCase {
	var $Listeners = null;

	function startTest() {
		$this->Listeners = new TestListeners();
		$this->Listeners->constructClasses();
	}

	function testListenersControllerInstance() {
		$this->assertTrue(is_a($this->Listeners, 'ListenersController'));
	}

	function endTest() {
		unset($this->Listeners);
	}
}
?>