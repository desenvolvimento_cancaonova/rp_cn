<?php 
/* SVN FILE: $Id$ */
/* ProgramsController Test cases generated on: 2009-11-11 23:42:39 : 1257990159*/
App::import('Controller', 'Programs');

class TestPrograms extends ProgramsController {
	var $autoRender = false;
}

class ProgramsControllerTest extends CakeTestCase {
	var $Programs = null;

	function startTest() {
		$this->Programs = new TestPrograms();
		$this->Programs->constructClasses();
	}

	function testProgramsControllerInstance() {
		$this->assertTrue(is_a($this->Programs, 'ProgramsController'));
	}

	function endTest() {
		unset($this->Programs);
	}
}
?>