<?php 
/* SVN FILE: $Id$ */
/* UserActionAuthsController Test cases generated on: 2009-12-03 00:11:33 : 1259806293*/
App::import('Controller', 'UserActionAuths');

class TestUserActionAuths extends UserActionAuthsController {
	var $autoRender = false;
}

class UserActionAuthsControllerTest extends CakeTestCase {
	var $UserActionAuths = null;

	function startTest() {
		$this->UserActionAuths = new TestUserActionAuths();
		$this->UserActionAuths->constructClasses();
	}

	function testUserActionAuthsControllerInstance() {
		$this->assertTrue(is_a($this->UserActionAuths, 'UserActionAuthsController'));
	}

	function endTest() {
		unset($this->UserActionAuths);
	}
}
?>