<?php 
/* SVN FILE: $Id$ */
/* ControllerAuthsController Test cases generated on: 2009-12-03 00:11:23 : 1259806283*/
App::import('Controller', 'ControllerAuths');

class TestControllerAuths extends ControllerAuthsController {
	var $autoRender = false;
}

class ControllerAuthsControllerTest extends CakeTestCase {
	var $ControllerAuths = null;

	function startTest() {
		$this->ControllerAuths = new TestControllerAuths();
		$this->ControllerAuths->constructClasses();
	}

	function testControllerAuthsControllerInstance() {
		$this->assertTrue(is_a($this->ControllerAuths, 'ControllerAuthsController'));
	}

	function endTest() {
		unset($this->ControllerAuths);
	}
}
?>