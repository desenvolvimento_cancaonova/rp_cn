<?php 
/* SVN FILE: $Id$ */
/* ParticipationPromotionsController Test cases generated on: 2009-11-11 23:42:30 : 1257990150*/
App::import('Controller', 'ParticipationPromotions');

class TestParticipationPromotions extends ParticipationPromotionsController {
	var $autoRender = false;
}

class ParticipationPromotionsControllerTest extends CakeTestCase {
	var $ParticipationPromotions = null;

	function startTest() {
		$this->ParticipationPromotions = new TestParticipationPromotions();
		$this->ParticipationPromotions->constructClasses();
	}

	function testParticipationPromotionsControllerInstance() {
		$this->assertTrue(is_a($this->ParticipationPromotions, 'ParticipationPromotionsController'));
	}

	function endTest() {
		unset($this->ParticipationPromotions);
	}
}
?>