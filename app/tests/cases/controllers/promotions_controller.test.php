<?php 
/* SVN FILE: $Id$ */
/* PromotionsController Test cases generated on: 2009-11-11 23:42:41 : 1257990161*/
App::import('Controller', 'Promotions');

class TestPromotions extends PromotionsController {
	var $autoRender = false;
}

class PromotionsControllerTest extends CakeTestCase {
	var $Promotions = null;

	function startTest() {
		$this->Promotions = new TestPromotions();
		$this->Promotions->constructClasses();
	}

	function testPromotionsControllerInstance() {
		$this->assertTrue(is_a($this->Promotions, 'PromotionsController'));
	}

	function endTest() {
		unset($this->Promotions);
	}
}
?>