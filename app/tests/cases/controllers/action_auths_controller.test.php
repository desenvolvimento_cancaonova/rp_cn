<?php 
/* SVN FILE: $Id$ */
/* ActionAuthsController Test cases generated on: 2009-12-03 00:11:14 : 1259806274*/
App::import('Controller', 'ActionAuths');

class TestActionAuths extends ActionAuthsController {
	var $autoRender = false;
}

class ActionAuthsControllerTest extends CakeTestCase {
	var $ActionAuths = null;

	function startTest() {
		$this->ActionAuths = new TestActionAuths();
		$this->ActionAuths->constructClasses();
	}

	function testActionAuthsControllerInstance() {
		$this->assertTrue(is_a($this->ActionAuths, 'ActionAuthsController'));
	}

	function endTest() {
		unset($this->ActionAuths);
	}
}
?>