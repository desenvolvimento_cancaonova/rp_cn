<?php 
/* SVN FILE: $Id$ */
/* RadiosController Test cases generated on: 2009-11-11 23:42:42 : 1257990162*/
App::import('Controller', 'Radios');

class TestRadios extends RadiosController {
	var $autoRender = false;
}

class RadiosControllerTest extends CakeTestCase {
	var $Radios = null;

	function startTest() {
		$this->Radios = new TestRadios();
		$this->Radios->constructClasses();
	}

	function testRadiosControllerInstance() {
		$this->assertTrue(is_a($this->Radios, 'RadiosController'));
	}

	function endTest() {
		unset($this->Radios);
	}
}
?>