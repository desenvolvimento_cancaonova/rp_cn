<?php 
/* SVN FILE: $Id$ */
/* ParticipationsController Test cases generated on: 2009-11-11 23:42:36 : 1257990156*/
App::import('Controller', 'Participations');

class TestParticipations extends ParticipationsController {
	var $autoRender = false;
}

class ParticipationsControllerTest extends CakeTestCase {
	var $Participations = null;

	function startTest() {
		$this->Participations = new TestParticipations();
		$this->Participations->constructClasses();
	}

	function testParticipationsControllerInstance() {
		$this->assertTrue(is_a($this->Participations, 'ParticipationsController'));
	}

	function endTest() {
		unset($this->Participations);
	}
}
?>