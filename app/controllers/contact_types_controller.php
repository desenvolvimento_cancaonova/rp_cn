<?php
class ContactTypesController extends AppController {

	var $name = 'ContactTypes';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->ContactType->recursive = 0;
		$this->set('contact_types', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('contact_type', $this->ContactType->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ContactType->create();
			if ($this->ContactType->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ContactType->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ContactType->read(null, $id);
		}
		
		$this->set('contact_type', $this->data);
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ContactType->delete($id)) {
			$this->Session->setFlash(__(ROW_DELETED, true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>