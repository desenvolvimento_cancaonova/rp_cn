<?php
class OccupationsController extends AppController {

	var $name = 'Occupations';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->Occupation->recursive = 0;
		$this->set('occupations', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('occupation', $this->Occupation->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Occupation->create();
			if ($this->Occupation->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Occupation->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Occupation->read(null, $id);
		}
		
		$this->set('occupation', $this->data);
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Occupation->delete($id)) {
			$this->Session->setFlash(__(ROW_DELETED, true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>