<?php
class TesteController extends AppController {

	var $name = 'Teste';
	var $uses = array('ContactBs');
	
	
	function index() {
		
		$rows = $this->ContactBs->find('all', array('fields'=>array('codigo', 'foto_c', 'foto'), 'conditions'=>array('foto IS NOT NULL')));
		
		foreach ($rows as $row) {
			
			echo "<img src='".$this->webroot."teste/img/".$row['ContactBs']['codigo']."' />";
		}
		
		//debug($rows);
		
		exit;
	}
	
	function img($codigo) {
		
		$row = $this->ContactBs->find('first', array('conditions'=>array('codigo'=>$codigo), 'fields'=>array('foto', 'foto_c')));
		
		
		//arquivo temporario
		$tmp_file = uniqid();
		//criando arquivo
		$file = new File(TMP.$codigo.'.jpg', true);
		//salvando dados serializados
		$file->append($row['ContactBs']['foto']);
		
		$file->close();
		
		header('Content-Type: image/jpeg');
		
		echo $row['ContactBs']['foto'];
		
		exit;
	}
	
	
}
?>