<?php
class UsersController extends AppController {

	var $name = 'Users';
	var $helpers = array('Html', 'Form');
	var $uses = array('User', 'ControllerAuth', 'UserActionAuth');
	
	function login() {
		
		if (!empty($this->data)) {
			
			$user = $this->User->find('first', array('conditions'=>array('username'=>$this->data['User']['username'])));
			if (empty($user)) {
				
				$this->Session->setFlash('Usuario desconhecido.');
			}
			else {
				if ($user['User']['password'] == md5($this->data['User']['password'])) {
					
					//debug($this->data); exit;
					unset($user['User']['password']);
					$this->Session->write('Auth', $user);
					
					$this->redirect(array('controller'=>'pages', 'action'=>'home'));
				}
				else {
					
					$this->Session->setFlash('Senha incorreta.');
				}
			}
		}
	}
	
	function logout() {

		$this->Session->delete('Auth');
		$this->redirect(array('action'=>'login'));
	}
	
	function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

	function add() {
		
		if (!empty($this->data['User']['senha'])) {
				
			$this->data['User']['password'] = md5($this->data['User']['senha']);
		}
		
		if (!empty($this->data)) {
			$this->User->create();
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'edit', $this->User->id));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			
			if (!empty($this->data['User']['senha'])) {
				
				$this->data['User']['password'] = md5($this->data['User']['senha']);
			}
			//debug($this->data);
			//exit;
			
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index', $id));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
		
		unset($this->data['User']['password']);
		unset($this->data['User']['senha']);
		
		$controllers = $this->ControllerAuth->find('all', array('conditions'=>array('ControllerAuth.show'=>1), 'contain'=>array('ActionAuth.show'=>1)));
		//pega permissoes do usuario
		$permissions = $this->UserActionAuth->find('list', array(	'conditions'=> array('UserActionAuth.user_id'=>$id), 
																	'recursive'	=> -1,
																	'fields' 	=> array('UserActionAuth.action_auth_id', 'UserActionAuth.action_auth_id')));
			
		
		$this->set('user', $this->data);
		$this->set('controllers', $controllers);
		$this->set('permissions', $permissions);
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->del($id)) {
			$this->Session->setFlash(__(ROW_DELETED, true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function set_permission($user_id = null, $id = null, $op = null) {
		
		Configure::write('debug', 2);
		
		if ($user_id && $id && $op) {
			
			if ((int)$op == 1) {
				
				$this->UserActionAuth->deleteAll(array('user_id' => $user_id, 'action_auth_id' => $id));
			}
			else if ((int)$op == 2) {
				
				$this->UserActionAuth->create();
				$this->UserActionAuth->save(array('UserActionAuth' => array('user_id' => $user_id, 'action_auth_id' => $id)));
			}
		}
		
		exit;
	}

}
?>