<?php
class CampaignsController extends AppController {

	var $name = 'Campaigns';
	var $helpers = array('Html', 'Form', 'Util');
	var $uses = array('Campaign', 'Contact', 'Occupation', 'ContactType', 'Subcontact', 'Address', 'MediaGallery', 'Sender', 'City', 'State');
	var $components = array('Email');
	
	function beforeRender() {
		
		parent::beforeRender();
				
		$this->set('fields', $this->fields);
		$this->set('per_page', $this->per_page);
		$this->set('meses', $this->meses);
		$this->set('semanal', $this->semanal);
		$this->set('estados', $this->Address->estados);
		$this->set('sexo', $this->Contact->sexo);
		$this->set('estados_civil', $this->Contact->estados_civil);
		$this->set('show_contacts', $this->Contact->contact_kind);

		$this->set('states', $this->State->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
		$this->set('cities', $this->City->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
		$this->set('occupations', $this->Occupation->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
		$this->set('contact_types', $this->ContactType->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
	}
	
	function index() {
		$this->Campaign->recursive = 0;
		$this->set('campaigns', $this->paginate('Campaign'));
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('campaign', $this->Campaign->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Campaign->create();
			if ($this->Campaign->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Campaign->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Campaign->read(null, $id);
		}
		
		$this->set('campaign', $this->data);
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Campaign->delete($id)) {
			$this->Session->setFlash(__(ROW_DELETED, true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function send() {

		if (!empty($this->data)) {
			
			//Filtros
			$conditions = $this->default_filter_conditions();

			$this->Contact->recursive = 1;
			$this->set('rows', $this->Contact->find('all', array('conditions'=>$conditions, 'fields'=>array('Contact.nome', 'Contact.empresa', 'Contact.email', 'Contact.id'), 'group'=>array('Contact.id'))));

			$this->set('campaigns', $this->Campaign->find('list', array('fields'=>array('id', 'titulo'), 'order'=>array('Titulo'=>'ASC'))));
			$this->set('senders', $this->Sender->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'ASC'))));					
		}		
	}
	
	function do_send($contact_id = null, $campaign_id = null, $sender_id = null) {
		
		Configure::write('debug', 0);
		
		if (!empty($contact_id) && !empty($campaign_id) && !empty($sender_id)) {

			$error = 0;
			
			$contact = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$contact_id)));			
			
			if (!empty($contact['Contact']['email'])) {

				$campaign = $this->Campaign->find('first', array('conditions'=>array('Campaign.id'=>$campaign_id)));
				$sender = $this->Sender->find('first', array('conditions'=>array('Sender.id'=>$sender_id)));
				
				$this->smtpOptions = array('host'=>$sender['Sender']['servidor_smtp'], 'port'=>$sender['Sender']['porta']);
				
				//verifica se precisa de autenticacao
				if (!empty($sender['Sender']['autentica'])) {
					
					$this->smtpOptions['username'] = $sender['Sender']['login'];
					$this->smtpOptions['password'] = $sender['Sender']['senha'];
				}
				
				//send the email primary
				$error = $this->send_email($sender, $contact, $campaign, 'email') ? 1 : 0;
				
				$contact['Contact']['email_secundario'] = trim($contact['Contact']['email_secundario']);
				if (!empty($contact['Contact']['email_secundario'])) {
					//send the email secundary
					$this->send_email($sender, $contact, $campaign, 'email_secundario');
					$error = 1;
				}
			}
			else
				$error = 0;
			
			//output
			echo $error;
		}		
		else {
			
			$this->redirect(array('controller'=>'campaigns', 'action'=>'send'));
		}		
				
		//echo $this->Email->smtpError;
		
		exit;
	}
	
	private function send_email($sender, $contact, $campaign, $field) {
				
		$this->Email->from = $sender['Sender']['nome']."<".$sender['Sender']['email'].">";				
		$this->Email->to = $contact['Contact']['nome']."<".$contact['Contact'][$field].">";				
		$this->Email->sendAs = 'html';
		$this->Email->subject = $campaign['Campaign']['assunto'];
		$this->Email->delivery = 'smtp';
		$this->Email->smtpOptions = $this->smtpOptions;
		$this->set('data', $campaign);
		
		return $this->Email->send('', 'campaign');			
	}
		
}
?>