<?php
class PagesController extends AppController {

	var $name = 'Pages';
	var $uses = array('Contact');
	
	function no_permission() {
		
	}
	
	function home() {

		$this->Contact->recursive = -1;
		
		$contacts = $this->Contact->find('count', array('conditions'=>array('contact_kind'=>CONTACT_KIND_CONTACT)));
		$contacts_conjuge = $this->Contact->find('count', array('conditions'=>array('contact_kind'=>CONTACT_KIND_CONJUGE)));
		$contacts_sub = $this->Contact->find('count', array('conditions'=>array('contact_kind'=>CONTACT_KIND_SUBCONTACT)));
		
		$this->set(compact('contacts', 'contacts_conjuge', 'contacts_sub'));
	}
}
?>