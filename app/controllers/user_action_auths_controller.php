<?php
class UserActionAuthsController extends AppController {

	var $name = 'UserActionAuths';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->UserActionAuth->recursive = 0;
		$this->set('userActionAuths', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid UserActionAuth.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('userActionAuth', $this->UserActionAuth->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->UserActionAuth->create();
			if ($this->UserActionAuth->save($this->data)) {
				$this->Session->setFlash(__('The UserActionAuth has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The UserActionAuth could not be saved. Please, try again.', true));
			}
		}
		$users = $this->UserActionAuth->User->find('list');
		$actionAuths = $this->UserActionAuth->ActionAuth->find('list');
		$this->set(compact('users', 'actionAuths'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid UserActionAuth', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->UserActionAuth->save($this->data)) {
				$this->Session->setFlash(__('The UserActionAuth has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The UserActionAuth could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->UserActionAuth->read(null, $id);
		}
		$users = $this->UserActionAuth->User->find('list');
		$actionAuths = $this->UserActionAuth->ActionAuth->find('list');
		$this->set(compact('users','actionAuths'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for UserActionAuth', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->UserActionAuth->del($id)) {
			$this->Session->setFlash(__('UserActionAuth deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>