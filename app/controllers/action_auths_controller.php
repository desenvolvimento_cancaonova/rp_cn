<?php
class ActionAuthsController extends AppController {

	var $name = 'ActionAuths';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->ActionAuth->recursive = 0;
		$this->set('actionAuths', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ActionAuth.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('actionAuth', $this->ActionAuth->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ActionAuth->create();
			if ($this->ActionAuth->save($this->data)) {
				$this->Session->setFlash(__('The ActionAuth has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ActionAuth could not be saved. Please, try again.', true));
			}
		}
		$users = $this->ActionAuth->User->find('list');
		$controllerAuths = $this->ActionAuth->ControllerAuth->find('list');
		$this->set(compact('users', 'controllerAuths'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ActionAuth', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ActionAuth->save($this->data)) {
				$this->Session->setFlash(__('The ActionAuth has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ActionAuth could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ActionAuth->read(null, $id);
		}
		$users = $this->ActionAuth->User->find('list');
		$controllerAuths = $this->ActionAuth->ControllerAuth->find('list');
		$this->set(compact('users','controllerAuths'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ActionAuth', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ActionAuth->delete($id)) {
			$this->Session->setFlash(__('ActionAuth deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>