<?php
class ControllerAuthsController extends AppController {

	var $name = 'ControllerAuths';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->ControllerAuth->recursive = 0;
		$this->set('controllerAuths', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ControllerAuth.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('controllerAuth', $this->ControllerAuth->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ControllerAuth->create();
			if ($this->ControllerAuth->save($this->data)) {
				$this->Session->setFlash(__('The ControllerAuth has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ControllerAuth could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ControllerAuth', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ControllerAuth->save($this->data)) {
				$this->Session->setFlash(__('The ControllerAuth has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ControllerAuth could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ControllerAuth->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ControllerAuth', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ControllerAuth->delete($id)) {
			$this->Session->setFlash(__('ControllerAuth deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function create_default_actions($controller_id) {

                $controller = $this->ControllerAuth->find('first', array('conditions'=>array('id'=>$controller_id)));
                $default_actions = array('admin_index'=>'Listar', 'admin_add'=>'Adicionar', 'admin_edit'=>'Editar', 'admin_view'=>'Visualizar', 'admin_delete'=>'Apagar');

                foreach($default_actions as $name=>$show_name) {

                        $this->ActionAuth->create();
                        $this->ActionAuth->save(array('ActionAuth'=>array('controller_auth_id'=>$controller['ControllerAuth']['id'], 'name'=>$name, 'show_name'=>$show_name, 'show'=>1, 'allowed'=>0)));
                }

                $this->Session->setFlash(__('Acoes criadas!!', true));
                $this->redirect(array('controller'=>'action_auths', 'action'=>'index'));
        }
        
	function create_all_actions() {

		$controller = get_class_methods('Controller');
		$app_controller = get_class_methods('AppController');
		
		$dirs = scandir(CONTROLLERS);
				
		foreach($dirs as $file) {
			
			if (is_file(CONTROLLERS.$file) && substr_count($file, '.php')) {
								
				require_once CONTROLLERS.$file;				
				
				$ctlr = Inflector::camelize(str_replace('.php', '', $file));
				
				$methods = get_class_methods($ctlr);
				$methods = array_diff(array_diff($methods, $controller), $app_controller);
				
				$this->ControllerAuth->create();
				$controller = $this->ControllerAuth->save(array('ControllerAuth'=>array('name'=>str_replace('_controller.php', '', $file), 'show_name'=>$ctlr)));
				 
				foreach($methods as $method) {
					
					 $this->ActionAuth->create();
                     $this->ActionAuth->save(array('ActionAuth'=>array('controller_auth_id'=>$this->ControllerAuth->id, 'name'=>$method, 'show_name'=>$method, 'show'=>1, 'allowed'=>0)));
				}
			}			
		}
		
		exit;
	}
}
?>