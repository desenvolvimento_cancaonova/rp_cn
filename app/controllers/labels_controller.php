<?php
class LabelsController extends AppController {

	var $name = 'Labels';
	var $helpers = array('Html', 'Form');

	var $formatos = array('Letter'=>'Carta/Letter', 'A4'=>'A4');
	
	function beforeFilter() {
		
		parent::beforeFilter();
		
		$this->set('formatos', $this->formatos);
	}
	
	function index() {
		$this->Label->recursive = 0;
		$this->set('labels', $this->paginate('Label'));
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('label', $this->Label->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Label->create();
			if ($this->Label->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Label->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Label->read(null, $id);
		}
		
		$this->set('label', $this->data);
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Label->delete($id)) {
			$this->Session->setFlash(__(ROW_DELETED, true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>