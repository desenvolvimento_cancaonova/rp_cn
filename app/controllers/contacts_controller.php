<?php
class ContactsController extends AppController {

	var $name = 'Contacts';
	var $helpers = array('Html', 'Form');
	var $uses = array('Contact', 'Occupation', 'ContactType', 'Subcontact', 'Address', 'MediaGallery', 'City', 'State');	
	var $components = array('Image');	
	
	var $form_name = 'Contatos';
	
	function beforeFilter() {
		
		parent::beforeFilter();
		
		$this->set('estados_civil', $this->Contact->estados_civil);
	}
	
	function beforeRender() {
		
		parent::beforeRender();
		
		$this->set('form_name', $this->form_name);
		$this->set('fields', $this->fields);
		$this->set('per_page', $this->per_page);
		$this->set('meses', $this->meses);
		$this->set('semanal', $this->semanal);
		$this->set('estados', $this->Address->estados);
		$this->set('sexo', $this->Contact->sexo);
		$this->set('estados_civil', $this->Contact->estados_civil);
		$this->set('show_contacts', $this->Contact->contact_kind);

		$this->set('occupations', $this->Occupation->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
		$this->set('contact_types', $this->ContactType->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
	}
	
	function index() {

		$conditions = array('Contact.ativo'=>1, 'Contact.contact_kind'=>CONTACT_KIND_CONTACT);
		
		$this->paginate = array('order'=>array('Contact.nome'=>'asc'), 'contain'=>array('Conjuge', 'AddressComercial', 'AddressResidencial', 'MediaGalleryMain'), 'fields'=>array('Contact.contact_kind', 'Contact.id', 'Contact.nome', 'Contact.empresa', 'Contact.email', 'Contact.fone1_comercial', 'Contact.fone2_comercial', 'Contact.celular', 'Contact.fone1_residencial', 'Contact.ativo', 'MediaGalleryMain.id'));
		
		if (!empty($this->data)) {
						
			$conditions = $this->default_filter_conditions();
			
			$this->paginate['limit'] = 50000;
			$this->paginate['group'] = array('Contact.id');		
		}
		
		$this->set('states', $this->State->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'ASC'))));
		$this->set('cities', $this->City->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'ASC'))));
		$this->set('contacts', $this->paginate('Contact', $conditions));
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}

		$this->set('contact', $this->Contact->read(null, $id));
	}
	
	function add() {
					
		if (!empty($this->data)) {

			$this->manage_date_fields_input();
						
			$this->Contact->create();
			if ($this->Contact->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'edit', $this->Contact->id));		
								
			} else {
				
				$this->manage_date_fields_output();
								
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}		
		
		$this->set_common_vars();
	}

	function edit($id = null) {
						
		if (!empty($this->data)) {
			
			//debug($this->data); exit;
			
			$this->manage_date_fields_input();
		
			if ($this->Contact->save($this->data)) {
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'edit', $this->Contact->id));		
								
			} else {
				
				$this->manage_date_fields_output();
								
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
				
				//pegando main image e main address
				$data = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$id), 'recursive'=>0, 'contain'=>array('AddressMain', 'MediaGalleryMain')));
				
				$this->data['MediaGalleryMain'] = $data['MediaGalleryMain'];
				$this->data['AddressMain'] = $data['AddressMain'];
			}
		}
		else
			$this->data = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$id), 'recursive'=>0, 'contain'=>array('AddressMain', 'MediaGalleryMain')));

			
		$this->manage_date_fields_output();
				
		$this->set('contact', $this->data);
		$this->set('modified', $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$id), 'fields'=>array('User.nome', 'Contact.modified'))));
		
		$this->set_common_vars();
		
		//debug($this->data);
	}

	function conjuge($id = null) {

		//verifica de o id nao foi passado
		if (empty($id)) {
			//verifica se o conjuge id foi passado e seta o id do contato
			if (!empty($this->params['named']['conjuge_id']))
				$id = $this->Contact->field('id', array('conjuge_id'=>$this->params['named']['conjuge_id']));
			else
				$this->redirect(array('controller'=>'contacts', 'action'=>'index'));
		}
		
		//setting main contact_id
		$this->set('contact_id', $id);
		
		$data = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$id), 'recursive'=>2, 'contain'=>array('AddressMain', 'Conjuge.AddressMain', 'MediaGalleryMain')));
		//debug($data);
		
		if (!empty($this->data)) {

			$this->manage_date_fields_input();
			
			//verifica se o conjuge ja existe
			if (empty($data['Conjuge']['id']))
				$this->Contact->create();
			else 
				$this->Contact->id = $data['Conjuge']['id'];
				
			$this->data['Contact']['contact_kind'] = CONTACT_KIND_CONJUGE;
			
			if ($this->Contact->save($this->data)) {
				
				$conjuge_id = $this->Contact->id;
				
				//salvando conjuge_id
				$this->Contact->id = $id;
				$this->Contact->save(array('Contact'=>array('conjuge_id'=>$conjuge_id)));
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));				
			}
			else {
				
				$this->manage_date_fields_output();
								
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
		else {

			if (!empty($data['Conjuge']['id']))
				$this->data['Contact'] = $data['Conjuge'];
			else
				$this->data = array('Contact'=>array('ativo'=>1));		
		}
		
		if (!empty($data['Conjuge']['id']))
			$this->data['AddressMain'] = $data['Conjuge']['AddressMain'];
		else
			$this->data['AddressMain'] = array('Conjuge'=>array('AddressMain'=>array()));

		$this->manage_date_fields_output();		
		
		$this->set('contact', $data);
		$this->set('modified', $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$data['Contact']['conjuge_id']), 'fields'=>array('User.nome', 'Contact.modified'))));
		
		$this->set_common_vars();
	}
		
	function subcontacts($id = null, $subcontact_id = null) {

		//verifica de o id nao foi passado
		if (empty($id)) {
			//verifica se o subcontato id foi passado e seta o id do contato
			if (!empty($this->params['named']['subcontact_id'])) {
					
				$subcontact_id = $this->params['named']['subcontact_id'];
				$id = $this->Subcontact->field('contact_id', array('subcontact_id'=>$subcontact_id));
			}	
			else
				$this->redirect(array('controller'=>'contacts', 'action'=>'index'));
		}
		
		//setting main contact_id
		$this->set('contact_id', $id);
		
		if (!empty($this->data)) {

			$this->manage_date_fields_input();
						
			if ($subcontact_id == 'new')
				$this->Contact->create();
			else 
				$this->Contact->id = $subcontact_id;
				
			$this->data['Contact']['contact_kind'] = CONTACT_KIND_SUBCONTACT;
				
			if ($this->Contact->save($this->data)) {

				//salvando o relacionamento 
				if ($subcontact_id == 'new') {

					$this->Subcontact->create();
					$this->Subcontact->save(array('Subcontact'=>array('contact_id'=>$id, 'subcontact_id'=>$this->Contact->id)));
				}						 
				
				$this->Session->setFlash(__(ROW_SAVED_SUCCESS, true));
				$this->redirect(array('action'=>'subcontacts', $id));				
			}
			else {
				
				$this->manage_date_fields_output();
								
				$this->Session->setFlash(__(ROW_SAVED_ERROR, true));
			}
		}
		else if (!empty($subcontact_id)) {
				
			$this->data = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$subcontact_id), 'recursive'=>1, 'contain'=>array('AddressMain')));
			
		}
		
		$this->manage_date_fields_output();
	
		
		$this->Subcontact->recursive = 0;
		
		$conditions = array('Subcontact.contact_id' => $id, 'SubcontactRow.contact_kind'=>CONTACT_KIND_SUBCONTACT);
						
		$this->set('contacts', $this->paginate('Subcontact', $conditions));
		$this->set('contact', $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$id), 'fields'=>array(), 'recursive'=>1)));
		$this->set('modified', $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$subcontact_id), 'fields'=>array('User.nome', 'Contact.modified'))));
		
		$this->set_common_vars();
	}
	
	function addresses($parent_contact_id = null, $contact_id = null, $address_id = null) {

		$conditions_cities = array();
		
		if (!empty($this->data)) {
			
			//settando o id do contato do endereco
			$this->data['Address']['contact_id'] = $contact_id;
			
			//edicao
			if (!empty($address_id))
				$this->data['Address']['id'] = $address_id;
						
			if ($this->Address->save($this->data)) {
				
				if (!empty($this->data['Address']['main']) && $address_id != 'new')
					$this->Address->updateAll(array('main'=>0), array('Address.contact_id'=>$contact_id, 'Address.id <> '.$address_id));
				
				$this->Session->setFlash('Endere&ccedil;o salvo.');
				$this->redirect(array('controller'=>'contacts', 'action'=>'addresses', $parent_contact_id, $contact_id));
			}
			else {
				
				$this->Session->setFlash('Erro ao salvar Endere&ccedil;o.');
			}
		}
		else {

			if (!empty($address_id) && $address_id != 'new' && empty($this->data)) {

				$this->data = $this->Address->find('first', array('conditions'=>array('Address.id'=>$address_id)));
				$conditions_cities['state_id'] = $this->data['Address']['state_id'];
			}
		}
		
		if ($parent_contact_id != 'contact') 
			$contact = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$parent_contact_id), 'recursive'=>-1));
		else
			$contact = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$contact_id), 'recursive'=>-1));
						
		$contact_row = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$contact_id), 'recursive'=>-1));
		$addresses = $this->Address->find('all', array('conditions'=>array('Address.contact_id'=>$contact_id)));
		
		$this->set('contact', $contact);
		$this->set('contact_row', $contact_row);
		$this->set('addresses', $addresses);
		$this->set('address_names', $this->Address->address_names);
		$this->set('states', $this->State->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
		$this->set('cities', $this->City->find('list', array('conditions'=>$conditions_cities, 'fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
		
		$this->set_common_vars();
	}
		
	function address_delete($parent_contact_id = null, $contact_id = null, $address_id = null) {
		
		if (!empty($address_id)) {
			
			$this->Address->delete(array('id'=>$address_id));
			
			$this->Session->setFlash('Endere&ccedil;o apagado.');
		}
		else
			$this->Session->setFlash('Erro ao endere&ccedil;o.');
				
		$this->redirect(array('controller'=>'contacts', 'action'=>'addresses', $parent_contact_id, $contact_id));
	}
	
	function delete($id = null) {
		
		if (!$id) {
			
			$this->Session->setFlash(__(ROW_INVALID, true));
			$this->redirect(array('action'=>'index'));
		}
		
		$contact = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$id), 'recursive'=>3, 'contain'=>array('Conjuge.Address', 'Address', 'MediaGallery', 'Subcontact')));
		
		//apagando Enderecos do Contato
		$this->Address->deleteAll(array('contact_id'=>$id));

		//apagando registros de medias		
		$this->MediaGallery->deleteAll(array('contact_id'=>$id));
		//deletando arquivos de imagem do contato		
		foreach($contact['MediaGallery'] as $media)			
			foreach($media['images'] as $image) {				
				if (substr_count('noimage', $image) > 0)					
					unlink(WWW_ROOT.$image);				
			}
				
		//apagando conjuge
		$this->Contact->delete($contact['Contact']['conjuge_id']);
		//apagando Enderecos do conjuge do Contato
		$this->Address->deleteAll(array('contact_id'=>$contact['Contact']['conjuge_id']));
		
		
		if (!empty($contact['Subcontact'])) {

			//subcontatos
			foreach($contact['Subcontact'] as $subcontact) {
					
				//apagando Enderecos do subcontato do Contato
				$this->Address->deleteAll(array('contact_id'=>$subcontact['subcontact_id']));
				
				//apagando subcontato do contato
				$this->Contact->delete($subcontact['subcontact_id']);
			}
		}

		//apagando contato
		if ($this->Contact->delete($id)) {

			$this->Session->setFlash(__(ROW_DELETED, true));
			$this->redirect(array('action'=>'index'));
		}
	}
		
	function get_address($cep = null) {
	
		$consulta = 'http://viavirtual.com.br/webservicecep.php?cep='.$cep;
		$consulta = file($consulta);
		
		$consulta = explode('||',$consulta[0]);
		
		// Caso seja necessario poder salvar os dados em SESSION
		$rua = trim(utf8_encode($consulta[0]));
		$bairro = trim(utf8_encode($consulta[1]));
		$cidade = trim(utf8_encode($consulta[2]));
		$uf = trim(utf8_encode($consulta[4]));
		
		Configure::write('debug', 0);
		
		$this->layout = 'blank';
		
		$this->set(compact('rua', 'bairro', 'cidade', 'uf'));			
	}
		
	public function add_image($contact_id = null) {

		$data = $this->Contact->find('first', array('conditions'=>array('Contact.id'=>$contact_id), 'recursive'=>-1));
		
		//checking if form was submitted
		if (!empty($this->data)) {

			$this->MediaGallery->create();
			
			//checking if image was selected and submitted
			if (!empty($this->data['MediaGallery']['image']['tmp_name'])) {

				//saves media row
				$this->MediaGallery->save(array('MediaGallery'=>array('contact_id'=>$contact_id)));

				$this->_upload($this->MediaGallery->id, $this->data['MediaGallery']['image']);
				
				$this->Session->setFlash('Imagem adicionada!');
				
				unset($this->data['MediaGallery']);
			}
			else {
				
				$this->Session->setFlash('Problema ao salvar!');
			}
		}
		
		
		$images = $this->MediaGallery->find('all', array('conditions'=>array('contact_id'=>$contact_id), 'recursive'=>-1, 'order'=>array('main'=>'DESC')));		
		
		$this->set(compact('images'));
		$this->set('contact', $data);
	}
	
	public function delete_image($id = null) {
				
		if ($id) {

			$this->MediaGallery->deleteAll(array('id'=>$id));
		}
		
		exit;
	}
	
	public function set_main_image($contact_id = null, $id = null) {
				
		if ($id) {

			$this->MediaGallery->updateAll(array('main'=>0), array('contact_id'=>$contact_id));
			$this->MediaGallery->updateAll(array('main'=>1), array('id'=>$id));
		}
		
		$this->redirect(array('controller'=>'contacts', 'action'=>'add_image', $contact_id));
	}
	
	public function status($contact_id = null, $ativo = 0) {
				
		if ($contact_id) {

			$this->Contact->recursive = -1;
			$this->Contact->updateAll(array('Contact.ativo'=>$ativo), array('Contact.id'=>$contact_id));			
		}
		
		$this->redirect(array('controller'=>'contacts', 'action'=>'index'));
	}
	
	private function manage_date_fields_input() {
		
		if (!empty($this->data['Contact']['data_nascimento']))				
			$this->data['Contact']['data_nascimento'] = $this->__input($this->data['Contact']['data_nascimento']);
		else
			unset($this->data['Contact']['data_nascimento']);
			
		if (!empty($this->data['Contact']['data_casamento']))				
			$this->data['Contact']['data_casamento'] = $this->__input($this->data['Contact']['data_casamento']);
		else
			unset($this->data['Contact']['data_casamento']);
			
		if (!empty($this->data['Contact']['data_ordenacao']))				
			$this->data['Contact']['data_ordenacao'] = $this->__input($this->data['Contact']['data_ordenacao']);
		else
			unset($this->data['Contact']['data_ordenacao']);
			
		if (!empty($this->data['Contact']['data_ord_episcopal']))				
			$this->data['Contact']['data_ord_episcopal'] = $this->__input($this->data['Contact']['data_ord_episcopal']);
		else
			unset($this->data['Contact']['data_ord_episcopal']);
			
		if (!empty($this->data['Contact']['data_consagracao']))				
			$this->data['Contact']['data_consagracao'] = $this->__input($this->data['Contact']['data_consagracao']);		
		else
			unset($this->data['Contact']['data_consagracao']);
	}
		
	private function manage_date_fields_output() {
					
		if (!empty($this->data['Contact']['data_nascimento']))				
			$this->data['Contact']['data_nascimento'] = $this->__output($this->data['Contact']['data_nascimento']);
			
		if (!empty($this->data['Contact']['data_casamento']))				
			$this->data['Contact']['data_casamento'] = $this->__output($this->data['Contact']['data_casamento']);
			
		if (!empty($this->data['Contact']['data_ordenacao']))				
			$this->data['Contact']['data_ordenacao'] = $this->__output($this->data['Contact']['data_ordenacao']);
			
		if (!empty($this->data['Contact']['data_ord_episcopal']))				
			$this->data['Contact']['data_ord_episcopal'] = $this->__output($this->data['Contact']['data_ord_episcopal']);
			
		if (!empty($this->data['Contact']['data_consagracao']))				
			$this->data['Contact']['data_consagracao'] = $this->__output($this->data['Contact']['data_consagracao']);
	}
	
	public function get_cities($state_id = null) {

		if (empty($state_id))
			exit;
		
		Configure::write('debug', 0);
		
		$cities = $this->City->find('list', array('conditions'=>array('state_id'=>$state_id), 'fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc')));
				
		$this->layout = 'blank';
		
		$this->set(compact('cities'));	
	}
	
	private function set_common_vars() {
		
		$this->set('occupations', $this->Occupation->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'ASC'))));
		$this->set('contact_types', $this->ContactType->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'ASC'))));
		$this->set('sexo', $this->Contact->sexo);	
		$this->set('estados', $this->Address->estados);
	}
			
	protected function _upload($media_id, $upload_item) {
		
		if (!empty($upload_item['tmp_name'])) {
						
			$file = $upload_item ['tmp_name'];
			$new_file = MEDIAS_IMAGES_PATH . $media_id . '.jpg';
			copy($file, WWW_ROOT.$new_file );
			
			$sizes = $this->MediaGallery->sizes;
			
			foreach ( $sizes as $size_name => $size ) {
				
				$name = $size ['path'] . $media_id . '.jpg';
				
				$this->Image->setSourceFile ( $file );
				$this->Image->resizeImageMin ( $size ['width'], $size ['height'] );
				$this->Image->createFile ( $name, 'image/jpeg' );
				
				$this->Image->setSourceFile ( $name );
				$this->Image->cropImage ( array ($size ['width'], $size ['height'] ) );
				$this->Image->createFile ( $name, 'image/jpeg' );
			}
		}
	}
}
?>
