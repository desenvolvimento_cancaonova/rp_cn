<?php
class ReportsController extends AppController {

	var $name = 'Reports';
	var $helpers = array('Html', 'Form', 'Text', 'Report', 'Xls');
	var $uses = array('Contact', 'Address', 'Occupation', 'Label', 'ContactType', 'State', 'City');

	var $form_name = 'Contatos';

	function beforeFilter() {

		parent::beforeFilter();

		$this->set_config();
	}

	function beforeRender() {
		
		parent::beforeRender();

		$this->set('form_name', $this->form_name);
		$this->set('fields', $this->fields);
		$this->set('per_page', $this->per_page);
		$this->set('meses', $this->meses);
		$this->set('semanal', $this->semanal);
		$this->set('estados', $this->Address->estados);
		$this->set('sexo', $this->Contact->sexo);
		$this->set('estados_civil', $this->Contact->estados_civil);
		$this->set('show_contacts', $this->Contact->contact_kind);

		$this->set('states', $this->State->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
		$this->set('cities', $this->City->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));		
		$this->set('occupations', $this->Occupation->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
		$this->set('contact_types', $this->ContactType->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
	}

	function index() {}

	function email($do = null, $export_type = null) {

		if (!empty($this->data)) {

			//nao gera pdf, soh pre visualizacao
			if (empty($do)) {
					
				//Filtros
				$conditions = $this->default_filter_conditions();


				$this->Contact->recursive = 1;
				$this->set('rows', $this->paginate('Contact', $conditions));

				//campos para ordenacao (opcoes)
				$this->set('fields_pdf', array('Contact.nome'=>'Nome', 'Contact.email'=>'Email'));
			}
			//gera relatorio
			else {

				$relative_url = $this->do_report($this->action);

				if (!empty($export_type)) {
						
					$this->redirect($relative_url.'/'.$export_type.'/true');
				}
			}
		}

		$this->form_name = 'Email';
		$this->set('estados', $this->Address->estados);
	}

	function resumido($do = null, $export_type = null) {

		if (!empty($this->data)) {

			//nao gera pdf, soh pre visualizacao
			if (empty($do)) {
					
				//Filtros
				$conditions = $this->default_filter_conditions();
					
				$this->Contact->recursive = 1;
				$this->set('rows', $this->paginate('Contact', $conditions));

				//campos para ordenacao (opcoes)
				$this->set('fields_pdf', array('Contact.nome'=>'Nome', 'Contact.email'=>'Empresa', 'Occupation.nome'=>'Cargo', 'Contact.cidade'=>'Cidade', 'Contact.data_nascimento'=>'Data Nascimento'));
			}
			//gera relatorio
			else {

				$relative_url = $this->do_report($this->action);

				if (!empty($export_type)) {
						
					$this->redirect($relative_url.'/'.$export_type.'/true');
				}
			}
		}

		$this->form_name = 'Resumido';
		$this->set('estados', $this->Address->estados);
		$this->set('occupations', $this->Occupation->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
	}

	function aniversario($do = null, $export_type = null) {

		if (!empty($this->data)) {

			//nao gera pdf, soh pre visualizacao
			if (empty($do)) {
					
				//Filtros
				$conditions = $this->default_filter_conditions();
					
				//$this->Contact->recursive = 1;
				$this->set('rows', $this->paginate('Contact', $conditions));
				
				//campos para ordenacao (opcoes)
				$this->set('fields_pdf', array('Contact.nome'=>'Nome', 'data_nascimento_order'=>'Data Nascimento'));
			}
			//gera relatorio
			else {

				$relative_url = $this->do_report($this->action);

				if (!empty($export_type)) {
						
					$this->redirect($relative_url.'/'.$export_type.'/true');
				}
			}
		}

		$this->form_name = 'Aniversario';
	}

	function completo($do = null, $export_type = null) {

		if (!empty($this->data)) {
			
			//nao gera pdf, soh pre visualizacao
			if (empty($do)) {
					
				//Filtros
				$conditions = $this->default_filter_conditions();				
					
				//$this->Contact->recursive = 1;
				$this->set('rows', $this->paginate('Contact', $conditions));
				
				//campos para ordenacao (opcoes)
				$this->set('fields_pdf', array('Contact.nome'=>'Nome'));
			}
			//gera relatorio
			else {

				$relative_url = $this->do_report($this->action);

				if (!empty($export_type)) {
						
					$this->redirect($relative_url.'/'.$export_type.'/true');
				}
			}
		}

		$this->form_name = 'Completo';		
	}
		
	function estatistico($do = null, $export_type = null) {
		
		if (!empty($this->data)) {

			// gera pdf
			if (!empty($do)) {

				//verificando qual tipo de agrupamento
				switch ($this->data['Report']['group']) {
					
					case 'cargo': {
						
						$this->data['data'] = $this->occupation_filter($this->data);
						break;
					}
					case 'cidade': {
						
						$this->data['data'] = $this->cidade_filter($this->data);
						break;
					}
					case 'cidade_outro': {
						
						$this->data['data'] = $this->cidade_outro_filter($this->data);
						break;
					}
					case 'pais': {
						
						$this->data['data'] = $this->pais_filter($this->data);
						break;
					}
					case 'sexo': {
						
						$this->data['data'] = $this->sexo_filter($this->data);
						break;
					}
					case 'contact_type': {
						
						$this->data['data'] = $this->contact_type_filter($this->data);
						break;
					}
					case 'estado': {
						
						$this->data['data'] = $this->estado_filter($this->data);
						break;
					}
					case 'estado_outro': {
						
						$this->data['data'] = $this->estado_outro_filter($this->data);
						break;
					}
										
					default: break;
				}

				$relative_url = $this->do_report($this->action);
				
				if (!empty($export_type)) {
						
					$this->redirect($relative_url.'/'.$export_type.'/true');
				}
			}
		}

		//verificando tipo de agrupamento para mostrar opcoes
		if (!empty($this->params['named']['group']) || !empty($this->data['Report']['group'])) {
			
			$group = !empty($this->params['named']['group'])?$this->params['named']['group']:$this->data['Report']['group'];
			
			$fields_pdf = array('quantidade'=>'Quantidade', 'percentagem'=>'Percentagem');
			
			switch ($group) {
				
				case 'cargo': {

					$fields_pdf['Occupation.nome'] = 'Cargo';
					break;
				}
				case 'cidade': {
					
					$fields_pdf['AddressMain.city_id'] = 'Cidade';				
					break;
				}
				case 'cidade_outro': {
					
					$fields_pdf['AddressMain.cidade'] = 'Cidade(outro)';				
					break;
				}
				case 'estado': {
					
					$fields_pdf['AddressMain.state_id'] = 'Estado';				
					break;
				}
				case 'estado_outro': {
					
					$fields_pdf['AddressMain.uf'] = 'Estado(outro)';				
					break;
				}
				case 'pais': {
					
					$fields_pdf['AddressMain.pais'] = 'Pais';				
					break;
				}
				case 'sexo': {
					
					$fields_pdf['Contact.sexo'] = 'Sexo';				
					break;
				}
				case 'contact_type': {
					
					$fields_pdf['ContactType.nome'] = 'Tipo de Contato';				
					break;
				}
				
				default; break;
			}
						
			$this->data['Report']['group'] = $group;
			
			$this->set('fields_pdf', $fields_pdf);
		}
		
		
		$groups = array('cargo'=>'Cargos', 'cidade'=>'Cidades', 'cidade_outro'=>'Cidades(outro)', 'estado'=>'Estado', 'estado_outro'=>'Estado(outro)', 'pais'=>'Pais', 'contact_type'=>'Tipo de Contato', 'sexo'=>'Sexo');
		
		$this->set('groups', $groups);
		$this->form_name = 'Estatistico';
	}
	
	function etiqueta($do = null, $export_type = null) {

		if (!empty($this->data)) {

			//nao gera pdf, soh pre visualizacao
			if (empty($do)) {
					
				//Filtros
				$conditions = $this->default_filter_conditions();
				
				$this->Contact->recursive = 1;
				$this->set('rows', $this->paginate('Contact', $conditions));

				//campos para ordenacao (opcoes)
				$this->set('fields_pdf', array('Contact.nome'=>'Nome', 'Contact.email'=>'Email'));
			}
			//gera relatorio
			else {

				//busca a label selecionada
				if (!empty($this->data['ReportConfig']['label_id']))					
					$this->data['Label'] = current($this->Label->find('first', array('conditions'=>array('id'=>$this->data['ReportConfig']['label_id']))));
									
				$relative_url = $this->do_report($this->action);

				if (!empty($export_type)) {
						
					$this->redirect($relative_url.'/'.$export_type.'/true');
				}
			}
		}

		$this->form_name = 'Etiqueta';
		$this->set('estados', $this->Address->estados);
		$this->set('labels', $this->Label->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
	}
		
	function ficha($do = null, $export_type = null){
		
		if (!empty($this->data)) {

			//nao gera pdf, soh pre visualizacao
			if (empty($do)) {
					
				//Filtros
				$conditions = $this->default_filter_conditions();
				
				$this->Contact->recursive = 2;
				$this->set('rows', $this->paginate('Contact', $conditions));
				
				$this->paginate('Contact', $conditions);

				//campos para ordenacao (opcoes)
				$this->set('fields_pdf', array('Contact.nome'=>'Nome', 'Contact.email'=>'Email'));
			}
			//gera relatorio
			else {

				//busca a label selecionada
				if (!empty($this->data['ReportConfig']['label_id']))					
					$this->data['Label'] = current($this->Label->find('first', array('conditions'=>array('id'=>$this->data['ReportConfig']['label_id']))));
									
				$relative_url = $this->do_report($this->action);

				if (!empty($export_type)) {
						
					$this->redirect($relative_url.'/'.$export_type.'/true');
				}
			}
		}

		$this->form_name = 'Ficha';
		$this->set('estados', $this->Address->estados);
		$this->set('labels', $this->Label->find('list', array('fields'=>array('id', 'nome'), 'order'=>array('nome'=>'asc'))));
	}
		
	function do_report_render($type = null, $tmp_file = null, $file = 'pdf', $download = null) {

		Configure::write('debug', 0);
	
		//abrir arquivo para recuperar contatos
		$file_name = new File(TMP.$tmp_file, true);
		//deserializando dados
		$data = unserialize($file_name->read());
			
		//report estatistico
		if ($type == 'estatistico') {

			$this->set('rows', $data['data']);
		}
		//outros demais
		else {
					 
			//recuperando dados
			$contacts = $this->Contact->find('all', array(	'conditions'=> array('Contact.id'=>$data['Report']),
															'fields' 	=> array('((MONTH(Contact.data_nascimento)*30)+DAY(Contact.data_nascimento)) AS data_nascimento_order', 'Contact.*'), 
															'recursive'	=> 3,															
															'contain'	=> array('Occupation', 'ContactType', 'AddressMain', 'Conjuge.AddressMain', 'Conjuge.Occupation', 'Conjuge.ContactType', 'AddressComercial', 'AddressResidencial', 'Subcontact.SubcontactRow.AddressMain', 'MediaGalleryMain', 'Subcontact.SubcontactRow.Occupation', 'Subcontact.SubcontactRow.ContactType', 'ParentConjuge', 'ParentSubContact.Contact'), 
															'order'		=> array($data['ReportConfig']['fields']=>$data['ReportConfig']['order_fields']),
															'limit'		=> 50000));
			//debug($contacts); exit;
			
			$this->set('contacts', $contacts);			
		}

		//apagando arquivo temporario
		unlink(TMP.$tmp_file);
			
		$this->set('type', $type);
		$this->set('data', $data);
		
		if ($download)
			$this->layout = $file.'_export';
		else
			$this->layout = $file;
	
		
		$this->render($file.'/'.$type);
	}

	private function do_report($type = null) {
		
		unset($this->data['Report']['all']);
			
		//removendo itens sem selecao
		foreach($this->data['Report'] as $key=>$item)
			if (empty($item))
				unset($this->data['Report'][$key]);

		//arquivo temporario
		$tmp_file = uniqid();
		//criando arquivo
		$file = new File(TMP.$tmp_file, true);
		//salvando dados serializados
		$file->append(serialize($this->data));

		//gerando url relativa do arquivo a ser gerado
		$pdf_url = '/reports/do_report_render/'.$type.'/'.$tmp_file;

		$this->set('pdf_url', $this->webroot.$pdf_url);
		$this->set('tmp_file', $tmp_file);

		return $pdf_url;
	}

	private function set_config() {

		$config = array('fields'=>array('Contact.id', 'Contact.nome', 'Contact.empresa', 'Contact.email'), 'contain'=>array('Conjuge', 'AddressComercial', 'AddressResidencial'));
		$config['group'] = array('Contact.id');
		
		unset($this->paginate['limit']);

		if (!empty($this->data['ReportConfig'])) {
				
			$config['order'] = array($this->data['ReportConfig']['fields']=>$this->data['ReportConfig']['order_fields']);
				
			if (!empty($this->data['ReportConfig']['per_page']))
				$config['limit'] = $this->per_page[$this->data['ReportConfig']['per_page']];
			else
				$config['limit'] = 50000;			
		}
		
		$this->paginate = $config;
	}

	
	private function occupation_filter($data) {
				
		$total = $this->Contact->find('count');

		$this->Contact->recursive = 2;
		
		$config = array('order'		=> array($this->data['ReportConfig']['fields']=>$this->data['ReportConfig']['order_fields']),
						'limit'		=> 50000,
						'group' 	=> 'Contact.occupation_id',
						'contain'	=> array('Occupation'),
						'fields'	=> array('TRIM(Occupation.nome) AS default_name', 'COUNT(Contact.occupation_id) AS quantidade', '(COUNT(Contact.occupation_id)*100)/'.$total.' AS percentagem')								
				);

		$this->paginate = $config;
		
		$rows = $this->paginate('Contact', array('Occupation.nome IS NOT NULL'));
		
		return $rows;
	}
	
	private function contact_type_filter($data) {
				
		return $this->default_filter($data, 'ContactType.nome', 'Contact.contact_type_id');
	}
	
	private function cidade_filter($data) {

		$field = 'AddressMain.city_id';
		
		$total = $this->Contact->find('count');

		$this->Contact->recursive = 3;
		
		$config = array('order'		=> array($data['ReportConfig']['fields']=>$data['ReportConfig']['order_fields']),
						'limit'		=> 50000,
						'group' 	=> $field,
						'contain'	=> array('AddressMain.City.nome', 'AddressMain.State.uf', 'ContactType', 'Occupation'),
						'fields'	=> array('Contact.*', 'COUNT('.$field.') AS quantidade', '(COUNT('.$field.')*100)/'.$total.' AS percentagem'),
						'recursive'	=> 3						
				);

		$this->paginate = $config;
		
		$rows = $this->paginate('Contact', array('ContactType.nome IS NOT NULL', 'ContactType.nome IS NOT NULL', 'Occupation.nome IS NOT NULL', 'AddressMain.cidade IS NOT NULL'));
		
		
		$tmp = array();
		foreach ($rows as $row) {
			if (!empty($row['AddressMain']['City']['nome']))
				$tmp[] = array(0=>array_merge($row[0], array('default_name'=>$row['AddressMain']['City']['nome']."-".$row['AddressMain']['State']['uf'])));
		}
		$rows = $tmp;
		
		//Configure::write('debug', 2);
		//debug($rows);
		//exit;
		return $rows;
		
		//return $this->default_filter($data, 'CONCAT(TRIM(AddressMain.City.nome),"-",AddressMain.State.nome)', 'AddressMain.city_id');
	}
	
	private function cidade_outro_filter($data) {
				
		return $this->default_filter($data, 'CONCAT(TRIM(AddressMain.cidade),"-",AddressMain.uf)', 'AddressMain.cidade');
	}
	
	private function estado_filter($data) {
		
		$field = 'AddressMain.state_id';
		
		$total = $this->Contact->find('count');

		$this->Contact->recursive = 3;
		
		$config = array('order'		=> array($data['ReportConfig']['fields']=>$data['ReportConfig']['order_fields']),
						'limit'		=> 50000,
						'group' 	=> $field,
						'contain'	=> array('AddressMain.City.nome', 'AddressMain.State.nome', 'ContactType', 'Occupation'),
						'fields'	=> array('Contact.*', 'COUNT('.$field.') AS quantidade', '(COUNT('.$field.')*100)/'.$total.' AS percentagem'),
						'recursive'	=> 3						
				);

		$this->paginate = $config;
		
		$rows = $this->paginate('Contact', array('ContactType.nome IS NOT NULL', 'ContactType.nome IS NOT NULL', 'Occupation.nome IS NOT NULL', 'AddressMain.cidade IS NOT NULL'));
		
		
		$tmp = array();
		foreach ($rows as $row) {
			if (!empty($row['AddressMain']['State']['nome']))
				$tmp[] = array(0=>array_merge($row[0], array('default_name'=>$row['AddressMain']['State']['nome'])));
		}
		$rows = $tmp;
		
		//Configure::write('debug', 2);
		//debug($rows);
		//exit;
		return $rows;
		
		//return $this->default_filter($data, 'AddressMain.uf', 'AddressMain.uf');
	}
	
	private function estado_outro_filter($data) {
		
		return $this->default_filter($data, 'AddressMain.state_id', 'AddressMain.state_id');
	}
	
	private function pais_filter($data) {
				
		return $this->default_filter($data, 'AddressMain.pais', 'AddressMain.pais');
	}
	
	private function sexo_filter($data) {

		return $this->default_filter($data, 'Contact.sexo', 'Contact.sexo');
	}
	
	private function default_filter($data, $return_field, $field) {
				
		$total = $this->Contact->find('count');

		$this->Contact->recursive = 2;
		
		$config = array('order'		=> array($data['ReportConfig']['fields']=>$data['ReportConfig']['order_fields']),
						'limit'		=> 50000,
						'group' 	=> $field,
						'contain'	=> array('AddressMain', 'ContactType', 'Occupation'),
						'fields'	=> array('TRIM('.$return_field.') AS default_name', 'COUNT('.$field.') AS quantidade', '(COUNT('.$field.')*100)/'.$total.' AS percentagem'),
						'recursive'	=> 2								
				);

		$this->paginate = $config;
		
		$rows = $this->paginate('Contact', array('ContactType.nome IS NOT NULL', 'ContactType.nome IS NOT NULL', 'Occupation.nome IS NOT NULL', 'AddressMain.cidade IS NOT NULL'));
		
		return $rows;
	}
}

?>
