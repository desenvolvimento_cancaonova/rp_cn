$(document).ready(function() {
			
	$(".phone_type").mask("(99)99999999",{placeholder:' '});
	$(".cep_type").mask("99999999",{placeholder:' '});
	
	$.datepick.setDefaults($.datepick.regional['pt-BR']);
	
	$('.input_date').datepick($.extend({showStatus: true, 
	    showOn: 'both', buttonImageOnly: true, buttonImage: webroot+'img/calendar.gif', 
	    altField: '#l10nAlternate', altFormat: 'DD, d MM, yyyy'}, 
	    $.datepick.regional['pt-BR'])
	);
	
	//sumindo somente a primeira msg
	/*
	$(".msg_filter:first").each(function() {
		
		$(this).toggle();
	});
	*/
		
	//pegando todos os fieldsets
	
	$('fieldset').each(function(){
		
		//sumindo com todos campos de filtros
		//$(this).find('.filter_fields').toggle();
		
		//colocando acao click nos legends para sumir com a msg e aparecer os campos e vice-versa
		$(this).find('legend').click(function(){
						
			$(this).parent().find('.filter_fields').toggle();
			$(this).parent().find('.msg_filter').toggle('fast');
			
		});
	});
	
	
	//aparecendo somente com os primeros campos
	/*
	$(".filter_fields:first").each(function() {
		
		$(this).toggle();
	});
	*/
	
	$(".date_filter").each(function(){
		
		$(this).change(function(){
			
			$('#'+$(this).attr('date_field')+'_ativo').attr('checked', true);
		});
	});
	
	//action to get cep
	$("#ReportStateIdResidencial").change(function(){	
		
		
		$.getJSON(webroot+'contacts/get_cities/'+$(this).val(), {ajax: 'true'}, function(j){
			
			var select = $('#ReportCityIdResidencial');
	        var options = select.attr('options');
	        
	        $('option', select).remove();
	        
	        option = new Option("Todos", "");
        	options.add(option);
        	
	        $.each(j, function(index, value) {
	        	option = new Option(value, index);
	        	options.add(option);
	        	//options[index] = new Option(value);
	        });
	        
	        
	        $('#cep_loading').fadeOut('slow');
		});				
	});
	
	//action to get cep
	$("#ReportStateIdComercial").change(function(){	
		
		
		$.getJSON(webroot+'contacts/get_cities/'+$(this).val(), {ajax: 'true'}, function(j){
			
			var select = $('#ReportCityIdComercial');
	        var options = select.attr('options');
	        
	        $('option', select).remove();
	        
	        option = new Option("Todos", "");
        	options.add(option);
        	
	        $.each(j, function(index, value) {
	        	option = new Option(value, index);
	        	options.add(option);
	        	//options[index] = new Option(value);
	        });
	        
	        
	        $('#cep_loading').fadeOut('slow');
		});				
	});
});