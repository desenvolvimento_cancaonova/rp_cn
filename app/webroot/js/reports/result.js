$(document).ready(function() {
	
	var action = $('#ReportFormDo').attr('action');
	
	$('#exportar_pdf_button').click(function(){
			
		$('#ReportFormDo').attr('action', action+'/pdf');
	});
	
	$('#exportar_excel_button').click(function(){
			
		$('#ReportFormDo').attr('action', action+'/excel');
	});
	
	$('#view_button').click(function(){
		
		$('#ReportFormDo').attr('action', action);
	});
	
	$("#all_check").click(function(){
		
		var checked = $(this).attr('checked');
		
		$(".row_check").each(function(){
			
			$(this).attr('checked', checked);
		});		 
	});
		
});