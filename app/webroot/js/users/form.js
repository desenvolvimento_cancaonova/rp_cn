$(document).ready(function() {
		
	$('.input_actions').each(function() {
	
		$(this).click(function() {
		
			request_action(this);
				
		});
	});
	
	$(".permission_all").each(function(){
		
		$(this).click(function() {
			
			var checked = $(this).attr('checked');
			
			var i = 800; 
			$(this).parent().find('input[type=checkbox]').each(function(){
				
				$(this).attr('checked', checked);
				setTimeout("request_action('#"+$(this).attr('id')+"');", i);
				i = i + 800;
			});
		});		
	});
		
});

function request_action(thiser) {
	
	var url = webroot+'users/set_permission/'+$('#UserId').val()+'/'+$(thiser).val()+'/';
	var controller = $(thiser).attr('controller');
	var action = $(thiser).attr('action');
	
	if ($(thiser).attr('checked') == true)
		url += '2';			
	else
		url += '1';
	
	
	$.ajax({
	  type:"GET",
	  url: url,
	  success:function(data){},
	  complete: function(){},
	  beforeSend: function(){}
	});
	
	
	//select index action always
	var found = 0;
	$('input[controller='+controller+']').each(function(){
		
		if ($(thiser).attr('checked') == true)				
			found = 1;		

	});			
		
	if (found)
		$('input[controller='+controller+'][action=index]').attr('checked', true);
}