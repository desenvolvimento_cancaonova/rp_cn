$(document).ready(function() {
	
	$("#ChargeValor").priceFormat({prefix:'', centsSeparator:'.', thousandsSeparator:''});
	$("#ChargeTotalRecebido").priceFormat({prefix:'', centsSeparator:'.', thousandsSeparator:''});
	
	//calcula o troco
	$("#ChargeTotalRecebido").blur(function(){
		
		$(this).val(parseFloat($(this).val()).toFixed(2));
		troco = parseFloat($(this).val()) - parseFloat($("#ChargeValor").val());
		
		$("#ChargeTroco").val(troco.toFixed(2));
	});
			
});