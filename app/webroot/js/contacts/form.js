$(document).ready(function() {
	
	$(".phone_type").mask("(99)99999999",{placeholder:' '});
		
	$.datepick.setDefaults($.datepick.regional['pt-BR']);
	
	$('.date_type').datepick($.extend({showStatus: true, 
	    showOn: 'both', buttonImageOnly: true, buttonImage: webroot+'img/calendar.gif', 
	    altField: '#l10nAlternate', altFormat: 'DD, d MM, yyyy'}, 
	    $.datepick.regional['pt-BR'])
	);		
});