$(document).ready(function() {
	
	//sumindo somente a primeira msg
	$(".search_bar:first").each(function() {
		
		$(this).toggle();
	});
	
	//sumindo somente a primeira msg
	$(".search_get h3").each(function() {
		
		$(this).click(function(){
			
			$(".search_bar:first").toggle();
			$(".search_get:first").toggle();
		});
	});
	
	//sumindo somente a primeira msg
	$(".search_bar h2").each(function() {
		
		$(this).click(function(){
			
			$(".search_bar:first").toggle();
			$(".search_get:first").toggle();
		});
	});
	
	$('.medias_images_a_class').lightBox({
		imageLoading: webroot+'img/lightbox-ico-loading.gif',
		imageBtnClose: webroot+'img/lightbox-btn-close.gif',
		imageBtnPrev: webroot+'img/lightbox-btn-prev.gif',
		imageBtnNext: webroot+'img/lightbox-btn-next.gif',
		imageBtnBlank: webroot+'img/lightbox-blank.gif'
	});
});