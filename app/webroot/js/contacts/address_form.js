$(document).ready(function() {
		
	//action to get cep
	$("#AddressCep").blur(function(){	
		$.ajax({
			  type:"GET", 
			  url: webroot+'contacts/get_address/'+$(this).val(),
			  success:function(data){
			  	
			  	address = data.split('|');
			  	
			  	$("#AddressEndereco").val(address[0]);
			  	$("#AddressBairro").val(address[1]);
			  	$("#AddressCidade").val(address[2]);
			  	$("#AddressUf").val(address[3]);		  		
			  	
			  	estado = $("#AddressUf option:selected").text();
			  	
			  	$("#AddressStateId option").each(function() {
			  		if ($(this).text() == estado) {
			  			$(this).attr('checked', true);			  			
			  		}
			  	});
			  	
			  	$("#AddressStateId").val(address[3]);
			  	
			  },
			  complete: function(){ $('#cep_loading').fadeOut('slow'); },
			  beforeSend: function(){ $('#cep_loading').show(); }
		});		
	});
	
	//action to get cep
	$("#AddressStateId").change(function(){	
		
		
		$.getJSON(webroot+'contacts/get_cities/'+$(this).val(), {ajax: 'true'}, function(j){
			
			var select = $('#AddressCityId');
	        var options = select.attr('options');
	        
	        $('option', select).remove();
	        
	        option = new Option("Todos", "");
        	options.add(option);
        	
	        $.each(j, function(index, value) {
	        	option = new Option(value, index);
	        	options.add(option);
	        	//options[index] = new Option(value);
	        });
	        
	        
	        $('#cep_loading').fadeOut('slow');
		});				
	});
});