$(document).ready(function() {
	
	$("#all_check").click(function(){
		
		var checked = $(this).attr('checked');
		
		$(".row_check").each(function(){
			
			$(this).attr('checked', checked);
		});		 
	});

	var contact_id;
	
	$("#send_button").click(function(){
						
		$(".row_check:checked").each(function(){
		
			contact_id = $(this).val();			
			
			$.ajax({
				  type:"GET",
				  async: false,
				  url: webroot+'campaigns/do_send/'+$(this).val()+'/'+$("#campaign_id").val()+'/'+$("#sender_id").val(),
				  success:function(data){
					 
					if (data == 1) {
									
						$("#status_"+contact_id).html('Enviado!');					  	
						$("#status_"+contact_id).css('color', 'green');
					}
					else {
						
						$("#status_"+contact_id).html('Erro!');					  	
						$("#status_"+contact_id).css('color', 'red');
					}
				  },
				  complete: function() {
					 				 
				  },
				  beforeSend: function(){
					  
					  $("#status_"+contact_id).html('Enviando...');
					  $("#status_"+contact_id).css('color', 'blue');
				  }
			});
			
		});
		
		alert('Emails enviados!');
		
		return false;
	});
		
});