$(document).ready(function() {
	
	$('.input_int').each(function() {
		
		$(this).keyup(function(){
			
			if (!$(this).val().match(/^(\d{1,3})?\.?(\d{1,2})?$/)) {
				
				$(this).val("");				
				alert('Use somente numeros!');
			}		
		});	
	});
			
});