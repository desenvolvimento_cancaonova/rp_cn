$(document).ready(function() {
	
	$('.delete_media').each(function() {
	
		$(this).click(function() {
		
			var media_id = $(this).attr('media_id');
						
			$("#media_"+media_id).remove();
			
			$.ajax({
				  type:"GET", 
				  url: webroot+'contacts/delete_image/'+media_id,
				  success:function(data){},
				  complete: function() {},
				  beforeSend: function(){}
			});
		});
	});
	
	$('.medias_images_a_class').lightBox({
		imageLoading: webroot+'img/lightbox-ico-loading.gif',
		imageBtnClose: webroot+'img/lightbox-btn-close.gif',
		imageBtnPrev: webroot+'img/lightbox-btn-prev.gif',
		imageBtnNext: webroot+'img/lightbox-btn-next.gif',
		imageBtnBlank: webroot+'img/lightbox-blank.gif'
	});
});