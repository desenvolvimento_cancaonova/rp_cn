// Register the related commands.
FCKCommands.RegisterCommand( 'EmbedMovies', new FCKDialogCommand( FCKLang['EmbedMoviesDlgTitle'], FCKLang['EmbedMoviesDlgTitle'], FCKConfig.PluginsPath + 'EmbedMovies/embedmovies.html', 450, 350 ) ) ;

// Create the "EmbedMovies" toolbar button.
var oFindItem		= new FCKToolbarButton( 'EmbedMovies', FCKLang['EmbedMoviesTip'] ) ;
oFindItem.IconPath	= FCKConfig.PluginsPath + 'EmbedMovies/embedmovies.gif' ;

FCKToolbarItems.RegisterItem( 'EmbedMovies', oFindItem ) ;			// 'embedMovies' is the name used in the Toolbar config.
