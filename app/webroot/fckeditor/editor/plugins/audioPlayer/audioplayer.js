﻿var dialog		= window.parent ;
var oEditor		= dialog.InnerDialogLoaded() ;
var FCK			= oEditor.FCK ;
var FCKLang		= oEditor.FCKLang ;
var FCKConfig	= oEditor.FCKConfig ;

//security RegExp
var REG_SCRIPT = new RegExp("< *script.*>|< *style.*>|< *link.*>|< *body .*>", "i");
var REG_PROTOCOL = new RegExp("javascript:|vbscript:|about:", "i");
var REG_CALL_SCRIPT = new RegExp("&\{.*\};", "i");
var REG_EVENT = new RegExp("onError|onUnload|onBlur|onFocus|onClick|onMouseOver|onMouseOut|onSubmit|onReset|onChange|onSelect|onAbort", "i");
// Cookie Basic
var REG_AUTH = new RegExp("document\.cookie|Microsoft\.XMLHTTP", "i");
// TEXTAREA
var REG_NEWLINE = new RegExp("\x0d|\x0a", "i");

//#### Dialog Tabs

// Set the dialog tabs.
dialog.AddTab( 'Info', oEditor.FCKLang.DlgInfoTab ) ;

// Get the selected flash embed (if available).
var oFakeImage = FCK.Selection.GetSelectedElement() ;
var oEmbed ;

window.onload = function()
{
	// Translate the dialog box texts.
	oEditor.FCKLanguageManager.TranslatePage(document) ;

	dialog.SetAutoSize( true ) ;

	// Activate the "OK" button.
	dialog.SetOkButton( true ) ;

	//SelectField( 'txtUrl' ) ;
}

//#### The OK button was hit.
function Ok()
{
	var span = document.createElement('span');
	span.innerHTML = createElement();
	
	span.setAttribute( '_fckflash', 'true', 0 );
	
	oFakeImage	= oEditor.FCKDocumentProcessor_CreateFakeImage( 'FCK__Flash',  span) ;
	oFakeImage.setAttribute( '_fckflash', 'true', 0 ) ;
	
	oFakeImage	= FCK.InsertElement( oFakeImage ) ;
	
	oEditor.FCKEmbedAndObjectProcessor.RefreshView( oFakeImage, span ) ;
	
	return true;
}

function createElement()
{
	var path = oEditor.FCKConfig.PluginsPath+'audioPlayer/';
	var file = GetE('txtUrl').value;
	var html = '';
	html += '<object type="application/x-shockwave-flash" data="'+path+'player.swf" width="290" height="24" id="audioplayer1">';
	html += '<param name="movie" value="'+path+'player.swf" />';
	html += '<param name="FlashVars" value="playerID=1&amp;bg=0xf8f8f8&amp;leftbg=0xeeeeee&amp;lefticon=0x3094CF&amp;rightbg=0xcccccc&amp;rightbghover=0x999999&amp;righticon=0x3094CF&amp;righticonhover=0xffffff&amp;text=0x666666&amp;slider=0x666666&amp;track=0xFFFFFF&amp;border=0x666666&amp;loader=0xA7D1EB&amp;soundFile='+escape(file)+'" />';
	html += '<param name="quality" value="high" />';
	html += '<param name="menu" value="false" />';
	html += '<param name="wmode" value="transparent" />';
	html += '</object>';
	
	return html;
}