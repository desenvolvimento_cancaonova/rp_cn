﻿/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 */

// Register the related commands.
FCKCommands.RegisterCommand( 'AudioPlayer' , new FCKDialogCommand( 'AudioPlayer1', 'Audio Player', FCKConfig.PluginsPath + 'audioPlayer/setup.html' , 440, 370 ) ) ;

// Create the "Find" toolbar button.
var oFindItem = new FCKToolbarButton( 'AudioPlayer', 'Adicionar MP3') ;
oFindItem.IconPath = FCKConfig.PluginsPath + 'audioPlayer/sound_icon.png' ;

FCKToolbarItems.RegisterItem( 'AudioPlayer', oFindItem ) ; // 'My_Find' is the name used in the Toolbar config.