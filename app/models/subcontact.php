<?php
class Subcontact extends AppModel {

	var $name = 'Subcontact';	
		
	var $belongsTo = array('Contact', 'SubcontactRow'=>array('className'=>'Contact', 'foreignKey' => 'subcontact_id'));
}
?>