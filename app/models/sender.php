<?php
class Sender extends AppModel {

	var $name = 'Sender';
	var $validate = array(		
		'nome' => array(
			'requerido' => array(
				'rule' => array('notEmpty'),				
				'message' => REQUIRED_FIELD
			)	
		),
		'unico' => array(
			'rule' => array('isUnique'),		
			'message' => 'Nome existente.'
		)
	);
}
?>