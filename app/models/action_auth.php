<?php
class ActionAuth extends AppModel {

	var $name = 'ActionAuth';
	var $useTable = 'action_auth';
	var $validate = array(
		'controller_auth_id' => array('numeric'),
		'name' => array('notempty'),
		'show_name' => array('notempty'),
		'show' => array('numeric'),
		'allowed' => array('numeric')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'ControllerAuth' => array(
			'className' => 'ControllerAuth',
			'foreignKey' => 'controller_auth_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'user_action_auth',
			'foreignKey' => 'action_auth_id',
			'associationForeignKey' => 'user_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	var $actsAs = array('Containable');
}
?>