<?php

require (LIBS . 'model' . DS . 'datasources' . DS . 'dbo' . DS . 'dbo_mysql.php');

class DboMysqlWithLog extends DboMysql {

	function _execute($sql) {
	  
		if (Configure::read('debug') == 1)
    		$this->log($sql, 'sql_debug');
    
    	return parent::_execute($sql);
	}
}

?>