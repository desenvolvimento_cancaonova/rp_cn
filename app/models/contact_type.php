<?php
class ContactType extends AppModel {

	var $name = 'ContactType';
	var $validate = array(		
		'nome' => array(
			'requerido' => array(
				'rule' => array('notEmpty'),				
				'message' => REQUIRED_FIELD
			)	
		),
		'unico' => array(
			'rule' => array('isUnique'),		
			'message' => 'Nome existente.'
		)
	);
}
?>