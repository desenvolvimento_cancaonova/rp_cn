<?php

define('CONTACT_KIND_CONTACT', 1);
define('CONTACT_KIND_CONJUGE', 2);
define('CONTACT_KIND_SUBCONTACT', 3);

class Contact extends AppModel {

	var $name = 'Contact';	
	var $validate = array(		
		'nome' => array(
			'requerido' => array(
				'rule' => array('notEmpty'),				
				'message' => REQUIRED_FIELD
			)
		)		
	);

	var $sexo = array('Masculino'=>'Masculino', 'Feminino'=>'Feminino');
	var $estados_civil = array('Solteiro'=>'Solteiro', 'Casado'=>'Casado', 'Divorciado'=>'Divorciado', 'Viuvo'=>'Viuvo');
	
	var $contact_kind = array(CONTACT_KIND_CONTACT=>'Contato', CONTACT_KIND_CONJUGE=>'Conjuge', CONTACT_KIND_SUBCONTACT=>'Sub Contato');
		
	var $belongsTo = array(	'Conjuge'=>array('className'=>'Contact', 'foreignKey'=>'conjuge_id'),
							'Occupation',
							'ContactType',
							'User'							
					);
					
	var $hasOne = array('AddressMain'=>array('className'=>'Address', 'foreignKey'=>'contact_id', 'conditions'=>array('AddressMain.main'=>1)), 
						'AddressComercial' => array('className'=>'Address', 'foreignKey'=>'contact_id', 'conditions'=>array('AddressComercial.address_name'=>'Comercial')),
						'AddressResidencial' => array('className'=>'Address', 'foreignKey'=>'contact_id', 'conditions'=>array('AddressResidencial.address_name'=>'Residencial')),
						'MediaGalleryMain'=>array('className'=>'MediaGallery', 'foreignKey'=>'contact_id', 'conditions'=>array('MediaGalleryMain.main'=>1)),
						'ParentConjuge'=>array('className'=>'Contact', 'foreignKey'=>'conjuge_id', 'conditions'=>array(), 'limit'=>1),
						'ParentSubContact'=>array('className'=>'Subcontact', 'foreignKey'=>'subcontact_id', 'conditions'=>array(), 'limit'=>1),
					);
	
	var $hasMany = array('Address', 'MediaGallery', 'Subcontact');
	
	var $actsAs = array('Containable');
	
	function beforeSave() {
			
		//saving user_id from the session
		App::Import('Component', 'Session');
		$session = new SessionComponent();
		
		$this->data['Contact']['user_id'] = $session->read('Auth.User.id');
		
		return true;
	}	
}
?>