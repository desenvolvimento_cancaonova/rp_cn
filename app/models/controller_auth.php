<?php
class ControllerAuth extends AppModel {

	var $name = 'ControllerAuth';
	var $useTable = 'controller_auth';
	var $validate = array(
		'name' => array('notempty'),
		'show_name' => array('notempty'),
		'show' => array('numeric'),
		'allowed' => array('numeric')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'ActionAuth' => array(
			'className' => 'ActionAuth',
			'foreignKey' => 'controller_auth_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>