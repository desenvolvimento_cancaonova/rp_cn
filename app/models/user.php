<?php
class User extends AppModel {

	var $name = 'User';
	var $validate = array(		
		'nome' => array(
			'requerido' => array(
				'rule' => array('notEmpty'),				
				'message' => REQUIRED_FIELD
			),
			'unico' => array(
				'rule' => array('isUnique'),		
				'message' => 'Nome existente.'
			)			
		),
		'username' => array(
			'requerido' => array(
				'rule' => array('notEmpty'),				
				'message' => REQUIRED_FIELD
			),
			'unico' => array(
				'rule' => array('isUnique'),		
				'message' => 'Usuario existente.'
			)		
		),
		'senha' => array(
			'rule' => array('notEmpty'),			
			'message' => REQUIRED_FIELD,
			'on' => 'create'
		)
	);
	
	var $actsAs = array('Containable');
}
?>