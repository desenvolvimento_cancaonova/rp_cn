<?php

class Address extends AppModel {

	var $name = 'Address';

	var $belongsTo = array('Contact', 'City', 'State');

	var $estados = array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapa","BA"=>"Bahia","CE"=>"Ceara","DF"=>"Distrito Federal","ES"=>"Espirito Santo","GO"=>"Goias","MA"=>"Maranhao","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Para","PB"=>"Paraiba","PR"=>"Parana","PE"=>"Pernambuco","PI"=>"Piaui","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondonia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"Sao Paulo","TO"=>"Tocantins");
	var $address_names = array('Residencial'=>'Residencial', 'Comercial'=>'Comercial', 'Outros'=>'Outros');
	
	var $actsAs = array('Containable');
}
?>