<?php
class UserActionAuth extends AppModel {

	var $name = 'UserActionAuth';
	var $useTable = 'user_action_auth';
	var $validate = array(
		'user_id' => array('numeric'),
		'action_auth_id' => array('numeric')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ActionAuth' => array(
			'className' => 'ActionAuth',
			'foreignKey' => 'action_auth_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>