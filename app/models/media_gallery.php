<?php

class MediaGallery extends AppModel {

	var $sizes = array(
				'thumb'	=>	array(	'width' => '60',
									'height'=> '60',
									'path'	=> MEDIAS_IMAGES_THUMB_PATH),
				'medium'=>	array(	'width' => '200',
									'height'=> '200',
									'path'	=> MEDIAS_IMAGES_MEDIUM_PATH),
				'big'	=>	array(	'width' => '500',
									'height'=> '500',
									'path'	=> MEDIAS_IMAGES_BIG_PATH)
			);
			
	
	/**
	 * AfterFind Callback
	 * Manage results before send to find return
	 * @param array $results
	 */
	public function afterFind($results) {

		
		if (empty($results['id'])) { 
		
			foreach ($results as $key=>$result) {
				
				if (is_array($result)) {
					 
					$model_key = key($result);	
										
					$results[$key][$model_key]['images'] = $this->make_images_url($result[$model_key]['id']);
				}
			}
		}
		else {
										
			$results['images'] = $this->make_images_url($results['id']);		
		}
				
		return $results;
	}
	
	private function make_images_url($id) {
						
		$tmp = array();
		
		$image = '/'.MEDIAS_IMAGES_PATH.$id.'.jpg';
		
		if (file_exists(WWW_ROOT.$image))
			$tmp = array('original'=>$image);
		else
			$tmp = array('original'=>'/'.MEDIAS_IMAGES_PATH.'noimage.jpg');	
		
		foreach ($this->sizes as $name=>$size) {
			
			//$tmp[$name] = 'http://'.$_SERVER['SERVER_NAME'].'/'. ?????? ./'.$size['path'].$result[$model_key]['id'].'.jpg';
			
			$image = '/'.$size['path'].$id.'.jpg';			
			
			if (!file_exists(WWW_ROOT.$image))
				$tmp[$name] = '/'.MEDIAS_IMAGES_PATH.'noimage.jpg';
			else
				$tmp[$name] = $image;
		}
			
		return $tmp;
	}
}

?>
