<?php
/* SVN FILE: $Id$ */
/**
 * Short description for file.
 *
 * Long description for file
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 *
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php is loaded
 * This is an application wide file to load any function that is not used within a class define.
 * You can also use this to include or require any files in your application.
 *
 */
/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * $modelPaths = array('full path to models', 'second full path to models', 'etc...');
 * $viewPaths = array('this path to views', 'second full path to views', 'etc...');
 * $controllerPaths = array('this path to controllers', 'second full path to controllers', 'etc...');
 *
 */
//EOF

date_default_timezone_set('America/Sao_Paulo');

//User messages
define('USER_LOGIN_ERROR_MESSAGE', 'Dados incorretos! Tente novamente.');
define('USER_AUTH_ERROR_MESSAGE', 'Voc&ecirc; n&atilde;o tem permiss&atilde;o para esta a&ccedil;&atilde;o.');

//Messages
define('REQUIRED_FIELD', 'Campo Requerido.');

define('ROW_SAVED_SUCCESS', 'Registro salvo com sucesso!');
define('ROW_SAVED_ERROR', 'Erro ao salvar registro! Tente novamente.');
define('ROW_INVALID', 'Registro inv&aacute;lido!');
define('ROW_DELETED', 'Registro apagado com sucesso!');


define('RADIOS_IMAGE_PATH', '/files/radios/imagem/');


define('MININUM_DAYS_RAFFLE', 20);

//Main uploaded files path
define('UPLOADED_FILES_PATH', 'files/');

//Medias paths
define('MEDIAS_IMAGES_PATH', UPLOADED_FILES_PATH.'medias/');
define('MEDIAS_IMAGES_THUMB_PATH', MEDIAS_IMAGES_PATH.'thumb/');
define('MEDIAS_IMAGES_MEDIUM_PATH', MEDIAS_IMAGES_PATH.'medium/');
define('MEDIAS_IMAGES_BIG_PATH', MEDIAS_IMAGES_PATH.'big/');
?>