<div class="campaigns index">
<h2><?php __('Campanha');?></h2>

<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('titulo');?></th>
	<th><?php echo $paginator->sort('Data Cadastro', 'created');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($campaigns as $campaign):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td width="20px" align="center">
			<?php echo $campaign['Campaign']['id']; ?>
		</td>
		<td>
			<?php echo $campaign['Campaign']['titulo']; ?>
		</td>
		<td align='center' width="130px">
			<?php echo date('d/m/Y h:i:s', strtotime($campaign['Campaign']['created'])); ?>
		</td>
		<td class="actions" width="120px" align="center">
			<?php echo $html->link(__('Ver', true), array('action' => 'view', $campaign['Campaign']['id'])); ?>
			<?php echo $html->link(__('Editar', true), array('action' => 'edit', $campaign['Campaign']['id'])); ?>
			<?php echo $html->link(__('Apagar', true), array('action' => 'delete', $campaign['Campaign']['id']), array('escape'=>false), sprintf(__('Confirma exclus&atilde;o do registro #%s?', true), $campaign['Campaign']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<?php echo $this->element('pagination'); ?>