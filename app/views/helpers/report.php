<?php

App::Import('Vendor', 'fpdf/fpdf');

if (!defined('PARAGRAPH_STRING')) define('PARAGRAPH_STRING', '~~~');

class ReportHelper extends AppHelper {

	public $pdf;
	
	function initialize($w = 'P', $unit = 'mm', $format = 'A4') {

		$this->pdf = new PDF(null, $w, $unit, $format);
	}
}

class PDF extends FPDF {
	
	var $report_type;
	var $header = true;
	var $footer = true;
	
	function PDF($w, $unit, $format) {
		
		parent::FPDF($w, $unit, $format);
	}
	
	function set_report_type($report_type) {

		$this->report_type = html_entity_decode($report_type);
	}

	function set_header($bool = true) {

		$this->header = $bool;
	}
	function set_footer($bool = true) {

		$this->footer = $bool;
	}	
	//Page header
	function Header() {
		
		if ($this->header) {

			//Arial bold 15
			$this->SetFont('Arial', 'B', 15);
	
			//Title
			$this->Cell(0, 10, 'Cancao Nova - Relacoes Publicas', 0, 1, 'C');
			$this->Cell(0, 10, $this->report_type, 0, 0, 'C');
	
			//Line break
			$this->Ln(15);
		}
	}

	//Page footer
	function Footer() {

		if ($this->footer) {
				
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
	
			//Arial italic 8
			$this->SetFont('Arial', 'I', 8);
	
			//Page number
			$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}', 0, 0,'C');
		}
	}
}

?>