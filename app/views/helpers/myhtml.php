<?php
class MyhtmlHelper extends AppHelper {

	function link($title, $url = NULL, $htmlAttributes = array(), $confirmMessage = false, $escapeTitle = true) {
		
		App::Import('Helper', array('Html'));
		$html = new HtmlHelper();

		$uri = null;
		if (is_array($url)) {
			
			if (empty($url['controller']))
				$controller = $this->params['controller'];
			else
				$controller = $url['controller'];
			
			if (empty($url['action']))
				$action = $this->params['action'];
			else
				$action = $url['action'];
				
			$uri = $controller.'/'.$action;			
		}
		
		return $html->link($title, $url, $htmlAttributes, $confirmMessage, $escapeTitle);
	}
}
?>