<?php
class UtilHelper extends Helper
{
	var $helpers = array('Html', 'Form', 'Javascript');
	
	function fckEditor($fieldName,  $options = array())
	{
		$options = array_merge(array('attributes'=>array(), 'toolbar' => 'Default2', 'width' => '100%', 'height' => 600), $options);
		
		$attributes = $options['attributes'] = $this->_initInputField($fieldName, $options['attributes']);
		unset($attributes['name']);

		$baseDir = $this->webroot("/fckeditor/editor/");
		
		$file = $baseDir . "fckeditor.html?InstanceName={$options['attributes']["id"]}&amp;Toolbar={$options['toolbar']}";

		$html = "\n\n\n".'<div class="input required">';
		$html .= @$options['label']===false?'':((!empty($options['label']) || @$options['label'] != false)?$options['label']:$this->Form->label($fieldName));
		$html .= '<div class="fckeditor">';
		$html .= "<iframe id=\"{$options['attributes']['id']}___Frame\" src=\"{$file}\" width=\"{$options['width']}\" height=\"{$options['height']}\" frameborder=\"0\" scrolling=\"no\"></iframe>" ;
		$html .= sprintf($this->Html->tags['hidden'], $options['attributes']['name'], $this->_parseAttributes($attributes));
		$html .= '</div>';
		$html .= $this->Form->error($fieldName);
		$html .= '</div>'."\n\n\n";
		
		$view =& ClassRegistry::getObject('view');
		$view->addScript($this->Javascript->link('fckeditor'));
		
		return  $html;
	}
	
	function printBool($bool)
	{
		if($bool){
			return("yes");
		}else{
			return("no");
		}
	}

	function checkbox($fieldName, $options = array(), $selected = null, $attributes = array(), $validate = "") {
		$select = array();
		$attributes = $this->__initInputField($fieldName, $attributes);

		if (empty($options)) {
			$view =& ClassRegistry::getObject('view');
			$varName = Inflector::variable(Inflector::pluralize(preg_replace('/_id$/', '', $this->field())));
			$options = $view->getVar($varName);
		}

		if (!isset($selected)) {
			$selected = $attributes['value'];
		}
		
		$tag = $this->Html->tags['checkbox'];

		$out = array();
		$i = 0;
		foreach($options as $id => $name) {
			$optionsHere = $attributes;

			if ((($selected !== null) && ($selected == $id)) || (is_array($selected) && in_array($id, $selected))) {
				$optionsHere['checked'] = 'checked';
			}
			$optionsHere['value'] = $id;
			$label = sprintf($this->Html->tags['label'], $attributes["id"].$id, null, $name);

			if($i == 0 && !empty($validate)){
				if(isSet($optionsHere['class'])){
					$optionsHere['class'] .= $validate;
				}else{
					$optionsHere['class'] = $validate;
				}
			}
			$optionsHere['id'] = $attributes["id"].$id;
			unset($optionsHere['name']);

			$out[] = "<li><label form=\"{$optionsHere['id']}\">" . sprintf($tag, $attributes["name"]."[]", $this->_parseAttributes($optionsHere)) . $name . "</label></li>\n";
			$i++;
		}
		return "\n<ul class=\"habtm\">\n" . $this->output(implode($out)) . "</ul>\n";
	}
	
	function radio($fieldName, $options = array(), $selected = null, $attributes = array()) {
		$select = array();
		$attributes = $this->__initInputField($fieldName, $attributes);
		
		if (!is_array($options)) {
			return null;
		}

		if (!isset($selected)) {
			$selected = $attributes['value'];
		}
		
		$tag = $this->Html->tags['radio'];
		
		$i = 0;
		$out = array();
		foreach($options as $id => $name) {
			$optionsYes = $attributes;
			$optionsNo = $optionsYes;
			
			if ((($selected !== null) && ($selected == $id)) || (is_array($selected) && in_array($id, $selected))) {
				$optionsYes['checked'] = 'checked';
			}else{
				$optionsNo['checked'] = 'checked';
			}
			
			$labelYes = sprintf($this->Html->tags['label'], $attributes["name"].$i."yes", null, "Yes");
			$labelNo = sprintf($this->Html->tags['label'], $attributes["name"].$i."no", null, "No");
			$optionsYes['value'] = $id;
			$optionsNo['value'] = "";

			$html = "";
			$html .= "<tr>\n";
			$html .= "<td style=\"text-align:right;\">" . $name . ":</td>\n";
			$html .= "<td>" . sprintf($tag, $attributes["name"]."[".$i."]", $attributes["name"].$i."yes", $this->_parseAttributes($optionsYes), $labelYes) . "</td>\n";
			$html .= "<td>" . sprintf($tag, $attributes["name"]."[".$i."]", $attributes["name"].$i."no", $this->_parseAttributes($optionsNo), $labelNo) . "</td>\n";
			$html .= "</tr>\n";
			
			$out[] =  $html;
			$i++;
		}

		return "\n<table cellpadding=\"0\" cellspacing=\"0\" class=\"habtm\">\n" . $this->output(implode($out)) . "</table>\n";
	}

	function listCheck($fieldName, $options, $selected = null, $attributes = null, $return = false) {
		$checkbox = '<input type="checkbox" name="data[%s][%s][]" id="%s" %s /> <label for="%s">%s</label>';
		
		$this->setFormTag($fieldName);
		
		if (!is_array($options)) {
			return null;
		}

		if (!isset($selected)) {
			$selected = $this->value($fieldName);
		}

		$fathers = $options[0];
		unset($options[0]);
		
		$i = 0;
		$checkboxs = array();
		
		foreach($options as $name => $option) {
			$optionsPai = $attributes;
			
			$id = array_search($name, $fathers);
			
			if($id){
				if ((($selected !== null) && ($selected == $id)) && (is_array($selected) && array_key_exists($name, $selected))) {
					$optionsPai['checked'] = 'checked';
				} else if (is_array($selected) && array_key_exists($id, $selected)) {
					$optionsPai['checked'] = 'checked';
				}
				
				$optionsPai['value'] = $id;
			}
			
			$html = "";
			$html .= "	<dl>\n";
			if($id){
				$html .= "		<dt>" . sprintf($checkbox, $this->field(), $this->field(), $this->model().$this->field().$i, $this->_parseAttributes($optionsPai), $this->model().$this->field().$i, $name) ."</dt>\n";
			}else{
				$html .= "		<dt>" . $name ."</dt>\n";
			}
			
			$a = 0;
			foreach($option as $id => $name) {
				$optionsFilho = $attributes;
				if ((($selected !== null) && ($selected == $id)) && (is_array($selected) && array_key_exists($name, $selected))) {
					$optionsFilho['checked'] = 'checked';
				} else if (is_array($selected) && array_key_exists($id, $selected)) {
					$optionsFilho['checked'] = 'checked';
				}
				
				$optionsFilho['value'] = $id;
				
				$html .= "		<dd>" . sprintf($checkbox, $this->field(), $this->field(), $this->model().$this->field().$i.$a, $this->_parseAttributes($optionsFilho), $this->model().$this->field().$i.$a, $name) . "</dd>\n";
				$a++;
			}
			$html .= "	</dl>\n";
			
			$checkboxs[] = $html;
			$i++;
		}

		return "\n<div class=\"habtm\">\n" . $this->output(implode($checkboxs), $return) . "</div>\n";
	}

	function image($file, $options = array()){
		if(file_exists(WWW_ROOT.$file)){
			return $this->Html->image($file, $options);
		}
		return '';
	}
	
	function img($file, $options = array()){
		if(file_exists(WWW_ROOT.$file)){
			return $this->Html->image($file, $options);
		}
		
		return $this->Html->image(ereg_replace("([0-9]){1,}", "0", $file), $options);
	}
	
}
?>
