<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="pt-br" />
	
	
	<title><?php echo $title_for_layout?></title>
	
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

	<?php

		echo $html->css(array('default', 'theme'));

		echo $javascript->link(array('jquery', 'default'));

		echo $javascript->codeBlock('var webroot = "'.$html->url('/').'";');

		echo $scripts_for_layout;
	?>
	
	<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/ie-sucks.css" />
	<![endif]-->
	
</head>

<body>
	<div id="container">
    	<div id="header">
        	<h2>Rela&ccedil;&otilde;es P&uacute;blicas Can&ccedil;&atilde;o Nova</h2>

<?php if ($this->action != "login"): ?>

    <div id="topmenu">
            	<ul>
            	<?php if ($admin || in_array('pages', $main_menu)): ?>
                	<li <?php echo $this->params['controller']=='pages'?'class="current"':'' ?>><?php echo $html->link('Home', array('controller'=>'pages', 'action'=>'home'), array('escape'=>false)); ?></li>
                <?php endif; ?>
            	<?php if ($admin || in_array('contacts', $main_menu)): ?>
            		<li <?php echo $this->params['controller']=='contacts'?'class="current"':'' ?>><?php echo $html->link('Contatos', array('controller'=>'contacts', 'action'=>'index'), array('escape' => false)); ?></li>
                <?php endif; ?>
            	<?php if ($admin || in_array('occupations', $main_menu)): ?>
                	<li <?php echo $this->params['controller']=='occupations'?'class="current"':'' ?>><?php echo $html->link('Cargos', array('controller'=>'occupations', 'action'=>'index'), array('escape' => false)); ?></li>                    
                <?php endif; ?>
                <?php if ($admin || in_array('contact_types', $main_menu)): ?>
                	<li <?php echo $this->params['controller']=='contact_types'?'class="current"':'' ?>><?php echo $html->link('Tipos de Contato', array('controller'=>'contact_types', 'action'=>'index'), array('escape' => false)); ?></li>
                <?php endif; ?>
                <?php if ($admin || in_array('labels', $main_menu)): ?>
                	<li <?php echo $this->params['controller']=='labels'?'class="current"':'' ?>><?php echo $html->link('Etiquetas', array('controller'=>'labels', 'action'=>'index'), array('escape' => false)); ?></li>                    
                <?php endif; ?>                
                <?php if ($admin || in_array('reports', $main_menu)): ?>
                	<li <?php echo $this->params['controller']=='reports'?'class="current"':'' ?>><?php echo $html->link('Relat&oacute;rios', array('controller'=>'reports', 'action'=>'index'), array('escape'=>false)); ?></li>
                <?php endif; ?>
				<?php if ($admin || in_array('campaigns', $main_menu) || in_array('senders', $main_menu)): ?>
                	<li <?php echo $this->params['controller']=='campaigns'?'class="current"':'' ?>><?php echo $html->link('Email Mkt', array('controller'=>'campaigns', 'action'=>'index'), array('escape'=>false)); ?></li>
                <?php endif; ?>
				<?php if ($admin || in_array('users', $main_menu)): ?>
                	<li <?php echo $this->params['controller']=='users'?'class="current"':'' ?>><?php echo $html->link('Usu&aacute;rios', array('controller'=>'users', 'action'=>'index'), array('escape'=>false)); ?></li>
                <?php endif; ?>                                
                <li <?php echo $this->params['controller']=='none'?'class="current"':'' ?>><?php echo $html->link('Sair', array('controller'=>'users', 'action'=>'logout'), array('escape'=>false), 'Deseja realmente Sair?'); ?></li>
              </ul>
          </div>
      </div>
      <div id="top-panel">
            <div id="panel">
                                
                <?php if ($this->params['controller'] == 'contacts' ): ?>
                
                	<div class="actions">
						<ul>
							<li><?php echo $html->link(__('Novo Contato', true), array('action' => 'add'), array('class'=>'useradd')); ?> </li>
							<li><?php echo $html->link(__('Listar', true), array('action' => 'index'), array('class'=>'report')); ?> </li>
							<li> &nbsp; &nbsp; &nbsp; </li>
						<?php if (!in_array($this->action, array('add', 'index'))): ?>
							<li><?php echo $html->link(__('Editar Dados', true), array('action' => 'edit', $contact['Contact']['id']), array('class'=>'useredit')); ?> </li>
							<li><?php echo $html->link(__('Conjuge', true), array('action' => 'conjuge', $contact['Contact']['id']), array('class'=>'useredit')); ?> </li>
							<li><?php echo $html->link(__('Sub Contatos', true), array('action' => 'subcontacts', $contact['Contact']['id']), array('class'=>'useredit')); ?> </li>
							<li><?php echo $html->link(__('Imagens', true), array('action' => 'add_image', $contact['Contact']['id']), array('class'=>'useredit')); ?> </li>							
							<li><?php //echo $html->link(__('Apagar', true), array('action' => 'delete', $client['Client']['id']), array('class'=>'userdelete', 'escape'=>false), sprintf('Confirma exclusao do registro #%s?', $client['Client']['id'])); ?> </li>							
						<?php endif; ?>
						</ul>
					</div>
	                                
                <?php elseif ($this->params['controller'] == 'occupations'): ?>
		
					<div class="actions">
						<ul>							
							<li><?php echo $html->link(__('Novo Cargo', true), array('action' => 'add'), array('class'=>'useradd')); ?> </li>
							<li><?php echo $html->link(__('Listar', true), array('action' => 'index'), array('class'=>'report')); ?> </li>
							<li> &nbsp; &nbsp; &nbsp; </li>							
						<?php if (!in_array($this->action, array('add', 'index'))): ?>
							<li><?php echo $html->link(__('Ver', true), array('action' => 'view', $occupations['Occupation']['id']), array('class'=>'search')); ?> </li>
							<li><?php echo $html->link(__('Editar', true), array('action' => 'edit', $occupations['Occupation']['id']), array('class'=>'useredit')); ?> </li>
							<li><?php echo $html->link(__('Apagar', true), array('action' => 'delete', $occupations['Occupation']['id']), array('class'=>'userdelete', 'escape'=>false), sprintf('Confirma exclusao do registro #%s?', $occupations['Occupation']['id'])); ?> </li>
						<?php endif; ?>	
						</ul>
					</div>					

				<?php elseif ($this->params['controller'] == 'contact_types'): ?>
				
					<div class="actions">
						<ul>
							<li><?php echo $html->link(__('Novo Tipo de Contato', true), array('action' => 'add'), array('class'=>'useradd')); ?> </li>							
							<li><?php echo $html->link(__('Listar', true), array('action' => 'index'), array('class'=>'report')); ?> </li>
							<li> &nbsp; &nbsp; &nbsp; </li>
						<?php if (!in_array($this->action, array('add', 'index'))): ?>
							<li><?php echo $html->link(__('Ver', true), array('action' => 'view', $contact_type['ContactType']['id']), array('class'=>'search')); ?> </li>
							<li><?php echo $html->link(__('Editar', true), array('action' => 'edit', $contact_type['ContactType']['id']), array('class'=>'useredit')); ?> </li>
							<li><?php echo $html->link(__('Apagar', true), array('action' => 'delete', $contact_type['ContactType']['id']), array('class'=>'userdelete', 'escape'=>false), sprintf('Confirma exclusao do registro #%s?', $contact_type['ContactType']['id'])); ?> </li>
						<?php endif; ?>							
						</ul>
					</div>
				
				<?php elseif ($this->params['controller'] == 'labels'): ?>
		
					<div class="actions">
						<ul>							
							<li><?php echo $html->link(__('Nova Etiqueta', true), array('action' => 'add'), array('class'=>'useradd')); ?> </li>
							<li><?php echo $html->link(__('Listar', true), array('action' => 'index'), array('class'=>'report')); ?> </li>
							<li> &nbsp; &nbsp; &nbsp; </li>							
						<?php if (!in_array($this->action, array('add', 'index'))): ?>
							<li><?php echo $html->link(__('Ver', true), array('action' => 'view', $label['Label']['id']), array('class'=>'search')); ?> </li>
							<li><?php echo $html->link(__('Editar', true), array('action' => 'edit', $label['Label']['id']), array('class'=>'useredit')); ?> </li>
							<li><?php echo $html->link(__('Apagar', true), array('action' => 'delete', $label['Label']['id']), array('class'=>'userdelete', 'escape'=>false), sprintf('Confirma exclusao do registro #%s?', $label['Label']['id'])); ?> </li>
						<?php endif; ?>	
						</ul>
					</div>							
						
				<?php elseif ($this->params['controller'] == 'users'): ?>                                
					
					<div class="actions">
						<ul>
							<li><?php echo $html->link(__('Novo Usu&aacute;rio', true), array('action' => 'add'), array('escape'=>false, 'class'=>'useradd')); ?> </li>
							<li><?php echo $html->link(__('Listar', true), array('action' => 'index'), array('class'=>'report')); ?> </li>
							<li> &nbsp; &nbsp; &nbsp; </li>							
						<?php if (!in_array($this->action, array('add', 'index'))): ?>
							<li><?php echo $html->link(__('Ver', true), array('action' => 'view', $user['User']['id']), array('class'=>'search')); ?> </li>
							<li><?php echo $html->link(__('Editar', true), array('action' => 'edit', $user['User']['id']), array('class'=>'useredit')); ?> </li>
							<li><?php echo $html->link(__('Apagar', true), array('action' => 'delete', $user['User']['id']), array('class'=>'userdelete', 'escape'=>false), sprintf(__('Confirma exclus&atilde;o do registro #%s?', true), $user['User']['id'])); ?> </li>
						<?php endif; ?>
						</ul>
					</div>
					
				<?php elseif ($this->params['controller'] == 'campaigns' || $this->params['controller'] == 'senders'): ?>                                
					
					<div class="actions">
						<ul>
							<li><?php echo $html->link(__('Nova Campanha', true), array('controller'=>'campaigns', 'action' => 'add'), array('escape'=>false, 'class'=>'useradd')); ?> </li>
							<li><?php echo $html->link(__('Listar', true), array('controller'=>'campaigns', 'action' => 'index'), array('class'=>'report')); ?> </li>
							<li><?php echo $html->link(__('Remetentes', true), array('controller'=>'senders', 'action' => 'index'), array('class'=>'report')); ?> </li>
							<li><?php echo $html->link(__('Enviar Campanha', true), array('controller'=>'campaigns', 'action' => 'send'), array('class'=>'app_add')); ?> </li>													
							<li> &nbsp; &nbsp; &nbsp; </li>							
							<?php if ($this->params['controller'] == 'senders'): ?>
								<li><?php echo $html->link(__('Novo Remetente', true), array('controller'=>'senders', 'action' => 'add'), array('escape'=>false, 'class'=>'useradd')); ?> </li>
								<li><?php echo $html->link(__('Listar Remetentes', true), array('controller'=>'senders', 'action' => 'index'), array('class'=>'report')); ?> </li>
							<?php else: ?>
								<li> &nbsp; &nbsp; &nbsp; </li>
								<?php if (!in_array($this->action, array('add', 'index', 'send'))): ?>
								<li><?php echo $html->link(__('Ver', true), array('controller'=>$this->params['controller'], 'action' => 'view', $campaign['Campaign']['id']), array('class'=>'search')); ?> </li>
								<li><?php echo $html->link(__('Editar', true), array('controller'=>$this->params['controller'], 'action' => 'edit', $campaign['Campaign']['id']), array('class'=>'useredit')); ?> </li>
								<li><?php echo $html->link(__('Apagar', true), array('controller'=>$this->params['controller'], 'action' => 'delete', $campaign['Campaign']['id']), array('class'=>'userdelete', 'escape'=>false), sprintf(__('Confirma exclus&atilde;o do registro #%s?', true), $campaign['Campaign']['id'])); ?> </li>							
							<?php endif;?>
						<?php endif; ?>
						</ul>
					</div>
				
				<?php elseif ($this->params['controller'] == 'reports'): ?>
							
					<div class="actions">
						<ul>
						 
							<li><?php echo $html->link(__('Tipos de Relat&oacute;rios', true), array('action' => 'index'), array('class'=>'report', 'escape'=>false)); ?> </li>
						<?php /* ?>	
							<li><?php echo $html->link(__('Ganhadores', true), array('action' => 'winners'), array('class'=>'report', 'escape'=>false)); ?> </li>					
							<li><?php echo $html->link(__('Participa&ccedil;&otilde;es', true), array('action' => 'participations'), array('class'=>'report', 'escape'=>false)); ?> </li>
							<li><?php echo $html->link(__('Ouvintes Cadastrados', true), array('action' => 'listeners'), array('class'=>'report', 'escape'=>false)); ?> </li>
							<?php if ($this->action != 'index'): ?>
								<li><?php echo $html->link('VERS&Atilde;O PARA IMPRESS&Atilde;O', array('action' => $this->action, 'html'), array('class'=>'printer', 'escape'=>false, 'target'=>'_blank')); ?></li>
							<?php endif; ?>
						<?php */ ?>
						</ul>
					</div>
							
                <?php endif; ?>                
                
            </div>
      </div>
      
      <?php if ($this->action == 'admin_index') { echo $this->element('admin_search_bar'); } ?>
      
      <?php if ($session->check('Message.flash')): ?>
      <div id="msg-area">
            <?php echo $session->flash(); ?>
      </div>
      <?php endif; ?>
      
        <div id="wrapper">
            <div id="content">
                <?php echo $content_for_layout; ?>
            </div>
        
      </div>
                
      <div id="footer">
		<div id="credits">Rela&ccedil;&otilde;es P&uacute;blicas Can&ccedil;&atilde;o Nova</div>
		<br />
	  </div>
	  
<?php else: ?>

	<?php if ($session->check('Message.flash')): ?>
	<div id="msg-area">
		<?php echo $session->flash(); ?>
    </div>
    <?php endif; ?>
      
    <div id="wrapper">
		<div id="content">
        	<?php echo $content_for_layout; ?>
        </div>
	</div>

<?php endif; ?>
              
</div>

</body>
</html>

<?php echo $this->element('sql_dump'); ?>
