<?php
	$data = date('d_m_Y_hh_ii_ss_', strtotime('now'));
	 
	header('Content-Type: application/pdf, charset=utf-8');
	header('Content-Disposition: attachment; filename="'.$data.$type.'.pdf"'); 
	header("Pragma: no-cache");
	header("Cache-Control: no-store, no-cache, max-age=0, must-revalidate");
?>
<?php echo $content_for_layout ?>
