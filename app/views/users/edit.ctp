<div class="users form">
<?php echo $form->create('User');?>
	<fieldset>
 		<legend><?php __('Editar Usu&aacute;rio');?></legend>
		
		<?php echo $form->input('id'); ?>
		<?php echo $this->element('users/form'); ?>
		
		<?php echo $form->end('Salvar');?>
		
	</fieldset>

<?php if (empty($user['User']['admin'])): ?>
	<br />
	<fieldset>
		
		<legend>Permiss&otilde;es</legend>
		
		<ul id="permissoes">
		<?php foreach($controllers as $key=>$controller): ?>
			<?php if (!empty($controller['ActionAuth'])): ?>
			<?php /* ?><li class="perm_controller"><?php echo $controller['ControllerAuth']['show_name']; ?></li> <?php */ ?>
			<fieldset>
				<legend><?php echo $controller['ControllerAuth']['show_name']; ?></legend>
				<?php echo $form->input('all_'.$controller['ControllerAuth']['id'], array('type'=>'checkbox', 'label'=>false, 'div'=>false, 'class'=>'permission_all')); ?> Selecionar Todos
			<li>
				<ul>
				<?php foreach($controller['ActionAuth'] as $key2=>$action): ?>
					<?php if ($action['show'] == 1): ?>					
						<li class="field">
							<?php echo $form->input('action_'.$action['id'], array('controller'=>$controller['ControllerAuth']['show_name'], 'action'=>$action['name'], 'label'=>false, 'div'=>false, 'type'=>'checkbox', 'class'=>'input_actions', 'value'=>$action['id'], 'checked'=>in_array($action['id'], $permissions)?true:false)); ?>
						</li>
						<li class="text"><?php echo $action['show_name']; ?></li>
					<?php endif; ?>					
				<?php endforeach;  ?>
				</ul>
			</li>
			</fieldset>
			<br />
			<?php endif; ?>
		<?php endforeach;  ?>
		</ul>
		
	</fieldset>
<?php endif; ?>
</div>

<?php $this->addScript($javascript->link(array('users/form'))); ?>