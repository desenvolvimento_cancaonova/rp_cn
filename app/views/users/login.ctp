<div class="users form">
<?php echo $form->create('User', array('url'=>array('controller'=>'users', 'action'=>'login')));?>
	<fieldset>
 		<legend><?php __('Login do Usu&aacute;rio');?></legend>

		<?php echo $session->flash('auth'); ?>

		<?php echo $this->element('users/login'); ?>

		<?php echo $form->end('Logar');?>	
	</fieldset>

</div>