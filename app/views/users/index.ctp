<div class="users index">
<h2><?php __('Usu&aacute;rios');?></h2>

<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('Nome', 'name');?></th>
	<th><?php echo $paginator->sort('Usu&aacute;rio', 'username', array('escape'=>false));?></th>
	<th><?php echo $paginator->sort('Administrador', 'admin');?></th>
	<th><?php echo $paginator->sort('Data Cadastro', 'created');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($users as $user):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $user['User']['id']; ?>
		</td>
		<td>
			<?php echo $user['User']['nome']; ?>
		</td>
		<td>
			<?php echo $user['User']['username']; ?>
		</td>
		<td align="center">
			<?php echo $user['User']['admin']?'Sim':'N&atilde;o'; ?>
		</td>
		<td align="center">
			<?php echo date('d/m/Y H:i:s', strtotime($user['User']['created'])); ?>
		</td>
		<td class="actions" align="center">
			<?php echo $html->link(__('Ver', true), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $html->link(__('Editar', true), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $html->link(__('Apagar', true), array('action' => 'delete', $user['User']['id']), null, sprintf(__('Confirma exclus&atilde;o do registro #%s?', true), $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<?php echo $this->element('pagination'); ?>