<div class="contact_types index">
<h2><?php __('Tipos de Contato');?></h2>


<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('nome');?></th>	
	<th><?php echo $paginator->sort('Data Cadastro', 'created');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($contact_types as $contact_type):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td width="20px" align="center">
			<?php echo $contact_type['ContactType']['id']; ?>
		</td>
		<td>
			<?php echo $contact_type['ContactType']['nome']; ?>
		</td>		
		<td align='center' width="130px">
			<?php echo date('d/m/Y h:i:s', strtotime($contact_type['ContactType']['created'])); ?>
		</td>
		<td class="actions" width="120px" align="center">
			<?php echo $html->link(__('Ver', true), array('action' => 'view', $contact_type['ContactType']['id'])); ?>
			<?php echo $html->link(__('Editar', true), array('action' => 'edit', $contact_type['ContactType']['id'])); ?>
			<?php echo $html->link(__('Apagar', true), array('action' => 'delete', $contact_type['ContactType']['id']), array('escape'=>false), sprintf('Confirma exclusao do registro #%s?', $contact_type['ContactType']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<?php echo $this->element('pagination'); ?>