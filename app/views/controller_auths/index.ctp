<div class="controllerAuths index">
<h2><?php __('ControllerAuths');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('show_name');?></th>
	<th><?php echo $paginator->sort('show');?></th>
	<th><?php echo $paginator->sort('allowed');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($controllerAuths as $controllerAuth):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $controllerAuth['ControllerAuth']['id']; ?>
		</td>
		<td>
			<?php echo $controllerAuth['ControllerAuth']['name']; ?>
		</td>
		<td>
			<?php echo $controllerAuth['ControllerAuth']['show_name']; ?>
		</td>
		<td>
			<?php echo $controllerAuth['ControllerAuth']['show']; ?>
		</td>
		<td>
			<?php echo $controllerAuth['ControllerAuth']['allowed']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $controllerAuth['ControllerAuth']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $controllerAuth['ControllerAuth']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $controllerAuth['ControllerAuth']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $controllerAuth['ControllerAuth']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New ControllerAuth', true), array('action' => 'add')); ?></li>
		<li><?php echo $html->link(__('List Action Auths', true), array('controller' => 'action_auths', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Action Auth', true), array('controller' => 'action_auths', 'action' => 'add')); ?> </li>
	</ul>
</div>
