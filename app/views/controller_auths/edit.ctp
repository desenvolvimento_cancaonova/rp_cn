<div class="controllerAuths form">
<?php echo $form->create('ControllerAuth');?>
	<fieldset>
 		<legend><?php __('Edit ControllerAuth');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('name');
		echo $form->input('show_name');
		echo $form->input('show');
		echo $form->input('allowed');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('ControllerAuth.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ControllerAuth.id'))); ?></li>
		<li><?php echo $html->link(__('List ControllerAuths', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Action Auths', true), array('controller' => 'action_auths', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Action Auth', true), array('controller' => 'action_auths', 'action' => 'add')); ?> </li>
	</ul>
</div>
