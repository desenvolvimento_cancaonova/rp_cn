<?php
	
	//inicializando configs P para portrait, mm para milimetros, A4 para formato da pagina
	$report->initialize('L', 'mm', 'A4');
	//setando o titulo do relatorio
	$report->pdf->set_report_type('Relatorio Resumido');

    $report->pdf->AliasNbPages();
    
    //abrindo pagina
	$report->pdf->AddPage();
	
    $report->pdf->setTitle('Resumido');
    
    //cabecalho dos campos dos registros
    $report->pdf->SetFont('Arial', 'B', 10); 
    $report->pdf->Cell(25, 8, 'Tratamento', 1, 0);   
    $report->pdf->Cell(60, 8, 'Nome', 1, 0);
	$report->pdf->Cell(65, 8, 'Empresa', 1, 0);
	$report->pdf->Cell(30, 8, 'Cargo', 1, 0);
	$report->pdf->Cell(33, 8, 'Cidade', 1, 0);
	$report->pdf->Cell(60, 8, 'Pertence a:', 1, 1);
		
		
	//setando fonte para imprimir os registros
	$report->pdf->SetFont('Arial', '', 7);	
	foreach($contacts as $contact) {

		$report->pdf->Cell(25, 8, $contact['Contact']['tratamento'], 1, 0);
		$report->pdf->Cell(60, 8, $contact['Contact']['nome'] , 1, 0);
		$report->pdf->Cell(65, 8, $contact['Contact']['empresa'], 1, 0);
		$report->pdf->Cell(30, 8, @$contact['Occupation']['nome'], 1, 0);
		$report->pdf->Cell(33, 8, $contact['AddressMain']['cidade'].'-'.$contact['AddressMain']['uf'], 1, 0);
		

		if ($contact['Contact']['contact_kind'] == 1) {
			//$report->pdf->Cell(22, 8, $contact['Contact']['fone1_comercial'], 1, 0);	
			//$report->pdf->Cell(20, 8, date('d/m/Y', strtotime($contact['Contact']['data_nascimento'])), 1, 1);
			//$report->pdf->Cell(22, 8, '', 1, 0);	
			$report->pdf->Cell(60, 8, '', 1, 1);
		}
		else if ($contact['Contact']['contact_kind'] == 2) {
			$report->pdf->Cell(60, 8, $contact['ParentConjuge']['nome'], 1, 1);			
		}
		else if ($contact['Contact']['contact_kind'] == 3) { 
			$report->pdf->Cell(60, 8, $contact['ParentSubContact']['Contact']['nome'], 1, 1);
		}
	}
    
	$data = date('d_m_Y_hh_ii_ss', strtotime('now'));
	
	echo $report->pdf->Output('resumido_'.$data.'.pdf', 'i');
?> 