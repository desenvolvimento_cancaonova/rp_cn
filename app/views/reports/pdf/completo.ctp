<?php

	//inicializando configs P para portrait, mm para milimetros, A4 para formato da pagina
	$report->initialize('P', 'mm', 'A4');
	//setando o titulo do relatorio
	$report->pdf->set_report_type('Relatorio Completo');
	
	$report->pdf->SetMargins(5.0, 5.0, 5.0);
	
	$report->pdf->AliasNbPages();
	
	//abrindo pagina
	$report->pdf->AddPage();
	
	$report->pdf->setTitle('Completof');
	
	//setando fonte para imprimir os registros
	$report->pdf->SetFont('Arial', '', 7);
	foreach($contacts as $contact) {
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(10, 5, 'Nome:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(70, 5, $contact['Contact']['tratamento']." ". $contact['Contact']['nome'], 0, 0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(13, 6, 'Conjuge:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(50, 5, $contact['Conjuge']['nome'], 0, 0);
			
			$report->pdf->SetFont('Arial', 'B', 8);	
			$report->pdf->Cell(11, 6, 'Email:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5, $contact['Contact']['email'], 0, 1);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(14, 5, 'Empresa:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(66, 5, $contact['Contact']['empresa'], 0, 0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(13, 5, 'Cargo:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(50, 5, $contact['Occupation']['nome'], 0, 0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(9, 5, 'CEP:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(39, 5, $contact['AddressComercial']['cep'], 0,1);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(17, 5, 'End. Com.:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(63, 5, $contact['AddressComercial']['endereco'], 0,0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(17, 5, 'Cidade/UF:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(46, 5, $contact['AddressComercial']['cidade']."/".$contact['AddressComercial']['uf'], 0,0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(11, 5, 'Bairro:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(39, 5, $contact['AddressComercial']['bairro'], 0,1);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(16, 5, 'Fone Com:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(30, 5, $contact['Contact']['fone1_comercial'], 0, 0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(15, 5, 'Fone Res:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(30, 5, $contact['Contact']['fone1_residencial'], 0, 0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(12, 5, 'Celular:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(30, 5, $contact['Contact']['celular'], 0, 0);
				
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(12, 5, 'D. Nasc:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(30, 5, implode("/", array_reverse(explode("-",$contact['Contact']['data_nascimento']))), 0, 0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(11, 5, 'D. Ord:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(30, 5, implode("/", array_reverse(explode("-",$contact['Contact']['data_ordenacao']))), 0, 0);
			
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(13, 5, 'D. Episc:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(30, 5,implode("/", array_reverse(explode("-",$contact['Contact']['data_ord_episcopal']))), 0, 1);
			
			$report->pdf->Cell(0,0,"",1,1);
			$report->pdf->ln();
	}
	
	$data = date('d_m_Y_hh_ii_ss', strtotime('now'));
	
	echo $report->pdf->Output('completo_'.$data.'.pdf', 'i');
	?>