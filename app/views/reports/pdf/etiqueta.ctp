<?php

// Variaveis de Tamanho - convertendo para mm
$data['Label']['margem_esquerda'] = (float)$data['Label']['margem_esquerda']*10; // Margem Esquerda (mm)
$data['Label']['margem_direita'] = (float)$data['Label']['margem_direita']*10; // Margem Direita (mm)
$data['Label']['margem_superior'] = (float)$data['Label']['margem_superior']*10; // Margem Superior (mm)
$data['Label']['largura_etiqueta'] = (float)$data['Label']['largura_etiqueta']*10; // Largura da Etiqueta (mm)
$data['Label']['altura_etiqueta'] = (float)$data['Label']['altura_etiqueta']*10; // Altura da Etiqueta (mm)
$data['Label']['espaco_horizontal'] = (float)$data['Label']['espaco_horizontal']*10; // Espa�o horizontal entre as Etiquetas (mm)
$data['Label']['espaco_vertical'] = (float)$data['Label']['espaco_vertical']*10; // Espa�o horizontal entre as Etiquetas (mm)
$linhas = $data['Label']['linhas']; // numero de linhas
$colunas = $data['Label']['colunas']; // numero de colunas


$report->initialize('P', 'mm', array(215.9, 279.4));//strtolower($data['Label']['formato']));

$report->pdf->set_report_type('Etiquetas');

$report->pdf->set_header(false);
$report->pdf->set_footer(false);

$report->pdf->AliasNbPages();
    
$report->pdf->setTitle('Etiqueta');
$report->pdf->Open(); // inicia documento
$report->pdf->AddPage(); // adiciona a primeira pagina
$report->pdf->SetMargins($data['Label']['margem_esquerda'], $data['Label']['margem_superior']); // Define as margens do documento
$report->pdf->SetAuthor(""); // Define o autor
$report->pdf->SetFont('helvetica', '', 7); // Define a fonte
$report->pdf->SetDisplayMode();

$coluna = 0;
$linha = 0;

$x = 0;
$y = 0;

foreach($contacts as $key=>$contact) {
		
	$nome = $contact['Contact']['tratamento']." ".$contact['Contact']['nome'];
	$ende = $contact['AddressMain']['endereco'];
	$bairro = $contact['AddressMain']['bairro'];
	$estado = $contact['AddressMain']['uf'];
	$cida = $contact['AddressMain']['cidade'];
	$local = $bairro . " - " . $cida . " - " . $estado;
	$cep = "CEP: " . $contact['AddressMain']['cep'];

		
	if ($coluna == $data['Label']['colunas']) {
			
		$linha++;
		$coluna = 0;		
	}
	
	if ($linha == $data['Label']['linhas']) {
		
		$report->pdf->AddPage();
		$linha = 0;
	}
	
	if ($coluna == 0) {

		$x = $data['Label']['margem_esquerda'];
		
		if ($linha == 0)
			$y = $data['Label']['margem_superior'];
		else
			$y = ($linha * $data['Label']['altura_etiqueta'])+$data['Label']['margem_superior'];
	}
	else {
	
		$x = ($coluna * $data['Label']['espaco_horizontal'])+$data['Label']['margem_esquerda'];
		
		if ($linha == 0)
			$y = $data['Label']['margem_superior'];		
		else
			$y = ($linha * $data['Label']['altura_etiqueta'])+$data['Label']['margem_superior'];		
	}
	
	$report->pdf->Text($x+7, $y+8, utf8_decode($nome)); // Imprime o nome da pessoa de acordo com as coordenadas
	$report->pdf->Text($x+7, $y+12, utf8_decode($ende)); // Imprime o endereco da pessoa de acordo com as coordenadas
	$report->pdf->Text($x+7, $y+16, utf8_decode($local)); // Imprime a localidade da pessoa de acordo com as coordenadas
	$report->pdf->Text($x+7, $y+20, utf8_decode($cep)); // Imprime o cep da pessoa de acordo com as coordenadas
	
	$coluna++;
}

$data = date('d_m_Y_hh_ii_ss', strtotime('now'));

echo $report->pdf->Output('etiqueta_'.$data.'.pdf', 'i');

?>
