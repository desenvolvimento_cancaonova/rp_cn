<?php

//inicializando configs P para portrait, mm para milimetros, A4 para formato da pagina
$report->initialize('P', 'mm', 'A4');
//setando o titulo do relatorio
$report->pdf->set_report_type('Relatorio de Ficha');

$report->pdf->SetMargins(2.0, 2.0, 2.0);

$report->pdf->AliasNbPages();

//abrindo pagina
//$report->pdf->AddPage();

$report->pdf->setTitle('Fichaf');



foreach($contacts as $contact) {

	$report->pdf->addPage();
	
	if( file_exists(WWW_ROOT.$contact['MediaGalleryMain']['images']['medium']))
		$report->pdf->Image(WWW_ROOT.$contact['MediaGalleryMain']['images']['medium'], 5, 27,50);
	else
		$report->pdf->Cell(50, 47,"", 1, 0);
	
	//cabecalho dos campos dos registros
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->SetX("55");

	$report->pdf->Cell(18, 5, 'Tratamento: ', 0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(98, 5, $contact['Contact']['tratamento'], 0, 0);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(17, 5, 'Data Nasc:');
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5, implode("/", array_reverse(explode("-",$contact['Contact']['data_nascimento']))), 0, 1);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->SetX("55");
	$report->pdf->Cell(11, 5, 'Nome: ', 0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5, $contact['Contact']['nome'], 0, 1);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->SetX("55");
	$report->pdf->Cell(14, 5, 'Empresa: ', 0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5, $contact['Contact']['empresa'], 0, 1);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->SetX("55");
	$report->pdf->Cell(10, 5, 'Cargo: ', 0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5, $contact['Occupation']['nome'], 0, 1);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->SetX("55");
	$report->pdf->Cell(32, 5, 'Endereco Residencial:', 0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5, $contact['AddressResidencial']['endereco'], 0, 1);

	$report->pdf->SetX("55");
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(11, 5, 'Bairro:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(40, 5, $contact['AddressResidencial']['bairro'], 0, 0);
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(17, 5, 'Cidade/UF:', 0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5, $contact['AddressResidencial']['cidade']."/". $contact['AddressResidencial']['uf'], 0, 1);

	$report->pdf->SetX("55");
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(9, 5, 'Pais:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(103, 5, $contact['AddressResidencial']['pais'], 0, 0);
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(9, 5, 'CEP:', 0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5, $contact['AddressResidencial']['cep'], 0, 1);

	$report->pdf->SetX("55");
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(18, 5, 'Fone Resid:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(40, 5, $contact['Contact']['fone1_residencial'], 0, 0);
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(12, 5, 'Celular:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5,$contact['Contact']['celular'], 0, 1);

	$report->pdf->SetX("55");
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(18, 5, 'Fone Come:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(40, 5,$contact['Contact']['fone1_comercial'], 0, 0);
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(18, 5, 'Fone Come:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5,$contact['Contact']['fone2_comercial'], 0, 1);

	$report->pdf->SetX("55");
	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(11, 5, 'E-mail:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(0, 5,$contact['Contact']['email'], 0,1);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(24, 5, 'Data Ordenação:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$contact['Contact']['data_ordenacao']))), 0,0);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(22, 5, 'Data Ord. Epis:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$contact['Contact']['data_ordenacao']))),0, 0);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(27, 5, 'Data Consagração:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$contact['Contact']['data_consagracao']))),0,0);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(24, 5, 'Data Casamento:',0, 0);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$contact['Contact']['data_casamento']))), 0, 1);

	$report->pdf->SetFont('Arial', 'B', 8);
	$report->pdf->Cell(24, 5, 'Observações:',0,1);
	$report->pdf->SetFont('Arial', '', 7);
	$report->pdf->MultiCell(0, 5, $contact['Contact']['observacoes']);

	$report->pdf->ln();
	if(!empty($contact['Conjuge']['id'])){
		
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(0, 5, 'Conjuge:',0,1);
		$report->pdf->SetX("5");
		$report->pdf->Cell(18, 5, 'Tratamento: ', 0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(98, 5, $contact['Conjuge']['tratamento'], 0, 0);

		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(17, 5, 'Data Nasc:');
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(0, 5, implode("/", array_reverse(explode("-",$contact['Conjuge']['data_nascimento']))), 0, 1);

		$report->pdf->SetX("5");
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(11, 5, 'Nome: ', 0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(0, 5, $contact['Conjuge']['nome'], 0, 1);

		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->SetX("5");
		$report->pdf->Cell(14, 5, 'Empresa: ', 0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(0, 5, $contact['Conjuge']['empresa'], 0, 1);
		
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->SetX("5");
		$report->pdf->Cell(14, 5, 'Cargo: ', 0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(0, 5, $contact['Conjuge']['Occupation']['nome'], 0, 1);

		$report->pdf->SetX("5");
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(18, 5, 'Fone Resid:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(40, 5, $contact['Conjuge']['fone1_residencial'], 0, 0);
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(12, 5, 'Celular:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(0, 5,$contact['Conjuge']['celular'], 0, 1);

		$report->pdf->SetX("5");
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(18, 5, 'Fone Come:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(40, 5,$contact['Conjuge']['fone1_comercial'], 0, 0);
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(18, 5, 'Fone Come:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(0, 5,$contact['Conjuge']['fone2_comercial'], 0, 1);

		$report->pdf->SetX("5");
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(11, 5, 'E-mail:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(0, 5,$contact['Conjuge']['email'], 0,1);

		$report->pdf->SetX("5");
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(24, 5, 'Data Ordenação:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$contact['Conjuge']['data_ordenacao']))), 0,0);

		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(22, 5, 'Data Ord. Epis:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$contact['Conjuge']['data_ordenacao']))),0, 0);

		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(27, 5, 'Data Consagração:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$contact['Conjuge']['data_consagracao']))),0,0);

		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(24, 5, 'Data Casamento:',0, 0);
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$contact['Conjuge']['data_casamento']))), 0, 1);

		$report->pdf->SetX("5");
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(24, 5, 'Observações:',0,1);
		$report->pdf->SetX("5");
		$report->pdf->SetFont('Arial', '', 7);
		$report->pdf->MultiCell(0, 5, $contact['Conjuge']['observacoes']);

		$report->pdf->ln();
	}

	if(!empty($contact['Subcontact'][0])){
		$report->pdf->SetFont('Arial', 'B', 8);
		$report->pdf->Cell(0, 5, 'SubContatos:',0,1);

		foreach($contact['Subcontact'] as $subContact) {
			
			$report->pdf->SetX("5");
			$report->pdf->Cell(18, 5, 'Tratamento: ', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(150, 5,$subContact['SubcontactRow']['tratamento'], 0, 0);

			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(17, 5, 'Data Nasc:');
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5, implode("/", array_reverse(explode("-",$subContact['SubcontactRow']['data_nascimento']))), 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(11, 5, 'Nome: ', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5, $subContact['SubcontactRow']['nome'], 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(14, 5, 'Empresa: ', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5, $subContact['SubcontactRow']['empresa'], 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(10, 5, 'Cargo: ', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5, $subContact['SubcontactRow']['Occupation']['nome'], 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(32, 5, 'Endereço Residencial:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5, utf8_decode($subContact['SubcontactRow']['AddressMain']['endereco']), 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(11, 5, 'Bairro:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(103, 5, $subContact['SubcontactRow']['AddressMain']['bairro'], 0, 0);
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(15, 5, 'Cidade/UF:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5, $subContact['SubcontactRow']['AddressMain']['cidade']."/". $subContact['SubcontactRow']['AddressMain']['uf'], 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->Cell(9, 5, 'Pais:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(105, 5, $subContact['SubcontactRow']['AddressMain']['pais'], 0, 0);
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(10, 5, 'CEP:', 0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5, $subContact['SubcontactRow']['AddressMain']['cep'], 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(18, 5, 'Fone Resid:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(25, 5, $subContact['SubcontactRow']['fone1_residencial'],0, 0);
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(12, 5, 'Celular:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(25, 5,$subContact['SubcontactRow']['celular'], 0, 0);

			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(18, 5, 'Fone Come:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(25, 5,$subContact['SubcontactRow']['fone1_comercial'],0, 0);
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(18, 5, 'Fone Come:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5,$subContact['SubcontactRow']['fone2_comercial'], 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(11, 5, 'E-mail:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(0, 5,$subContact['SubcontactRow']['email'], 0,1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(24, 5, 'Data Ordenação:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$subContact['SubcontactRow']['data_ordenacao']))), 0,0);

			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(22, 5, 'Data Ord. Epis:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$subContact['SubcontactRow']['data_ordenacao']))),0, 0);

			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(27, 5, 'Data Consagração:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$subContact['SubcontactRow']['data_consagracao']))),0,0);

			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(24, 5, 'Data Casamento:',0, 0);
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->Cell(25, 5,implode("/", array_reverse(explode("-",$subContact['SubcontactRow']['data_casamento']))), 0, 1);

			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', 'B', 8);
			$report->pdf->Cell(24, 5, 'Observações:',0,1);
			$report->pdf->SetX("5");
			$report->pdf->SetFont('Arial', '', 7);
			$report->pdf->MultiCell(0, 5, $subContact['SubcontactRow']['observacoes']);

			$report->pdf->ln(2);
		}
	}
}

$data = date('d_m_Y_hh_ii_ss', strtotime('now'));

echo $report->pdf->Output('ficha_'.$data.'.pdf', 'i');
?>