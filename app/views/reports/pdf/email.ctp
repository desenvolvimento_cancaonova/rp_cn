<?php
	
	//inicializando configs P para portrait, mm para milimetros, A4 para formato da pagina
	$report->initialize('P', 'mm', 'A4');
	//setando o titulo do relatorio
	$report->pdf->set_report_type('Relatorio de Emails');

    $report->pdf->AliasNbPages();
    
    //abrindo pagina
	$report->pdf->AddPage();
	
    $report->pdf->setTitle('Email');
    
    //cabecalho dos campos dos registros
    $report->pdf->SetFont('Arial', 'B', 12);    
    $report->pdf->Cell(110, 8, 'Nome', 1, 0);
	$report->pdf->Cell(80, 8, 'Email', 1, 1);
		
	//setando fonte para imprimir os registros
	$report->pdf->SetFont('Arial', '', 9);	
	foreach($contacts as $contact) {

		$report->pdf->Cell(110, 8, $contact['Contact']['nome'], 1, 0);
		$report->pdf->Cell(80, 8, $contact['Contact']['email'], 1, 1);
	}
    
	$data = date('d_m_Y_hh_ii_ss', strtotime('now'));
	
	echo $report->pdf->Output('email_'.$data.'.pdf', 'i');
?> 