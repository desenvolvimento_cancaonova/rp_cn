<?php

	//inicializando configs P para portrait, mm para milimetros, A4 para formato da pagina
	$report->initialize('L', 'mm', 'A4');
	//setando o titulo do relatorio
	$report->pdf->set_report_type('Relatorio de Anivers&aacute;rios');
	
	$report->pdf->SetMargins(5.0, 5.0, 5.0);
	
	$report->pdf->AliasNbPages();
	
	//abrindo pagina
	$report->pdf->AddPage();
	
	$report->pdf->setTitle('Aniversariof');
	
	//cabecalho dos campos dos registros
	$report->pdf->SetFont('Arial', 'B', 9);
	$report->pdf->Cell(27, 8, 'Tratamento', 1, 0);
	$report->pdf->Cell(70, 8, 'Nome', 1, 0);
	$report->pdf->Cell(40, 8, 'Conjuge', 1, 0);
	$report->pdf->Cell(52, 8, 'Empresa', 1, 0);
	$report->pdf->Cell(30, 8, 'Cargo', 1, 0);
	$report->pdf->Cell(20, 8, 'Fone Com.', 1, 0);
	$report->pdf->Cell(35, 8, 'Cidade', 1, 0);
	$report->pdf->Cell(15, 8, 'Nasci.', 1, 0);
	
	$report->pdf->Ln();
	
	//setando fonte para imprimir os registros
	$report->pdf->SetFont('Arial', '', 7);
	foreach($contacts as $contact) {
			$report->pdf->Cell(27, 8, $this->Text->truncate($contact['Contact']['tratamento'], 21, array('exact'=>true, 'ending'=>'')), 1, 0);
			$report->pdf->Cell(70, 8, $this->Text->truncate($contact['Contact']['nome'], 47, array('exact'=>true, 'ending'=>'')), 1, 0, 'L', false);
			$report->pdf->Cell(40, 8, $this->Text->truncate($contact['Conjuge']['nome'], 24, array('exact'=>true, 'ending'=>'')), 1, 0, 'L', false);
			$report->pdf->Cell(52, 8, substr($contact['Contact']['empresa'],0,29), 1, 0);
			$report->pdf->Cell(30, 8, $this->Text->truncate($contact['Occupation']['nome'], 18, array('exact'=>true, 'ending'=>'')), 1, 0);
			$report->pdf->Cell(20, 8, $contact['Contact']['fone1_comercial'], 1, 0);
			$report->pdf->Cell(35, 8, $contact['AddressMain']['cidade']." - ". $contact['AddressMain']['uf'], 1, 0);
			$report->pdf->Cell(15, 8, implode("/", array_reverse(explode("-",$contact['Contact']['data_nascimento']))), 1, 1);
	}
	
	$data = date('d_m_Y_hh_ii_ss', strtotime('now'));
	
	echo $report->pdf->Output('aniversario_'.$data.'.pdf', 'i');
	?>