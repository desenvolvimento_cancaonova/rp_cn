<?php
	
	//inicializando configs P para portrait, mm para milimetros, A4 para formato da pagina
	$report->initialize('P', 'mm', 'A4');
	//setando o titulo do relatorio
	$report->pdf->set_report_type('Relatorio Estat&iacute;stico');

    $report->pdf->AliasNbPages();
    
    //abrindo pagina
	$report->pdf->AddPage();
	
    $report->pdf->setTitle('Estatistico');
    
    //cabecalho dos campos dos registros
    $report->pdf->SetFont('Arial', 'B', 12);    
    $report->pdf->Cell(110, 8, 'Nome', 1, 0);
    $report->pdf->Cell(40, 8, 'Quantidade', 1, 0);
	$report->pdf->Cell(40, 8, 'Percentagem', 1, 1);
		
	//setando fonte para imprimir os registros
	$report->pdf->SetFont('Arial', '', 9);	
	foreach($rows as $row) {

		$report->pdf->Cell(110, 8, !empty($row[0]['default_name'])?$row[0]['default_name']:'Nao Especificado', 1, 0);
		$report->pdf->Cell(40, 8, $row[0]['quantidade'], 1, 0);
		$report->pdf->Cell(40, 8, number_format($row[0]['percentagem'], 2).'%', 1, 1);
	}
    
	$data = date('d_m_Y_hh_ii_ss', strtotime('now'));
	
	echo $report->pdf->Output('estatistica_'.$data.'.pdf', 'i');
?> 