<div class='reports title'>
	<h1><?php echo $form_name; ?></h1>
</div>

<br />

<div class="contacts reports search_bar">		
	<?php echo $this->element('reports/estatistico_filter'); ?>	
</div>

<br />

<div class="contacts reports result">
	<?php if (!empty($pdf_url)): ?>	
		<?php echo $this->element('reports/do_report_pdf'); ?>
	<?php endif; ?>
</div>