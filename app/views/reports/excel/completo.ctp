<?php

	//input the export file name
	$xls->setHeader($type);

    $xls->addXmlHeader();
    $xls->setWorkSheetName('Email');

    //1st row for columns name
    $xls->openRow();
    $xls->writeString('Tratamento');
    $xls->writeString('Nome');
    $xls->writeString('Conjuge');
    $xls->writeString('E-mail');
    $xls->writeString('Empresa');
    $xls->writeString('Cargo');
    $xls->writeString('Endere�o Com.');
    $xls->writeString('Cidade. Com.');
    $xls->writeString('Bairro Com.');
    $xls->writeString('CEP Com.');
    $xls->writeString('Fone Com.');
    $xls->writeString('Fone Res.');
    $xls->writeString('Celular');
    $xls->writeString('Data Nasc');
    $xls->writeString('Data Ord.');
    $xls->writeString('Data Episc.');
    $xls->closeRow();
    

    //rows for data
    foreach ($contacts as $contact):
	    $xls->openRow();
	    $xls->writeString( $contact['Contact']['tratamento']);
	    $xls->writeString( $contact['Contact']['nome']);
	    $xls->writeString( $contact['Conjuge']['nome']);
	    $xls->writeString( $contact['Contact']['email']);
	    $xls->writeString( $contact['Contact']['empresa']);
	    $xls->writeString( $contact['Occupation']['nome'], 1, 0);
	    $xls->writeString( $contact['AddressComercial']['endereco']);
	    $xls->writeString( $contact['AddressComercial']['cidade'].' - '.$contact['AddressComercial']['uf']);
	    $xls->writeString( $contact['AddressComercial']['bairro']);
	    $xls->writeString( $contact['AddressComercial']['cep']);
	    $xls->writeString( $contact['Contact']['fone1_comercial']);
	    $xls->writeString( $contact['Contact']['fone1_residencial']);
	    $xls->writeString( $contact['Contact']['celular']);
	    $xls->writeString( implode("/", array_reverse(explode("-",$contact['Contact']['data_nascimento']))));
	    $xls->writeString( implode("/", array_reverse(explode("-",$contact['Contact']['data_ordenacao']))));
	    $xls->writeString( implode("/", array_reverse(explode("-",$contact['Contact']['data_ord_episcopal']))));
	     
	    $xls->closeRow();
    endforeach;

    $xls->addXmlFooter();
    exit();
?>