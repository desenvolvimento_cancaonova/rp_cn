<?php

	//input the export file name
	$xls->setHeader($type);

    $xls->addXmlHeader();
    $xls->setWorkSheetName('Email');

    //1st row for columns name
    $xls->openRow();
    $xls->writeString('Tratamento');
    $xls->writeString('Nome');
    $xls->writeString('Empresa');
    $xls->writeString('Cargo');
    $xls->writeString('Fone Comercial');
    $xls->writeString('Cidade');
    $xls->writeString('Data Nascimento');
    
    $xls->closeRow();
    

    //rows for data
    foreach ($contacts as $contact):
    	$xls->openRow();
	    $xls->writeString($contact['Contact']['tratamento']);
	    $xls->writeString($contact['Contact']['nome']);
	    $xls->writeString($contact['Contact']['empresa']);
	    $xls->writeString($contact['Occupation']['nome']);
	    $xls->writeString($contact['Contact']['fone1_comercial']);
	    $xls->writeString($contact['AddressMain']['cidade']." - ". $contact['AddressMain']['uf']);
	    $xls->writeString(implode("/", array_reverse(explode("-",$contact['Contact']['data_nascimento']))));		    
	    $xls->closeRow();
    endforeach;

    $xls->addXmlFooter();
    exit();
?>