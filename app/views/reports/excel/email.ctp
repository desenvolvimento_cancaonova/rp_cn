<?php

	//input the export file name
	$xls->setHeader($type);

    $xls->addXmlHeader();
    $xls->setWorkSheetName('Email');

    //1st row for columns name
    $xls->openRow();
    $xls->writeString('Nome');
    $xls->writeString('Email');
    $xls->closeRow();

    //rows for data
    foreach ($contacts as $contact):
    	$xls->openRow();
	    $xls->writeString($contact['Contact']['nome']);
	    $xls->writeString($contact['Contact']['email']);	    
	    $xls->closeRow();
    endforeach;

    $xls->addXmlFooter();
    exit();
?>