<?php

	//input the export file name
	$xls->setHeader($type);

    $xls->addXmlHeader();
    $xls->setWorkSheetName('Ficha');

    //1st row for columns name
    $xls->openRow();
    $xls->writeString('Tratamento');
    $xls->writeString('Nome');
    $xls->writeString('E-mail');
    $xls->writeString('Data Nasc');
    $xls->writeString('Fone Res.');
    $xls->writeString('Celular');
    $xls->writeString('Observações');
       $xls->closeRow();

    //rows for data
    foreach ($contacts as $contact):
		$xls->openRow();
	    $xls->writeString($contact['Contact']['tratamento']);
	    $xls->writeString($contact['Contact']['nome']);
		$xls->writeString( $contact['Contact']['email']);
		$xls->writeString(implode("/", array_reverse(explode("-",$contact['Contact']['data_nascimento']))));
		$xls->writeString( $contact['Contact']['fone1_residencial']);
		$xls->writeString( $contact['Contact']['celular']);
		$xls->writeString($contact['Contact']['observacoes']);
		$xls->closeRow();
		
		if(count($contact['Subcontact'])>0){
			   $xls->openRow();
			 	$xls->writeString( 'Sub Contatos');
				$xls->writeString( 'Tratamento');
				$xls->writeString( 'Nome');
				$xls->writeString( 'D. Nasc.');
				$xls->writeString( 'Endereço');
				$xls->writeString( 'Cidade');
				$xls->writeString( 'Bairro');
				$xls->writeString( 'CEP');
				$xls->writeString( 'Pais');
				$xls->writeString( 'Empresa');
				$xls->writeString( 'Cargo');
				$xls->writeString( 'E-mail Principal');
				$xls->writeString( 'E-mail Secundario');
				$xls->writeString( 'Fone Com');
				$xls->writeString( 'Fone Res.');
				$xls->writeString( 'Celular');
				$xls->writeString( 'Observações');
				$xls->closeRow();
				   
				foreach($contact['Subcontact'] as $subContact) {
					$xls->openRow();
					$xls->writeString( $subContact['tratamento'] );
					$xls->writeString( $subContact['nome'] );
					$xls->writeString( $subContact['data_nascimento'] );
					$xls->writeString( $subContact['endereco'] );
					$xls->writeString( $subContact['cidade'] );
					$xls->writeString( $subContact['bairro'] );
					$xls->writeString( $subContact['cep'] );
					$xls->writeString( $subContact['pais'] );
					$xls->writeString( substr($subContact['empresa'], 0 , 25) );
					$xls->writeString( $subContact['Occupation']['nome'] );
					$xls->writeString( $subContact['email'] );
					$xls->writeString( $subContact['email_secundario'] );
					$xls->writeString( $subContact['fone1_comercial'] );
					$xls->writeString( $subContact['fone1_residencial'] );
					$xls->writeString( $subContact['celular'] );
					$xls->writeString( $subContact['observacao'] );
					$xls->closeRow();
				}
		}
		
		
    endforeach;

    $xls->addXmlFooter();
    exit();
?>