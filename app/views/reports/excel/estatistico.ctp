<?php

	//input the export file name
	$xls->setHeader($type);

    $xls->addXmlHeader();
    $xls->setWorkSheetName('Estatistico');

    //1st row for columns name
    $xls->openRow();
    $xls->writeString('Nome');
    $xls->writeString('Quantidade');
    $xls->writeString('Percentagem');
    $xls->closeRow();

    //rows for data
    foreach ($rows as $row):
    	$xls->openRow();
	    $xls->writeString($row[0]['default_name']);
	    $xls->writeString($row[0]['quantidade']);
	    $xls->writeString(number_format($row[0]['percentagem'], 2));	    
	    $xls->closeRow();
    endforeach;

    $xls->addXmlFooter();
    exit();
?>