<div class="actionAuths index">
<h2><?php __('ActionAuths');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('controller_auth_id');?></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('show_name');?></th>
	<th><?php echo $paginator->sort('show');?></th>
	<th><?php echo $paginator->sort('allowed');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($actionAuths as $actionAuth):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $actionAuth['ActionAuth']['id']; ?>
		</td>
		<td>
			<?php echo $html->link($actionAuth['ControllerAuth']['name'], array('controller' => 'controller_auths', 'action' => 'view', $actionAuth['ControllerAuth']['id'])); ?>
		</td>
		<td>
			<?php echo $actionAuth['ActionAuth']['name']; ?>
		</td>
		<td>
			<?php echo $actionAuth['ActionAuth']['show_name']; ?>
		</td>
		<td>
			<?php echo $actionAuth['ActionAuth']['show']; ?>
		</td>
		<td>
			<?php echo $actionAuth['ActionAuth']['allowed']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $actionAuth['ActionAuth']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $actionAuth['ActionAuth']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $actionAuth['ActionAuth']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $actionAuth['ActionAuth']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New ActionAuth', true), array('action' => 'add')); ?></li>
		<li><?php echo $html->link(__('List Controller Auths', true), array('controller' => 'controller_auths', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Controller Auth', true), array('controller' => 'controller_auths', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
