<div class="actionAuths form">
<?php echo $form->create('ActionAuth');?>
	<fieldset>
 		<legend><?php __('Add ActionAuth');?></legend>
	<?php
		echo $form->input('controller_auth_id');
		echo $form->input('name');
		echo $form->input('show_name');
		echo $form->input('show');
		echo $form->input('allowed');
		echo $form->input('User');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ActionAuths', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Controller Auths', true), array('controller' => 'controller_auths', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Controller Auth', true), array('controller' => 'controller_auths', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
