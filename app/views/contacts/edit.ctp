<div class="contacts form">

<h2><?php __('Contato');?></h2>
<br />

<?php echo $form->create('Contact');?>
	<fieldset>
 		<legend><?php __('Editar Contato');?></legend>
				
		<?php echo $this->Html->link(':: Gerenciar Endere&ccedil;os ::', array('controller'=>'contacts', 'action'=>'addresses', 'contact', $this->params['pass'][0]), array('escape'=>false, 'class'=>'')); ?>
		<?php echo $this->element('contacts/address_main'); ?>		
		<br /><br />		
			
		<?php echo $form->input('id'); ?>	
		<?php echo $this->element('contacts/form'); ?>
		
		<?php echo $form->end('Salvar');?>
		
	</fieldset>
</div>