<div class="contacts index">
<h2><?php __('SubContatos');?></h2>

<br />

Contato: <strong><?php echo $contact['Contact']['nome']; ?></strong>

<br /><br />
<?php echo $html->link(__('Novo Sub Contato', true), array('action' => 'subcontacts', $contact_id, 'new')); ?>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('nome');?></th>
	<?php /*
	<th><?php echo $paginator->sort('endereco');?></th>
	<th><?php echo $paginator->sort('estado_id');?></th>
	<th><?php echo $paginator->sort('cidade_id');?></th>
	<th><?php echo $paginator->sort('cep');?></th>
	<th><?php echo $paginator->sort('telefone');?></th>
	<th><?php echo $paginator->sort('data_nascimento');?></th>
	*/ ?>
	<th><?php echo $paginator->sort('email');?></th>
	<th><?php echo $paginator->sort('Ativo?', 'ativo', array('escape'=>false));?></th>
	<th><?php echo $paginator->sort('Data Cadastro', 'created');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($contacts as $contact):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td align='center'>
			<?php echo $contact['SubcontactRow']['id']; ?>
		</td>
		<td>
			<?php echo $contact['SubcontactRow']['nome']; ?>
		</td>
		<?php /*
		<td>
			<?php echo $contact['Contact']['endereco']; ?>
		</td>
		<td>
			<?php echo $contact['Estado']['descricao']; ?>
		</td>
		<td>
			<?php echo $contact['Cidade']['descricao']; ?>
		</td>
		<td>
			<?php echo $contact['Contact']['cep']; ?>
		</td>
		<td>
			<?php echo $contact['Contact']['telefone']; ?>
		</td>		
		<td align='center'>
			<?php echo date('d/m/Y', strtotime($contact['Contact']['data_nascimento'])); ?>
		</td>
		*/ ?>
		<td>
			<?php echo $contact['SubcontactRow']['email']; ?>
		</td>
		<td align='center'>
			<?php echo empty($contact['SubcontactRow']['ativo'])?'N&atilde;o':'Sim'; ?>
		</td>
		<td align='center'>
			<?php echo date('d/m/Y H:i:s', strtotime($contact['SubcontactRow']['created'])); ?>
		</td>
		<td class="actions" align='center'>
			<?php //echo $myhtml->link(__('Ver', true), array('action' => 'view', $contact['SubcontactRow']['id'])); ?>
			<?php echo $html->link(__('Editar', true), array('action' => 'subcontacts', $contact_id, $contact['SubcontactRow']['id'])); ?>			 
			<?php //echo $html->link(__('Apagar', true), array('action' => 'delete', $contact['Contact']['id']), null, sprintf('Confirma exclusao do registro #%s?', $contact['Contact']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<?php echo $this->element('pagination'); ?>

<?php if (!empty($this->params['pass'][1])): ?>
<br />

<div class="contacts form">

<h2><?php __('SubContato');?></h2>
<br />

<?php echo $form->create('Contact', array('url'=>array('controller'=>'contacts', 'action'=>'subcontacts', $this->params['pass'][0], !empty($this->params['pass'][1])?$this->params['pass'][1]:'')));?>
	<fieldset>
 		<legend><?php echo $this->params['pass'][1]=='new'?'Novo':'Editar';?> SubContato</legend>
	
		<?php if (!empty($this->data['Contact']['id'])): ?>
			<?php echo $this->Html->link(':: Gerenciar Endere&ccedil;os ::', array('controller'=>'contacts', 'action'=>'addresses', $this->params['pass'][0], $this->data['Contact']['id']), array('escape'=>false, 'class'=>'')); ?>
			<?php echo $this->element('contacts/address_main'); ?>
		<?php endif; ?>
		
		<br /><br />
		
		
		<?php echo $this->element('contacts/form'); ?>
				
		<?php echo $form->end('Salvar');?>
		
	</fieldset>
</div>
<?php endif; ?>