<div class="contacts form">

<h2><?php __('Conjuge');?></h2>

<br />

Contato: <strong><?php echo $contact['Contact']['nome']; ?></strong>

<br /><br />

<?php echo $form->create('Contact', array('url'=>array('controller'=>'contacts', 'action'=>'conjuge', $contact_id)));?>
	<fieldset>
 		<legend><?php __('Editar Conjuge');?></legend>
		
		<?php if (!empty($this->data['Contact']['id'])): ?>
			<?php echo $this->Html->link(':: Gerenciar Endere&ccedil;os ::', array('controller'=>'contacts', 'action'=>'addresses', $contact_id, $this->data['Contact']['id']), array('escape'=>false, 'class'=>'')); ?>
			<?php echo $this->element('contacts/address_main'); ?>
		<?php endif; ?>
				
		<br /><br />
		
		<?php echo $form->input('id'); ?>	
		<?php echo $this->element('contacts/form'); ?>
		
		<?php echo $form->end('Salvar');?>
		
	</fieldset>
</div>