<div class="imagens form">

	<h2>Imagens</h2>
	<br />
	
	Contato: <strong><?php echo $contact['Contact']['nome']; ?></strong>
	<br /><br />	
	<?php echo $this->element('medias/form'); ?>

<div style="padding:20px">
	<?php if (!empty($images)): ?>
		<h2>Imagens Cadastradas</h2>
		<br />
		<div id="medias_images">
			<?php foreach($images as $image): ?>
				<div id='media_<?php echo $image['MediaGallery']['id']; ?>' class="medias_images_class"><?php echo $this->Html->link($this->Html->image($image['MediaGallery']['images']['thumb']), $image['MediaGallery']['images']['big'], array('escape'=>false, 'class'=>'medias_images_a_class')); ?>
					<br>
					<?php if (!empty($image['MediaGallery']['main'])): ?>
						Imagem principal
					<?php else: ?>
						<?php echo $this->Html->link('Setar Principal', array('controller'=>'contacts', 'action'=>'set_main_image', $image['MediaGallery']['contact_id'], $image['MediaGallery']['id']), array('media_id'=>$image['MediaGallery']['id'])); ?> 
					<?php endif; ?>
					<br />
					<?php echo $this->Html->link('apagar', 'javascript:void(0);', array('media_id'=>$image['MediaGallery']['id'], 'class'=>'delete_media')); ?>
										
				</div>
			<?php endforeach; ?>
		</div>
		<br />
	<?php endif; ?>
</div>


</div>

<?php $this->addScript($javascript->link(array('medias/add', 'plugins/jquery.lightbox'))); ?>
<?php $this->addScript($html->css(array('jquery.lightbox'))); ?>
