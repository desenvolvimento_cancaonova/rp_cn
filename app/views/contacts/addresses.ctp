<div class="address form">

<h2><?php __('Gerenciar Endere&ccedil;os');?></h2>

<br />
<?php if ($this->params['pass'][0] == 0): ?>
	Contato: <strong><?php echo $contact_row['Contact']['nome']; ?></strong>
<?php else: ?>
	Contato: <strong><?php echo $contact['Contact']['nome']; ?></strong>
<?php endif; ?>
<br />
<?php if ($contact_row['Contact']['contact_kind'] == CONTACT_KIND_CONJUGE): ?>
	Conjugue: <strong><?php echo $contact_row['Contact']['nome']; ?></strong>
<?php elseif ($contact_row['Contact']['contact_kind'] == CONTACT_KIND_SUBCONTACT): ?>
	Sub Contato: <strong><?php echo $contact_row['Contact']['nome']; ?></strong>
<?php endif; ?>

<br /><br />
<?php echo $html->link(__('Novo Endere&ccedil;o', true), array('action' => 'addresses', $this->params['pass'][0], $this->params['pass'][1], 'new'), array('escape'=>false)); ?>

<table cellpadding="0" cellspacing="0">
<tr>
	<th>Id</th>
	<th>Nome</th>
	<?php /*
	<th><?php echo $paginator->sort('endereco');?></th>
	<th><?php echo $paginator->sort('estado_id');?></th>
	<th><?php echo $paginator->sort('cidade_id');?></th>
	<th><?php echo $paginator->sort('cep');?></th>
	<th><?php echo $paginator->sort('telefone');?></th>
	<th><?php echo $paginator->sort('data_nascimento');?></th>
	*/ ?>
	<th>Endereco</th>
	<th>Principal</th>
	<th>Data Cadastro</th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($addresses as $row):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td align='center'>
			<?php echo $row['Address']['id']; ?>
		</td>
		<td>
			<?php echo $row['Address']['address_name']; ?>
		</td>
		<?php /*
		<td>
			<?php echo $contact['Contact']['endereco']; ?>
		</td>
		<td>
			<?php echo $contact['Estado']['descricao']; ?>
		</td>
		<td>
			<?php echo $contact['Cidade']['descricao']; ?>
		</td>
		<td>
			<?php echo $contact['Contact']['cep']; ?>
		</td>
		<td>
			<?php echo $contact['Contact']['telefone']; ?>
		</td>		
		<td align='center'>
			<?php echo date('d/m/Y', strtotime($contact['Contact']['data_nascimento'])); ?>
		</td>
		*/ ?>
		<td>
			<?php echo $row['Address']['endereco']; ?>
		</td>
		<td align='center'>
			<?php echo empty($row['Address']['main'])?'N&atilde;o':'Sim'; ?>
		</td>
		<td align='center'>
			<?php echo date('d/m/Y H:i:s', strtotime($row['Address']['created'])); ?>
		</td>
		<td class="actions" align='center'>
			<?php //echo $myhtml->link(__('Ver', true), array('action' => 'view', $contact['SubcontactRow']['id'])); ?>
			<?php echo $html->link(__('Editar', true), array('action' => 'addresses', $this->params['pass'][0], $this->params['pass'][1], $row['Address']['id'])); ?>			 
			<?php echo $html->link(__('Apagar', true), array('action' => 'address_delete', $this->params['pass'][0], $this->params['pass'][1], $row['Address']['id']), null, sprintf('Confirma exclusao do registro #%s?', $row['Address']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>

<?php if (!empty($this->params['pass']['2'])): ?>
<br />

	<?php echo $form->create('Address', array('url'=>array('controller'=>'contacts', 'action'=>'addresses', $this->params['pass'][0], $this->params['pass'][1], !empty($this->params['pass'][2])?$this->params['pass'][2]:'')));?>
	<fieldset>
 		<legend><?php __('Endere&ccedil;o');?></legend>
		
		<?php echo $form->input('id'); ?>	
		<?php echo $this->element('contacts/address_form'); ?>
		
		<?php echo $form->end('Salvar');?>
		
	</fieldset>
<?php endif; ?>
</div>