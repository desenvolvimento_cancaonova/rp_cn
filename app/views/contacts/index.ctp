<div class="search_get">
	<h3>::Filtrar registros::</h3>
</div>

<div class="contacts index search_bar">

<?php echo $this->element('reports/filter'); ?>	

</div>

<br />

<div class="contacts index">
<h2><?php __('Contatos');?></h2>

<table cellpadding="0" cellspacing="0">
<tr>
	<th>Imagem</th>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('nome');?></th>	
	<th><?php echo $paginator->sort('empresa');?></th>
	<th><?php echo $paginator->sort('email');?></th>
	<th width="90px">Telefones</th>
	<?php /*
	<th><?php echo $paginator->sort('cep');?></th>
	<th><?php echo $paginator->sort('telefone');?></th>
	<th><?php echo $paginator->sort('data_nascimento');?></th>
	
	<th><?php echo $paginator->sort('email');?></th>
	<th><?php echo $paginator->sort('Ativo?', 'ativo', array('escape'=>false));?></th>
	<th><?php echo $paginator->sort('Data Cadastro', 'created');?></th>
	*/ ?>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($contacts as $contact):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td align='center'>
			<?php echo $this->Html->link($this->Html->image($contact['MediaGalleryMain']['images']['thumb'], array('class'=>'noimage_contact')), $contact['MediaGalleryMain']['images']['big'], array('escape'=>false, 'class'=>'medias_images_a_class', 'title'=>$contact['Contact']['nome'].' - '.$contact['Contact']['empresa'])); ?>			
		</td>
		<td align='center'>
			<?php echo $contact['Contact']['id']; ?>
		</td>
		<td>
			<?php echo $this->element('contacts/edit_contact', array('link_value'=>$contact['Contact']['nome'], 'contact'=>$contact)); ?>
		</td>
		<td>
			<?php echo $contact['Contact']['empresa']; ?>
		</td>
		<td>
			<?php echo $contact['Contact']['email']; ?>
		</td>
		<td align='center'>
			<?php echo $contact['Contact']['fone1_comercial']; ?><br />
			<?php echo $contact['Contact']['fone2_comercial']; ?><br />
			<?php echo $contact['Contact']['celular']; ?><br />
			<?php echo $contact['Contact']['fone1_residencial']; ?>
		</td>
		<?php /*
		<td>
			<?php echo $contact['Contact']['endereco']; ?>
		</td>
		<td>
			<?php echo $contact['Estado']['descricao']; ?>
		</td>
		<td>
			<?php echo $contact['Cidade']['descricao']; ?>
		</td>
		<td>
			<?php echo $contact['Contact']['cep']; ?>
		</td>
		<td>
			<?php echo $contact['Contact']['telefone']; ?>
		</td>		
		<td align='center'>
			<?php echo date('d/m/Y', strtotime($contact['Contact']['data_nascimento'])); ?>
		</td>
				
		<td align='center'>
			<?php echo empty($contact['Contact']['ativo'])?'N&atilde;o':'Sim'; ?>
		</td>
		<td align='center'>
			<?php echo date('d/m/Y H:i:s', strtotime($contact['Contact']['created'])); ?>
		</td>
		*/ ?>
		<td class="actions" align='center'>
			<?php //echo $myhtml->link(__('Ver', true), array('action' => 'view', $contact['Contact']['id'])); ?>
			<?php echo $this->element('contacts/edit_contact', array('link_value'=>'editar', 'contact'=>$contact)); ?>
			<?php if (!empty($contact['Contact']['ativo'])): ?>			 
				<?php echo $html->link(__('Inativar', true), array('action' => 'status', $contact['Contact']['id'], 0), null, sprintf('Confirma inativacao do registro #%s? (%s)', $contact['Contact']['id'], $contact['Contact']['nome'])); ?>
			<?php else: ?>
				<?php echo $html->link(__('Ativar', true), array('action' => 'status', $contact['Contact']['id'], 1), null, sprintf('Confirma ativacao do registro #%s? (%s)', $contact['Contact']['id'], $contact['Contact']['nome'])); ?>
			<?php endif; ?>
			<?php echo $html->link(__('Apagar', true), array('action' => 'delete', $contact['Contact']['id']), null, sprintf('Confirma exclusao do registro #%s? (%s)', $contact['Contact']['id'], $contact['Contact']['nome'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<?php echo $this->element('pagination'); ?>

<?php $this->addScript($javascript->link(array('plugins/jquery.lightbox', 'contacts/index'))); ?>
<?php $this->addScript($html->css(array('jquery.lightbox'))); ?>