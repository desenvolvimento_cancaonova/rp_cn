<div class="labels index">
<h2><?php __('Etiquetas');?></h2>


<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('nome');?></th>	
	<th><?php echo $paginator->sort('Data Cadastro', 'created');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($labels as $label):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td width="20px" align="center">
			<?php echo $label['Label']['id']; ?>
		</td>
		<td>
			<?php echo $label['Label']['nome']; ?>
		</td>		
		<td align='center' width="130px">
			<?php echo date('d/m/Y h:i:s', strtotime($label['Label']['created'])); ?>
		</td>
		<td class="actions" width="120px" align="center">
			<?php echo $html->link(__('Ver', true), array('action' => 'view', $label['Label']['id'])); ?>
			<?php echo $html->link(__('Editar', true), array('action' => 'edit', $label['Label']['id'])); ?>
			<?php echo $html->link(__('Apagar', true), array('action' => 'delete', $label['Label']['id']), array('escape'=>false), sprintf('Confirma exclusao do registro #%s?', $label['Label']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<?php echo $this->element('pagination'); ?>