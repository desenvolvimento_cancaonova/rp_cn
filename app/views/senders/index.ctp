<div class="senders index">
<h2><?php __('Remetentes');?></h2>


<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('nome');?></th>	
	<th><?php echo $paginator->sort('Data Cadastro', 'created');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($senders as $sender):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td width="20px" align="center">
			<?php echo $sender['Sender']['id']; ?>
		</td>
		<td>
			<?php echo $sender['Sender']['nome']; ?>
		</td>		
		<td align='center' width="130px">
			<?php echo date('d/m/Y h:i:s', strtotime($sender['Sender']['created'])); ?>
		</td>
		<td class="actions" width="120px" align="center">
			<?php echo $html->link(__('Ver', true), array('action' => 'view', $sender['Sender']['id'])); ?>
			<?php echo $html->link(__('Editar', true), array('action' => 'edit', $sender['Sender']['id'])); ?>
			<?php echo $html->link(__('Apagar', true), array('action' => 'delete', $sender['Sender']['id']), array('escape'=>false), sprintf('Confirma exclusao do registro #%s?', $sender['Sender']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<?php echo $this->element('pagination'); ?>