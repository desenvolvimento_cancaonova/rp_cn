<div class="userActionAuths index">
<h2><?php __('UserActionAuths');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('user_id');?></th>
	<th><?php echo $paginator->sort('action_auth_id');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($userActionAuths as $userActionAuth):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $userActionAuth['UserActionAuth']['id']; ?>
		</td>
		<td>
			<?php echo $userActionAuth['UserActionAuth']['user_id']; ?>
		</td>
		<td>
			<?php echo $userActionAuth['UserActionAuth']['action_auth_id']; ?>
		</td>
		<td>
			<?php echo $userActionAuth['UserActionAuth']['created']; ?>
		</td>
		<td>
			<?php echo $userActionAuth['UserActionAuth']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $userActionAuth['UserActionAuth']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $userActionAuth['UserActionAuth']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $userActionAuth['UserActionAuth']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $userActionAuth['UserActionAuth']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New UserActionAuth', true), array('action' => 'add')); ?></li>
	</ul>
</div>
