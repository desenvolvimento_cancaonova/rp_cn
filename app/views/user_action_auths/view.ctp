<div class="userActionAuths view">
<h2><?php  __('UserActionAuth');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userActionAuth['UserActionAuth']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userActionAuth['UserActionAuth']['user_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Action Auth Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userActionAuth['UserActionAuth']['action_auth_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userActionAuth['UserActionAuth']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userActionAuth['UserActionAuth']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit UserActionAuth', true), array('action' => 'edit', $userActionAuth['UserActionAuth']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete UserActionAuth', true), array('action' => 'delete', $userActionAuth['UserActionAuth']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $userActionAuth['UserActionAuth']['id'])); ?> </li>
		<li><?php echo $html->link(__('List UserActionAuths', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New UserActionAuth', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
