<div class="userActionAuths form">
<?php echo $form->create('UserActionAuth');?>
	<fieldset>
 		<legend><?php __('Add UserActionAuth');?></legend>
	<?php
		echo $form->input('user_id');
		echo $form->input('action_auth_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List UserActionAuths', true), array('action' => 'index'));?></li>
	</ul>
</div>
