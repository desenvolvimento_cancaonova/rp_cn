	<?php
		echo $form->input('nome', array('style'=>'width:250px'));
		echo $form->input('formato', array('style'=>'', 'options'=>$formatos));
		echo $form->input('margem_esquerda', array('style'=>'width:50px', 'after'=>'cm', 'class'=>'input_int'));
		echo $form->input('margem_direita', array('style'=>'width:50px', 'after'=>'cm', 'class'=>'input_int'));
		echo $form->input('margem_superior', array('style'=>'width:50px', 'after'=>'cm', 'class'=>'input_int'));
		echo $form->input('largura_etiqueta', array('style'=>'width:50px', 'after'=>'cm', 'class'=>'input_int'));
		echo $form->input('altura_etiqueta', array('style'=>'width:50px', 'after'=>'cm', 'class'=>'input_int'));
		echo $form->input('espaco_horizontal', array('style'=>'width:50px', 'after'=>'cm', 'class'=>'input_int'));		
		echo $form->input('espaco_vertical', array('style'=>'width:50px', 'after'=>'cm', 'class'=>'input_int'));
		echo $form->input('linhas', array('style'=>'width:50px', 'after'=>'', 'class'=>'input_int'));
		echo $form->input('colunas', array('style'=>'width:50px', 'after'=>'', 'class'=>'input_int'));		
	?>
	
	
<?php $this->addScript($javascript->link(array('labels/form'))); ?>