<?php if (!empty($this->data)): ?>
	
	<h2>Resultado</h2> 
	
	<?php if (!empty($rows)): ?>

	<br />
	
	<?php echo $form->create('Report', array('url'=>"#", 'div'=>false, 'id'=>'ReportFormDo')); ?>

	<?php echo $form->submit('Enviar Campanha', array('div'=>false, 'id'=>'send_button')); ?> &nbsp;
			
	<strong>Op&ccedil;&otilde;es</strong> :
	
	<label>Campanha: </label>
		<?php echo $form->input('ReportConfig.campaign_id', array('label'=>false, 'div'=>false, 'style'=>'', 'options'=>$campaigns, 'id'=>'campaign_id')); ?>
	<label>Remetente: </label>
		<?php echo $form->input('ReportConfig.sender_id', array('label'=>false, 'div'=>false, 'style'=>'', 'options'=>$senders, 'id'=>'sender_id')); ?>
		
	<br /><br />
	
	Total de Contatos: <?php echo sizeof($rows); ?>
	
	<br />
	<table cellpadding="0" cellspacing="0">
	<tr>	
		<th><?php echo $form->input('all', array('type'=>'checkbox', 'checked'=>true, 'value'=>'', 'label'=>false, 'div'=>false, 'id'=>'all_check')); ?></th>
		<th>Nome</th>
		<th>Empresa</th>
		<th style="width:200px">Email</th>			
		<th>Status Envio</th>
	</tr>
		
	<?php
	$i = 0;
	foreach ($rows as $row):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td style='width:15px;'>
				<?php echo $form->input('Report.'.$i, array('type'=>'checkbox', 'checked'=>true, 'value'=>$row['Contact']['id'], 'label'=>false, 'div'=>false, 'class'=>'row_check')); ?>
			</td>			
			<td>
				<?php echo $row['Contact']['nome']; ?>
			</td>
			<td align="left">
				<?php echo $row['Contact']['empresa']; ?>
			</td>
			<td align="left">
				<?php echo $row['Contact']['email']; ?>
			</td>
			<td align="center">
				<span style="color:black" id="status_<?php echo $row['Contact']['id']; ?>">Aguardando</span>
			</td>		
		</tr>
	<?php endforeach; ?>
	
	</table>
	
	<br />	
	
	<?php echo $form->end(); ?>
	
	<?php //echo $this->element('pagination'); ?>
	
	<?php else: ?>
		<br />
		Nenhum registro.
	<?php endif; ?>
	
	<?php $this->addScript($javascript->link(array('campaigns/result'))); ?>
<?php endif;?>