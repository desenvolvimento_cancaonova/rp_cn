<fieldset>
	<legend>Adicionar Imagens</legend>
<?php echo $form->create('MediaGallery', array('type'=>'file', 'url'=>array('controller'=>$this->params['controller'], 'action'=>$this->action, @$this->params['pass'][0]))); ?>
	
	<?php echo $form->input('image', array('type'=>'file', 'class'=>'multi accept-png|jpg|jpeg|gif|PNG|JPG|JPEG|GIF', 'maxlength'=>'1', 'label'=>'Imagem')); ?>

	<br /><br />
	<?php echo $form->end('Adicionar Imagem'); ?>
</fieldset>
	
<?php $this->addScript($javascript->link(array('plugins/MultiFile.pack'))); ?>
