<?php foreach($available_controllers['Controller'] as $controller => $options): ?> 		
	<?php echo $controller ?>:
	<?php echo $form->input('Controllers.'.$controller.'.all', array('type' => 'checkbox')); ?>
	<?php foreach($options['actions'] as $action): ?>
		<?php echo $form->input('Controllers.'.$controller.'.'.$action, array('type' => 'checkbox', 'value'=>1, 'checked'=>!empty($this->data['Controllers'][$controller][$action])||!empty($this->data['Controllers'][$controller]['all'])?true:false)); ?>
	<?php endforeach; ?>
<?php endforeach; ?>