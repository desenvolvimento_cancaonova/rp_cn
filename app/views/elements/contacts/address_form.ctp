	<div class="input text required"><label for="AddressMain">Principal</label>
		<?php echo $form->input('main', array('div'=>false, 'label'=>false, 'style'=>'margin-top:4px')); ?>		
	</div>
	
	<br />
	
				
	<?php
		echo $form->input('address_name', array('style'=>'width:150px', 'label'=>'T&iacute;tulo', 'options'=>$address_names)); 
		echo $form->input('cep', array('style'=>'width:80px'));	
		echo $form->input('endereco', array('style'=>'width:370px', 'label'=>'Endere&ccedil;o'));
		echo $form->input('bairro', array('style'=>'width:170px'));
		echo $form->input('state_id', array('style'=>'', 'label'=>'Estado', 'empty'=>true));
		echo $form->input('city_id', array('style'=>'', 'label'=>'Cidade', 'empty'=>true));		
		echo $form->input('uf', array('style'=>'', 'label'=>'Estado', 'options'=>$estados, 'label'=>'Estado (outro)', 'empty'=>true));
		echo $form->input('cidade', array('style'=>'width:170px', 'label'=>'Cidade (outro)'));		
		echo $form->input('pais', array('style'=>'width:150px'));		
	?>
	<br />
	<?php $this->addScript($javascript->link(array('contacts/address_form'))); ?>