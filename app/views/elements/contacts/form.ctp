<div id="internal_form">

	<fieldset>
		<legend>Dados B&aacute;sicos</legend>
	
	<div class="input text required"><label for="ContactAtivo">Ativo</label>
		<?php echo $form->input('ativo', array('div'=>false, 'label'=>false, 'style'=>'margin-top:4px', 'checked'=>(empty($this->data)&&in_array($this->action, array('add', 'conjuge', 'subcontacts'))?true:$this->data['Contact']['ativo']))); ?>		
	</div>
	<?php if ($this->action == 'edit'): ?>
		<div id="contact_image"><?php echo @is_file(WWW_ROOT.$this->data['MediaGalleryMain']['images']['thumb'])?$this->Html->image($this->data['MediaGalleryMain']['images']['thumb'], array('id'=>'noimage_contact', 'width'=>'120px', 'height'=>'120px')):$this->Html->image('noimage.jpg', array('id'=>'noimage_contact', 'width'=>'120px', 'height'=>'120px')); ?></div>
	<?php endif; ?>
	<br />
	<?php
		echo $form->input('tratamento', array('style'=>'width:100px'));
		echo $form->input('contact_type_id', array('label'=>'Tipo de Contato', 'empty'=>true, 'options'=>$contact_types));
		echo $form->input('data_nascimento', array('type'=>'text', 'style'=>'width:100px', 'class'=>'date_type'));
	?>
	
	<?php
		echo $form->input('sexo', array('style'=>'', 'options'=>$sexo, 'empty'=>true));	
	?>
	
	<div class="input text required"><label for="ContactFilhos">Filhos?</label>
		<?php echo $form->input('filhos', array('div'=>false, 'label'=>false, 'style'=>'margin-top:4px')); ?>		
	</div>
	
	<?php
		echo $form->input('estado_civil', array('style'=>'', 'options'=>$estados_civil, 'empty'=>true));	
	?>
	
	<?php
		echo $form->input('nome', array('style'=>'width:270px'));
		echo $form->input('empresa', array('style'=>'width:270px'));
		echo $form->input('occupation_id', array('empty'=>true, 'label'=>'Cargo'));
		echo $form->input('site', array('style'=>'width:270px'));	
	?>	
	</fieldset>
	
	
	<fieldset>
		<legend>Dados para Contatos</legend>
	<?php 
		echo $form->input('email', array('style'=>'width:270px'));
		echo $form->input('email_secundario', array('style'=>'width:270px'));
		
		echo $form->input('celular', array('style'=>'width:100px', 'label'=>'Celular/WhatsApp', 'class'=>'phone_type'));
		echo $form->input('celular_secundario', array('style'=>'width:100px', 'label'=>'Celular Secund&aacute;rio: ', 'class'=>'phone_type'));

		echo $form->input('fone1_residencial', array('style'=>'width:100px', 'label'=>"Telefone&nbsp;Residencial", 'class'=>'phone_type'));
		
		echo $form->input('fone1_comercial', array('style'=>'width:100px', 'label'=>"Telefone&nbsp;Comercial&nbsp;1", 'class'=>'phone_type'));
		echo $form->input('fone2_comercial', array('style'=>'width:100px', 'label'=>'Telefone&nbsp;Comercial&nbsp;2', 'class'=>'phone_type'));
		
	?>
	</fieldset>
	
	<?php echo $form->submit('Salvar'); ?>
	
	<br />
	
	<fieldset style="height:200px">
		<legend>Dados Adicionais</legend>
		<?php 
			echo $form->input('data_casamento', array('type'=>'text', 'style'=>'width:100px', 'class'=>'date_type'));		
			echo $form->input('data_ordenacao', array('type'=>'text', 'style'=>'width:100px', 'class'=>'date_type'));
			echo $form->input('data_ord_episcopal', array('type'=>'text', 'style'=>'width:100px', 'class'=>'date_type', 'label'=>'Data Ordena&ccedil;&atilde;o Episcopal'));
			echo $form->input('data_consagracao', array('type'=>'text', 'style'=>'width:100px', 'class'=>'date_type'));		
		?>	
	</fieldset>
	
	<fieldset style="height:200px">
		<legend>Dados Adicionais</legend>
			
		<div style="height:100px">	
			<?php echo $form->input('observacoes', array('style'=>'width:410px;height:160px', 'label'=>'Observa&ccedil;&otilde;es')); ?>
		</div>
	</fieldset>
	
	<?php if ($this->action != 'add'): ?>
		<div id="last_modification">
			&Uacute;ltima modifica&ccedil;&atilde;o por: <strong><?php echo $modified['User']['nome']; ?></strong> em <?php echo date('d/m/Y H:i', strtotime($modified['Contact']['modified'])); ?>
		</div>
	<?php endif; ?>
	<br />
</div>	
	<?php $this->addScript($javascript->link(array('contacts/form'))); ?>
    <?php $this->addScript($javascript->link(array('plugins/datepick/jquery.datepick.min', 'plugins/datepick/jquery.datepick-pt-BR', 'plugins/jquery.maskedinput'))); ?>
	<?php $this->addScript($html->css(array('/js/plugins/datepick/jquery.datepick'))); ?>