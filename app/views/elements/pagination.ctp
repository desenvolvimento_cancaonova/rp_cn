<div class="paging">
	<?php echo $paginator->prev('<< '.__('Anterior', true), array('class'=>'disabled'), null, array());?>
  	<?php echo $paginator->numbers()!=''?'|':'';?> <?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('Pr&oacute;ximo', true).' >>', array('escape'=>false, 'class'=>'disabled'), null, array());?>
</div>
<br />
<p><?php
echo $paginator->counter(array(
'format' => __('P&aacute;gina %page% de %pages%, mostrando %current% registros de um total de %count%, iniciando no registro %start%, terminando no %end%', true)
));
?></p>