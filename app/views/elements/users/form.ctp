	<?php
		echo $form->input('nome');
		echo $form->input('username', array('label'=>'Usu&aacute;rio'));
		echo $form->input('senha', array('type'=>'password', 'label'=>'Senha', 'autocomplete' => 'off', 'value' => ''));
		
		if ($session->read('Auth.User.admin')) {
			echo "<div class=\"input text required\"><label for=\"UserAdmin\">Administrador</label>";
			echo $form->input('admin', array('div'=>false, 'label'=>false));
			echo "</div>";
		}
	?>