	<?php echo $form->create('Report', array('url'=>array('controller'=>'reports', 'action'=>$this->action, 'do'), 'div'=>false, 'id'=>'ReportFormDo')); ?>
	
	<label>Agrupar por: </label>
		<?php echo $form->input('group', array('options'=>$groups, 'empty'=>'Selecione um tipo', 'label'=>false, 'div'=>false, 'style'=>'width:200px', 'id'=>'group_by')); ?>
		
	<?php if (!empty($this->params['named']['group'])): ?>
		
		<br /><br />			

		<?php echo $form->submit('Visualizar Relatorio', array('div'=>false, 'id'=>'view_button')); ?> &nbsp;
		<?php echo $form->submit('Exportar PDF', array('div'=>false, 'id'=>'exportar_pdf_button')); ?> &nbsp;
		<?php echo $form->submit('Exportar Excel', array('div'=>false, 'id'=>'exportar_excel_button')); ?>
		
		<strong>Op&ccedil;&otilde;es</strong> :
		<label>Ordernar Por: </label>
			<?php echo $form->input('ReportConfig.fields', array('label'=>false, 'div'=>false, 'style'=>'', 'options'=>$fields_pdf)); ?> 
		<label>Ordem: </label>
		<?php echo $form->input('ReportConfig.order_fields', array('label'=>false, 'div'=>false, 'style'=>'', 'options'=>array('asc'=>'Crescente', 'desc'=>'Descrescente'))); ?>
				
	<?php endif; ?>
	
	<?php echo $form->end(); ?>

<?php $this->addScript($javascript->link(array('reports/estatistico'))); ?>
<?php $this->addScript($javascript->link(array('reports/result'))); ?>
