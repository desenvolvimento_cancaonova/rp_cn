<?php if (!empty($this->data)): ?>
	
	<h2>Resultado</h2> 
	
	<?php if (!empty($rows)): ?>

	<br />
	
	<?php echo $form->create('Report', array('url'=>array('controller'=>'reports', 'action'=>$this->action, 'do'), 'div'=>false, 'id'=>'ReportFormDo')); ?>

	<?php echo $form->submit('Visualizar Relatorio', array('div'=>false, 'id'=>'view_button')); ?> &nbsp;
	<?php echo $form->submit('Exportar PDF', array('div'=>false, 'id'=>'exportar_pdf_button')); ?> &nbsp;
	<?php echo $form->submit('Exportar Excel', array('div'=>false, 'id'=>'exportar_excel_button')); ?>
	
	<strong>Op&ccedil;&otilde;es</strong> :
	<label>Ordernar Por: </label>
		<?php echo $form->input('ReportConfig.fields', array('label'=>false, 'div'=>false, 'style'=>'', 'options'=>$fields_pdf)); ?> 
	<label>Ordem: </label>
		<?php echo $form->input('ReportConfig.order_fields', array('label'=>false, 'div'=>false, 'style'=>'', 'options'=>array('asc'=>'Crescente', 'desc'=>'Descrescente'))); ?>
	
	<br /><br />
	
	Total de Contatos: <?php echo sizeof($rows); ?>
	
	<br />
	<table cellpadding="0" cellspacing="0">
	<tr>	
		<th><?php echo $form->input('all', array('type'=>'checkbox', 'checked'=>true, 'value'=>'', 'label'=>false, 'div'=>false, 'id'=>'all_check')); ?></th>
		<th>Nome</th>
		<th>Empresa</th>
		<th style="width:200px">Email</th>			
	</tr>
		
	<?php
	$i = 0;
	foreach ($rows as $row):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td style='width:15px;'>
				<?php echo $form->input('Report.'.$i, array('type'=>'checkbox', 'checked'=>true, 'value'=>$row['Contact']['id'], 'label'=>false, 'div'=>false, 'class'=>'row_check')); ?>
			</td>			
			<td>
				<?php echo $row['Contact']['nome']; ?>
			</td>
			<td align="left">
				<?php echo $row['Contact']['empresa']; ?>
			</td>
			<td align="left">
				<?php echo $row['Contact']['email']; ?>
			</td>		
		</tr>
	<?php endforeach; ?>
	
	</table>
	
	<br />	
	
	<?php echo $form->end(); ?>
	
	<?php //echo $this->element('pagination'); ?>
	
	<?php else: ?>
		<br />
		Nenhum registro.
	<?php endif; ?>
	
	<?php $this->addScript($javascript->link(array('reports/result'))); ?>
<?php endif;?>