<?php /* ?>
<label><?php echo $date_field_name; ?>: </label>
	<?php echo $form->input($date_field, array('type'=>'date', 'monthNames'=>$meses, 'minYear'=>1900, 'maxYear'=>date('Y'), 'dateFormat'=>'DMY', 'label'=>false, 'div'=>false, 'date_field'=>$date_field, 'style'=>'width:100px', 'class'=>'date_filter diario')); ?>
	 ou <?php echo $form->input($date_field.'_mensal', array('options'=>$meses, 'empty'=>'Mensal:', 'label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'date_filter mensal', 'date_field'=>$date_field)); ?>
	 ou <?php echo $form->input($date_field.'_semanal', array('options'=>$semanal, 'empty'=>'Semanal', 'label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'date_filter semanal', 'date_field'=>$date_field)); ?>
	 &nbsp; <?php echo $form->input($date_field.'_ativo', array('type'=>'checkbox', 'label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'ativo', 'id'=>$date_field.'_ativo', 'date_field'=>$date_field, 'checked'=>!empty($this->data['Report'][$date_field.'_ativo'])?true:false)); ?>
<?php */ ?>

<label><?php echo $date_field_name; ?>: </label>
	<?php /* ?>
	<?php echo $form->input($date_field.'_inicio', array('label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'input_date')); ?>
	at&eacute; 
	<?php echo $form->input($date_field.'_fim', array('label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'input_date')); ?>
	<?php */ ?>
	
	Tipo: 
	<?php echo $form->input($date_field.'_tipo', array('options'=>array('1'=>'Data Completa', '2'=>'Por Mes', '3'=>'Periodo'), 'empty'=>'Selecione', 'label'=>false, 'div'=>false, 'style'=>'width:110px', 'class'=>'date_filter semanal', 'date_field'=>$date_field)); ?>
	=>
	<?php echo $form->input($date_field."_inicio", array('empty'=>'Selecione', 'type'=>'date', 'monthNames'=>$meses, 'minYear'=>1900, 'maxYear'=>date('Y'), 'dateFormat'=>'DMY', 'label'=>false, 'div'=>false, 'date_field'=>$date_field, 'style'=>'width:90px', 'class'=>'date_filter')); ?>
	at&eacute;
	<?php echo $form->input($date_field."_fim", array('empty'=>'Selecione', 'type'=>'date', 'monthNames'=>$meses, 'minYear'=>1900, 'maxYear'=>date('Y'), 'dateFormat'=>'DMY', 'label'=>false, 'div'=>false, 'date_field'=>$date_field, 'style'=>'width:90px', 'class'=>'date_filter')); ?>