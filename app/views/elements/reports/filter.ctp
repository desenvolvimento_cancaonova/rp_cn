<?php echo $form->create('Report', array('url'=>array('controller'=>$this->params['controller'], 'action'=>$this->action))); ?>
	<h2>Filtros</h2>		
	
	<br />
	
	<fieldset>
		<legend>Pessoal</legend>

		<span class='msg_filter' style="display:none">Clique para usar esses filtros.</span>

		<span class="filter_fields">
				
			<label>Status: </label>
				<?php echo $form->input('ativo', array('label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos', 'options'=>array('-1'=>'Inativo', '1'=>'Ativo') , 'value'=>!empty($this->data['Report']['ativo'])?$this->data['Report']['ativo']:1)); ?>
				
			<label>Usar: </label>
				<?php echo $form->input('contact_kind', array('options'=>$show_contacts, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos os Contatos', 'value'=>!empty($this->data['Report']['contact_kind'])?$this->data['Report']['contact_kind']:1)); ?>
			<br /><br />
									
			<label>Nome: </label>
				<?php echo $form->input('nome', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:200px')); ?>
				
			<label>Conjuge: </label>
				<?php echo $form->input('conjuge', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:200px')); ?>
						
			<label>Estado Civil: </label>
				<?php echo $form->input('estado_civil', array('options'=>$estados_civil, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos')); ?>
				
			<label>Sexo: </label>
				<?php echo $form->input('sexo', array('options'=>$sexo, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos')); ?>
			
			<label>Filhos: </label>
				<?php echo $form->input('filhos', array('type'=>'checkbox', 'label'=>false, 'div'=>false, 'style'=>'')); ?>
					
			<br /> <br />	
			
			<label>Endere&ccedil;o: </label>
				<?php echo $form->input('endereco_residencial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:200px')); ?>
					
			<label>Bairro: </label>
				<?php echo $form->input('bairro_residencial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px')); ?>
		
			<label>CEP: </label>
				<?php echo $form->input('cep_residencial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:65px', 'class'=>'cep_type')); ?>
				
			<label>Pa&iacute;s: </label>
				<?php echo $form->input('pais_residencial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px')); ?>
				
			<br /><br />
									
			<label>Estado: </label>
				<?php echo $form->input('state_id_residencial', array('options'=>$states, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos')); ?>
			
			<label>Cidade: </label>
				<?php echo $form->input('city_id_residencial', array('options'=>$cities, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos')); ?>
							
			<br /><br />
						
			<label>Estado (outro): </label>
				<?php echo $form->input('uf_residencial', array('options'=>$estados, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos')); ?>
							
			<label>Cidade (outro): </label>
				<?php echo $form->input('cidade_residencial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px')); ?>	
			
		</span>
	</fieldset>
	
	<br />

	<fieldset>
		<legend>Comercial</legend>

		<span class='msg_filter'>Clique para usar esses filtros.</span>

		<span class="filter_fields" style="display:none">	
	
			<label>Empresa: </label>
				<?php echo $form->input('empresa', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:300px')); ?>
									
			<br /> <br />	
			
			<label>Endere&ccedil;o: </label>
				<?php echo $form->input('endereco_comercial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:200px')); ?>
		
			<label>Bairro: </label>
				<?php echo $form->input('bairro_comercial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px')); ?>
				
			<label>CEP: </label>
				<?php echo $form->input('cep_comercial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:65px', 'class'=>'cep_type')); ?>
							
			<label>Pa&iacute;s: </label>
				<?php echo $form->input('pais_comercial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px')); ?>
				
					
			<br /><br />
			
			<label>Estado: </label>
				<?php echo $form->input('state_id_comercial', array('options'=>$states, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos')); ?>
				
			<label>Cidade: </label>
				<?php echo $form->input('city_id_comercial', array('options'=>$cities, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos')); ?>
							
			
			<br /><br />
										
			<label>Estado (outro): </label>
				<?php echo $form->input('uf_comercial', array('options'=>$estados, 'label'=>false, 'div'=>false, 'style'=>'', 'empty'=>'Todos')); ?>
			
			<label>Cidade (outro): </label>
				<?php echo $form->input('cidade_comercial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px')); ?>
			
		</span>
	</fieldset>		

	<br />
	
	<fieldset>
		<legend>Tipo do Contato</legend>

		<span class='msg_filter'>Clique para usar esses filtros.</span>

		<span class="filter_fields" style="display:none">
			
			<span style="float:left;margin-right:20px">
				<label>Tipo de Contato: </label>
				<br />			
				<?php echo $form->input('contact_type_id', array('options'=>$contact_types, 'multiple'=>true, 'empty'=>'Todos', 'label'=>false, 'div'=>false, 'style'=>'width:200px')); ?>		
			</span>
			
			<span style="float:left">
				<label>Cargo: </label>
				<br />			
				<?php echo $form->input('occupation_id', array('options'=>$occupations, 'multiple'=>true, 'empty'=>'Todos', 'label'=>false, 'div'=>false, 'style'=>'width:200px')); ?>
			</span>
		</span>
	</fieldset>
	
	<br />		
	
	<fieldset>
		<legend>Meios Contatos</legend>

		<span class='msg_filter'>Clique para usar esses filtros.</span>

		<span class="filter_fields" style="display:none">	
							
		
												
			<label>Celular/WhatsApp: </label>
				<?php echo $form->input('celular', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'phone_type')); ?>
			
			<label>Celular Secund&aacute;rio: : </label>
				<?php echo $form->input('celular_secundario', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'phone_type')); ?>

			<br /><br />
				
			<label>Fone Residencial: </label>
				<?php echo $form->input('fone1_residencial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'phone_type')); ?>

			<br /><br />
			<label>Fone Comercial: </label>
				<?php echo $form->input('fone1_comercial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'phone_type')); ?>
				
			<label>Fone Comercial Secund&aacute;rio: </label>
				<?php echo $form->input('fone2_comercial', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:100px', 'class'=>'phone_type')); ?>
		
						
			<br /><br/>
			
			<label>Email: </label>
				<?php echo $form->input('email', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:200px')); ?>
			
			<label>Email Secund&aacute;rio: </label>
				<?php echo $form->input('email_secundario', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:200px')); ?>
				
			<label>Site: </label>
				<?php echo $form->input('site', array('type'=>'text', 'label'=>false, 'div'=>false, 'style'=>'width:250px')); ?>
		</span>
	</fieldset>
	
	<br />
	
	<fieldset>
		<legend>Datas</legend>

		<span class='msg_filter' >Clique para usar esses filtros.</span>

		<span class="filter_fields" style="display:none">	
						
			<?php echo $this->element('reports/date_fields_filter', array('date_field'=>'data_nascimento', 'date_field_name'=>'Anivers&aacute;rio')); ?>
								
			<br /> <br />
			
			<?php echo $this->element('reports/date_fields_filter', array('date_field'=>'data_casamento', 'date_field_name'=>'Casamento')); ?>
								
			<br /> <br />
			
			<?php echo $this->element('reports/date_fields_filter', array('date_field'=>'data_ordenacao', 'date_field_name'=>'Ordena&ccedil;&atilde;o')); ?>
								
			<br /> <br />
			
			<?php echo $this->element('reports/date_fields_filter', array('date_field'=>'data_ord_episcopal', 'date_field_name'=>'Ordena&ccedil;&atilde;o Episcopal')); ?>
								
			<br /> <br />
			
			<?php echo $this->element('reports/date_fields_filter', array('date_field'=>'data_consagracao', 'date_field_name'=>'Consagra&ccedil;&atilde;o')); ?>			
			
		</span>
	</fieldset>
	
	<br /> <br />
		
	<?php echo $this->element('reports/order'); ?>
	
	
	 &nbsp; 
	<?php echo $form->submit('Buscar', array('label'=>false, 'div'=>false)); ?>
<?php echo $form->end(); ?>


<?php $this->addScript($javascript->link(array('plugins/datepick/jquery.datepick.min', 'plugins/datepick/jquery.datepick-pt-BR', 'plugins/jquery.maskedinput', 'reports/filter'))); ?>
<?php $this->addScript($html->css(array('/js/plugins/datepick/jquery.datepick'))); ?>