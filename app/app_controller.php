<?php

class AppController extends Controller {
	
	var $helpers = array('Form', 'Javascript', 'Html', 'Myhtml', 'Session');
	var $components = array('Session');
	var $uses = array('UserActionAuth', 'ControllerAuth', 'ActionAuth');

	var $per_page = array('Todos', 10, 20, 30, 50);
	var $fields = array('Contact.nome'=>'Nome', 'Contact.empresa'=>'Empresa', 'Contact.email'=>'Email');
	var $meses = array('1'=>'Janeiro', '2'=>'Fevereiro', '3'=>'Marco', '4'=>'Abril', '5'=>'Maio', '6'=>'Junho', '7'=>'Julho', '8'=>'Agosto', '9'=>'Setembro', '10'=>'Outubro', '11'=>'Novembro', '12'=>'Dezembro');
	var $semanal = array('7'=>'Esta semana', '14'=>'Proxima Semana', '-7'=>'Semana Anterior');
	
	function beforeFilter() {
		/*				
		$this->Auth->loginError = 'Erro ao logar. Tente novamente.';
		$this->Auth->authError = 'Voc&ecirc; n&atilde;o pode acessar essa &aacute;rea.';
		$this->Auth->loginRedirect = array('controller' => 'pages', 'action' => 'home');
		*/
							
		$controller = $this->params['controller'];
		$action = $this->action;
		
		//verifica se a acao � permissao publica
		$is_public_permission = $this->ActionAuth->find('first', array('conditions'=>array('ActionAuth.name'=>$action, 'ControllerAuth.name'=>$controller, 'ActionAuth.public'=>1), 'recursive'=>1, 'fields'=>array('public')));
				
		//checa se nao eh login e nao esta logado, entao redireciona pro login
		if ($action != 'login' && !$this->Session->check('Auth.User.id') && !$this->data && !$is_public_permission['ActionAuth']['public'])
			$this->redirect(array('controller'=>'users', 'action'=>'login', 'admin'=>false));


		if (!$is_public_permission['ActionAuth']['public']) {			
			
			//se a acao nao for login ou logout continua validacao
			if ($action != 'login' && $action != 'logout') {
				
				//permissoes
				$permissions = array();
		
				//eh admin ou nao
				$allow = array();
				
				//eh admin ou nao
				$admin = $this->Session->read('Auth.User.admin')?true:false;
				$this->set('admin', $admin);
				
				$user = $this->Session->read('Auth.User.username');
				if( !empty( $user ) ) {
					Configure::write( 'User', $user );
				}
				
				if (!$admin) {
					
					//pega permissoes do usuario logado				
					$permissions = $this->UserActionAuth->find('list', array(	'conditions'=> array('UserActionAuth.user_id'=>$this->Session->read('Auth.User.id')), 
																				'recursive'	=> -1,
																				'fields' 	=> array('UserActionAuth.action_auth_id', 'UserActionAuth.action_auth_id')));
					//pega permissoes globais
					$global_permissions = $this->ActionAuth->find('list', array('conditions'=> array('ActionAuth.allowed'), 
																				'recursive'	=> -1,
																				'fields' 	=> array('ActionAuth.id', 'ActionAuth.id')));
					
					//juntando permissoes do usuario com permissoes globais
					$permissions = array_merge($permissions, $global_permissions);
					//debug($permissions);
					//exit;
					
					//verfica se o usuario eh admin ou controller pages ou login do usuario ou logout
					$allow = $this->ActionAuth->find('all', array(	'conditions'=> array('ControllerAuth.name'=>$controller, 'ActionAuth.name'=>$action, 'ActionAuth.id'=>$permissions),
																	'fields'	=> array('ActionAuth.id')));			
									
					//guarda todos o menus que o usuario tem permissao
					$main_menu = $this->ActionAuth->find('all', array(	'conditions'=> array('ActionAuth.name'=>'index', 'ActionAuth.id'=>$permissions),
																		'fields'	=> array('ActionAuth.id', 'ControllerAuth.name')));
					$main_menu = Set::Extract($main_menu, '{n}.ControllerAuth.name');		
							
					//checa se o usuario tem permissao
					if (empty($allow) && $controller != 'pages') {
				
						$this->Session->setFlash($this->Auth->authError);
						$this->redirect($this->referer());
					}
				
					$permission_uri = $this->ActionAuth->find('all', array(	'conditions'=> array('ActionAuth.id'=>$permissions, 'ControllerAuth.name'=>$controller), 
																			'recursive'	=> 1,
																			'fields' 	=> array('ActionAuth.name', 'ControllerAuth.name'),
																			'contain'	=> array('ControllerAuth')));
					$this->set('main_menu', $main_menu);
				}				
			}
		}
	}

	protected function __input($field_data) {
		
		if ((int)$field_data) {
			$data1 = explode('/', $field_data);						
			return $data1[2].'-'.$data1[1].'-'.$data1[0];
		}
		else
			return '';
	}
	
	protected function __output($field_data) {
				
		if (strtotime($field_data) != strtotime('0000-00-00'))
			return date('d/m/Y', strtotime($field_data));			
		else
			return '';
	}
	
	protected function default_filter_conditions() {
				
		$conditions = array();
		
		if (!empty($this->data['Report']['contact_kind']))
			$conditions['Contact.contact_kind'] = $this->data['Report']['contact_kind'];
		
		if (!empty($this->data['Report']['ativo']) && $this->data['Report']['ativo'] == '-1')
			$conditions['Contact.ativo'] = 0;
		else if (!empty($this->data['Report']['ativo']) && $this->data['Report']['ativo'] == '1')
			$conditions['Contact.ativo'] = 1;
		
		if (!empty($this->data['Report']['nome']))
			$conditions[] = 'Contact.nome LIKE "%'.$this->data['Report']['nome'].'%"';
		
		if (!empty($this->data['Report']['conjuge']))
			$conditions[] = 'Conjuge.nome LIKE "%'.$this->data['Report']['conjuge'].'%"';
		
		if (!empty($this->data['Report']['filhos']))
			$conditions['Contact.filhos'] = $this->data['Report']['filhos'];
			
		if (!empty($this->data['Report']['estado_civil']))
			$conditions['Contact.estado_civil'] = $this->data['Report']['estado_civil'];
			
		if (!empty($this->data['Report']['sexo']))
			$conditions['Contact.sexo'] = $this->data['Report']['sexo'];
		
			
		if (!empty($this->data['Report']['endereco_residencial']))
			$conditions[] = 'AddressResidencial.endereco LIKE "%'.$this->data['Report']['endereco_residencial'].'%"';
							
		if (!empty($this->data['Report']['bairro_residencial']))
			$conditions[] = 'AddressResidencial.bairro LIKE "%'.$this->data['Report']['bairro_residencial'].'%"';

		if (!empty($this->data['Report']['state_id_residencial']))
			$conditions['AddressResidencial.state_id'] = $this->data['Report']['state_id_residencial'];
			
		if (!empty($this->data['Report']['city_id_residencial']))
			$conditions['AddressResidencial.city_id'] = $this->data['Report']['city_id_residencial'];

		//campos anteriores(antigos)	
		if (!empty($this->data['Report']['cidade_residencial']))
			$conditions[] = 'AddressResidencial.cidade LIKE "%'.$this->data['Report']['cidade_residencial'].'%"';
			
		if (!empty($this->data['Report']['uf_residencial']))
			$conditions['AddressResidencial.uf'] = $this->data['Report']['uf_residencial'];
			
		if (!empty($this->data['Report']['cep_residencial']))
			$conditions[] = 'AddressResidencial.cep LIKE "%'.$this->data['Report']['cep_residencial'].'%"';
			
		if (!empty($this->data['Report']['pais_residencial']))
			$conditions[] = 'AddressResidencial.pais LIKE "%'.$this->data['Report']['pais_residencial'].'%"';
			
		if (!empty($this->data['Report']['empresa']))
			$conditions[] = 'Contact.empresa LIKE "%'.$this->data['Report']['empresa'].'%"';

			
		if (!empty($this->data['Report']['occupation_id']))
			$conditions['Contact.occupation_id'] = $this->data['Report']['occupation_id'];
			
		if (!empty($this->data['Report']['contact_type_id']))
			$conditions['Contact.contact_type_id'] = $this->data['Report']['contact_type_id'];

																
		if (!empty($this->data['Report']['endereco_comercial']))
			$conditions[] = 'AddressComercial.endereco LIKE "%'.$this->data['Report']['endereco_comercial'].'%"';
		
		if (!empty($this->data['Report']['bairro_comercial']))
			$conditions[] = 'AddressComercial.bairro LIKE "%'.$this->data['Report']['bairro_comercial'].'%"';
			
		if (!empty($this->data['Report']['state_id_comercial']))
			$conditions['AddressComercial.state_id'] = $this->data['Report']['state_id_comercial'];
			
		if (!empty($this->data['Report']['city_id_comercial']))
			$conditions['AddressComercial.city_id'] = $this->data['Report']['city_id_comercial'];
			
		if (!empty($this->data['Report']['cidade_comercial']))
			$conditions[] = 'AddressComercial.cidade LIKE "%'.$this->data['Report']['cidade_comercial'].'%"';
		
		if (!empty($this->data['Report']['uf_comercial']))
			$conditions['AddressComercial.uf'] = $this->data['Report']['uf_comercial'];
		
		if (!empty($this->data['Report']['cep_comercial']))
			$conditions[] = 'AddressComercial.cep LIKE "%'.$this->data['Report']['cep_comercial'].'%"';
			
		if (!empty($this->data['Report']['pais_comercial']))
			$conditions[] = 'AddressComercial.pais LIKE "%'.$this->data['Report']['pais_comercial'].'%"';

			
		if (!empty($this->data['Report']['fone1_comercial']))
			$conditions[] = 'Contact.fone1_comercial = "'.$this->data['Report']['fone1_comercial'].'"';
		
		if (!empty($this->data['Report']['fone2_comercial']))
			$conditions[] = 'Contact.fone2_comercial = "'.$this->data['Report']['fone2_comercial'].'"';
			
		if (!empty($this->data['Report']['fone1_residencial']))
			$conditions[] = 'Contact.fone1_residencial = "'.$this->data['Report']['fone1_residencial'].'"';
		
		if (!empty($this->data['Report']['celular']))
			$conditions[] = 'Contact.celular = "'.$this->data['Report']['celular'].'"';
		
		if (!empty($this->data['Report']['celular_secundario']))
			$conditions[] = 'Contact.celular_secundario = "'.$this->data['Report']['celular_secundario'].'"';

		if (!empty($this->data['Report']['email']))
			$conditions[] = 'Contact.email LIKE "%'.$this->data['Report']['email'].'%"';
			
		if (!empty($this->data['Report']['email_secundario']))
			$conditions[] = 'Contact.email_secundario LIKE "%'.$this->data['Report']['email_secundario'].'%"';
			
		if (!empty($this->data['Report']['site']))
			$conditions[] = 'Contact.site LIKE "%'.$this->data['Report']['site'].'%"';
		
			
		//if (!empty($this->data['Report']['data_nascimento_ativo']))
		if (!empty($this->data['Report']['data_nascimento_tipo']))								
			$conditions = $this->date_filter_conditions('data_nascimento', $conditions);
							
		//if (!empty($this->data['Report']['data_casamento_ativo']))
		if (!empty($this->data['Report']['data_casamento_tipo']))
			$conditions = $this->date_filter_conditions('data_casamento', $conditions);
			
		//if (!empty($this->data['Report']['data_ordenacao_ativo']))
		if (!empty($this->data['Report']['data_ordenacao_tipo']))
			$conditions = $this->date_filter_conditions('data_ordenacao', $conditions);
		
		//if (!empty($this->data['Report']['data_ord_episcopal_ativo']))
		if (!empty($this->data['Report']['data_ord_episcopal_tipo']))
			$conditions = $this->date_filter_conditions('data_ord_episcopal', $conditions);
			
		//if (!empty($this->data['Report']['data_consagracao_ativo']))
		if (!empty($this->data['Report']['data_consagracao_tipo']))
			$conditions = $this->date_filter_conditions('data_consagracao', $conditions);
			
		return $conditions;		
	}
	
	protected function date_filter_conditions($field, $conditions) {
		
		/*
		//mensal
		if (!empty($this->data['Report'][$field.'_mensal'])) {
			
			$conditions[] = "MONTH(Contact.".$field.") = ".$this->data['Report'][$field.'_mensal'];	
		}
		//semanal
		else if (!empty($this->data['Report'][$field.'_semanal'])) {
									
			$conditions[] = "( TO_DAYS(DATE(CONCAT(YEAR(NOW()), MONTH(Contact.".$field."), DAY(Contact.".$field.")))) - TO_DAYS(NOW()) ) >= ".$this->data['Report'][$field.'_semanal'];
		}
		//dia
		else {
			
			$conditions[] = "MONTH(Contact.".$field.") = ".$this->data['Report'][$field]['month'];
			$conditions[] = "DAY(Contact.".$field.") = ".$this->data['Report'][$field]['day'];
		}
		*/
		
		//debug($this->data);
		
		//exato
		if ($this->data['Report'][$field.'_tipo'] == 1) {
			
			//dia exato (dia mes ano)
			if (!empty($this->data['Report'][$field.'_inicio']['day']) && !empty($this->data['Report'][$field.'_inicio']['month']) && !empty($this->data['Report'][$field.'_inicio']['year'])) {
		
				$conditions[] = "DATE(Contact.".$field.") = '".$this->data['Report'][$field.'_inicio']['year']."-".$this->data['Report'][$field.'_inicio']['month']."-".$this->data['Report'][$field.'_inicio']['day']."'";
			}
		}
		//mes
		else if ($this->data['Report'][$field.'_tipo'] == 2) {
			
			//mes
			if (!empty($this->data['Report'][$field.'_inicio']['month'])) {
		
				$conditions[] = "MONTH(Contact.".$field.") = ".$this->data['Report'][$field.'_inicio']['month'];
			}
		}
		//periodo
		else if ($this->data['Report'][$field.'_tipo'] == 3) {
			
			
			if (!empty($this->data['Report'][$field.'_inicio']['day']) && !empty($this->data['Report'][$field.'_inicio']['month'])) {
		
				//$conditions[] = "DATE(Contact.".$field.") >= '".$this->data['Report'][$field.'_inicio']['year']."-".$this->data['Report'][$field.'_inicio']['month']."-".$this->data['Report'][$field.'_inicio']['day']."'";
				$conditions[] = "DATE(CONCAT(YEAR(NOW()), '-', MONTH(Contact.".$field."), '-', DAY(Contact.".$field."))) >= '".date('Y', strtotime('now'))."-".$this->data['Report'][$field.'_inicio']['month']."-".$this->data['Report'][$field.'_inicio']['day']."'";
			}
			
			if (!empty($this->data['Report'][$field.'_fim']['day']) && !empty($this->data['Report'][$field.'_fim']['month'])) {
		
				//$conditions[] = "DATE(Contact.".$field.") <= '".$this->data['Report'][$field.'_fim']['year']."-".$this->data['Report'][$field.'_fim']['month']."-".$this->data['Report'][$field.'_fim']['day']."'";
				$conditions[] = "DATE(CONCAT(YEAR(NOW()), '-', MONTH(Contact.".$field."), '-', DAY(Contact.".$field."))) <= '".date('Y', strtotime('now'))."-".$this->data['Report'][$field.'_fim']['month']."-".$this->data['Report'][$field.'_fim']['day']."'";
			}
		}
		
		
		
		return $conditions;
	}
}

?>