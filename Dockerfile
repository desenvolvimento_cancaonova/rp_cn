FROM php:5.6-apache

# RUN apt-get update && apt-get install -y --force-yes \
#   libicu-dev \
#   libpq-dev \
#   mysql-client \
#   zip \
#   unzip \
#   libzip-dev \
#   zlib1g-dev \
#   libmcrypt-dev \
#   gettext-base \
#   && rm -r /var/lib/apt/lists/*

RUN docker-php-ext-install \
  # intl \
  # mbstring \
  # pcntl \
  # pdo_mysql \
  # zip \
  # opcache \
  # mcrypt \
  mysql

ENV APP_HOME /var/www/html

WORKDIR $APP_HOME

RUN a2enmod rewrite

# # COPY .bin/entrypoint.sh /usr/local/bin/entrypoint.sh
# # RUN chmod +x /usr/local/bin/entrypoint.sh

# RUN usermod -u 1000 www-data && groupmod -g 1000 www-data
# RUN chown -Rf www-data:www-data $APP_HOME

COPY .config/php.ini $PHP_INI_DIR/php.ini

# # ENTRYPOINT ["entrypoint.sh"]
# CMD ["apache2-foreground"]